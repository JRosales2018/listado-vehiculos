﻿<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    

    <title>Ayuda-T Pymes una Asesoría Online y presencial</title>
    <meta property="og:title" content="Ayuda-T Pymes una Asesoría Online y presencial" />
    <meta property="og:url" content="https://www.ayudatpymes.com/asesoria-online-ayuda-t-pymes" />
    <meta property="og:description" content="Muchos más que una Asesoria, una forma diferente de gestionar tu negocio, conocenos!" />
    <meta name="description" content="Muchos más que una Asesoria, una forma diferente de gestionar tu negocio, conocenos!" />
    
    <link rel="stylesheet" href="css/style-conocenos.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            
    <span class="breadcrumbs"><span> | </span>CONOCENOS</span>
    

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="conocenos-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <img src="img/icoT.png" alt="ayudaT ico" />
            </div>
            <div class="row justify-content-sm-center cabecera">
                <div class="col-lg-8">
                    <img src="img/ayudat.jpg" alt="Ayuda T" />
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-autonomo justify-content-sm-center">
            <h1><span>CONÓCENOS</span><br> MÁS QUE UNA ASESORÍA BARATA</h1>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row panel-why justify-content-md-center">

                <div class="col-sm-9 col-lg-4 subpanel-why">
                    <p>Mucho más que una asesoría barata. Somos la asesoría online y presencial que hará que dejes de preocuparte por el papeleo de tu negocio. Tendrás un servicio integral con tarifa plana a tu disposición, al mismo precio de una asesoría barata, pero contando con la excelencia de un servicio especializado, respaldado por la confianza de miles de clientes como tú.</p>
                    <p>Puedes dejar en nuestras manos todas las gestiones laborales, fiscales, contables y de asesoramiento jurídico, porque todas están incluidas dentro de tu cuota mensual con el servicio de asesoría online y presencial.</p>
                </div>
                <div class="col-sm-9 col-lg-4 subpanel-why">

                    <p>Tenemos una metodología de trabajo propia, desarrollamos tecnología como Selfconta, el programa de facturación y contabilidad gratuito para facilitar el día a día de cualquier pequeño y mediano negocio como el tuyo y que puedes usar sin límite en el momento que decidas, incluso si no quieres o no necesitas contratar nuestro servicio.</p>
                    <p>Se acabó acumular papeleo, sólo con descargándote nuestra app podrás digitalizar las facturas, envíalas a tu asesor fiscal con una simple fotografía y acceder a tus documentos desde cualquier parte.</p>


                </div>
                <div class="col-sm-9 col-lg-4 subpanel-why">
                    <p>No somos ni una simple gestoría online ni una "gestoría barata", somos la gestoría que está revolucionando el sector para hacer la vida más fácil a toda autónomo o empresa que quiera simplificar la gestión de su negocio. Elige entre una amplia gama de tarifas a tu medida, tú decides si quieres comunicarte con tu asesor personal vía email, telefónica o cara a cara yendo directamente a la oficina. Siempre obtendrás un trato cercano, un servicio rápido, cómodo y sencillo.</p>

                </div>
                <h2>¿Prefieres el servicio de asesoría online o presencial?</h2>
                <div class="img-why">
                    <img src="img/face.jpg" alt=":D" />
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN WHY --------------------------------------->
        <!--        ---------------------------------- OPINIONES --------------------------------------->


        <div class="row slide-opiniones justify-content-sm-center">
            <h2>+ DE
                <?php echo $clientes ?> CLIENTES CONFIAN EN NUESTRA ASESORÍA ONLINE</h2>
        </div>


        <div class="container">
            <div class="row opiniones-autonomo justify-content-sm-center">
                <div class="col-sm-5 col-lg-4">
                    <a href="http://www.ayudatpymes.com/opiniones/" target="_blank"><p class="title-opiniones1">Opiniones de Ayuda T clientes</p></a>
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <p class="comentario">"Me gusta por la sencillez y rapidez de nuestro gestor y sobre todo la facilidad de tener todo disponible en cualquier momento”</p>
                                        <p class="autor">Juan Villa Carrasco.</p>
                                        <p class="empresa">Gerente de <span>Voip telecom S.L.</span></p>
                                        <p class="url"><a >voiptelecom.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Me gusta Ayuda-T-Pymes " por su rápida respuesta y acertada" la recomiendo por su precio."</p>
                                        <p class="autor">Gerente de solupcdoctor</p>
                                        <p class="url"><a>solupcdoctor.es</a></p>

                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por su sencilla plataforma y su sencillez de uso, y por la excelente atención al cliente."</p>
                                        <p class="autor">Daniel Tomas</p>
                                        <p class="empresa">Gerente de <span>dtconsultor</span></p>
                                        <p class="url"><a>dtconsultor.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Ayuda-T nos está ayudando a sacar adelante nuestro proyecto, con sus consejos y su trabajo efectivo."</p>
                                        <p class="autor">Javier Calleja</p>
                                        <p class="url"><a>flunorte.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Siempre están cuando los necesito. Además de mucha profesionalidad y buen servicio. Y todo esto a un precio increíble."</p>
                                        <p class="autor">Inmaculada Ruiz</p>
                                        <p class="empresa">Gerente R.D. de <span>Nutricion Jerez S.L.</span></p>
                                        <p class="url"><a>Nutricion Jerez S.L.</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Mi asesor es el mejor!"</p>
                                        <p class="autor">Javier Infantes Martín </p>
                                        <p class="url"><a>Nutricion Jerez S.L.</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Muy buena. Siempre me ha atendido que prontitud y dado soluciones."</p>
                                        <p class="autor">Luis Alberto Encina Rojas </p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"No tenemos hasta el momento sugerencias de mejora. Nuestra súper asesora Laura cumple perfectamente con nuestras expectativas."</p>
                                        <p class="autor">Juri-Dileyc SL</p>
                                        <p class="url"><a>juri-dileyc.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por su profesionalidad, trato personalizado y empatía con el cliente."</p>
                                        <p class="autor">Gerente PresumeBox Ecommerce Report S.L.</p>
                                        <p class="url"><a>presumedebebe.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"me gusta por la atención y por el precio final, y se lo recomendaría por lo mismo."</p>
                                        <p class="autor">Gerente Laredo Express S.L.</p>
                                        <p class="url"><a >laredoexpress.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por la rapidez en la respuesta y la cercanía en el trato a pesar de la distancia.”</p>
                                        <p class="autor">Javier</p>
                                        <p class="empresa">Administrador de <span>inbade.es</span></p>
                                        <p class="url"><a>inbade.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Me simplifican mucho las cosas. Estoy empezando y se agradece."</p>
                                        <p class="autor">Angel Canas</p>
                                        <p class="url"><a>Field Engineer Autónomo</a></p>
                                    </div>

                                </div>
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-caret-left flecha-opiniones-izquierda"></i>
  </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-caret-right flecha-opiniones-derecha"></i>
  </a>
                    </div>
                </div>
            </div>
        </div>

        <!--        ---------------------------------- FIN OPIONES --------------------------------------->
        <!--        ---------------------------------- FORMULARIO 1 --------------------------------------->


        <div class="row pre-formulario  justify-content-sm-center">
            <h2>
                AYUDAMOS A LAS STARTUPS
            </h2>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Tenemos sangre de Startup y creemos en ellas. Por ello hemos creado un pequeño programa para desarrollo e inversión de proyectos relacionados con nuestro sector.
                        </p>
                        <p class="texto-formulario">Si tienes un proyecto o quieres lanzarlo y está relacionado con las nuevas tecnología y nuestra actividad, ponte en contacto con nosotros a ver si te podemos ayudar.
                        </p>


                    </div>
                    <div class="col-lg-4 ">
                        <?php 
if(isset($_GET['gracias'])){
?>
<div>
    <p class="gracias1">¡Genial! tu mensaje ha sido enviado con exito</p>
    <p class="gracias2">En breve nos pondremos en contacto contigo</p>
</div>

<?php }else { ?>
                        <form id="form-section2" action="mail/send_mail.php" method="post">
                            <p class="texto-formulario2">INFÓRMATE Y CONTACTA</p>
                            <div class="form-group radj-reg-14 form-group-l">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="teléfono*">
                            </div>
                            <div class="form-group radj-reg-14 form-group-r">
                                <input type="email" name="email" class="form-control" id="email" placeholder="email">
                            </div>
                            <div class="form-group radj-reg-14">
                                <textarea class="form-control" name="comment" id="comment" placeholder="consulta"></textarea>
                            </div>

                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input class="form-check-input styled" type="checkbox" id="aviso" name="aviso" required>
                                    <label class="radj-med-10 agree" for="aviso">
                            Acepto las <a href="terminos_y_condiciones">términos y condiciones</a>, <a href="politica_de_privacidad" target="_blank">política de privacidad</a> y <a href="acuerdo_de_procesamiento_de_datos" target="_blank">procesamiento de datos</a>.*
                        </label>
                                </div>

                            </div>
                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input type="checkbox" class="form-check-input" id="news" name="news">
                                    <label class="radj-med-10 agree" for="news">
                            Deseo recibir información acerca de los servicios de Ayuda T un lugar todas las soluciones por medios electrónicos.
                        </label>
                                </div>

                            </div>
                            <input type="hidden" name="posicion" value="<?php echo $_SERVER['REQUEST_URI']?>/emprendedores">
                            <button type="submit" id="submit" class="btn btn-primary">QUIERO SABER MÁS</button>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!--        ---------------------------------- FIN FORMULARIOS 1 --------------------------------------->
        <!--        ----------------------------------  FORMULARIOS 2--------------------------------------->
        <div class="row pre-formulario pre-formulario2 justify-content-sm-center">
            <h2>
                TRABAJA CON NOSOTROS
            </h2>
        </div>
        <div class="row formulario formulario2 justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Si te gusta trabajar en equipo, las nuevas tecnologías y te gusta Ayuda T Pymes.</p>
                        <p class="texto-formulario">Envíanos tus datos y luego en comentarios qué puedes aportar a Ayuda T Pymes.</p>
                        <p class="texto-formulario">Eso sí, recuerda, estamos en ¡¡Cádiz!!</p>

                    </div>
                    <div class="col-lg-4 ">
                        <?php 
if(isset($_GET['gracias'])){
?>
<div>
    <p class="gracias1">¡Genial! tu mensaje ha sido enviado con exito</p>
    <p class="gracias2">En breve nos pondremos en contacto contigo</p>
</div>

<?php }else { ?>
                        <form id="form-section3" action="mail/send_mail.php" method="post">
                            <p class="texto-formulario2">INFÓRMATE Y CONTACTA</p>
                            <div class="form-group radj-reg-14 form-group-l">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="teléfono*">
                            </div>
                            <div class="form-group radj-reg-14 form-group-r">
                                <input type="email" name="email" class="form-control" id="email" placeholder="email">
                            </div>
                            <div class="form-group radj-reg-14">
                                <textarea class="form-control" name="comment" id="comment" placeholder="Comentarios"></textarea>
                            </div>

                            <div class="form-group form-check">
                                <div class="checkbox">
                                <input class="form-check-input styled" type="checkbox" id="aviso2" name="aviso" required>
                                <label class="radj-med-10 agree" for="aviso2">
                            Acepto las <a href="terminos_y_condiciones">términos y condiciones</a>, <a href="politica_de_privacidad" target="_blank">política de privacidad</a> y <a href="acuerdo_de_procesamiento_de_datos" target="_blank">procesamiento de datos</a>.*
                        </label>
                                </div>

                            </div>
                            <div class="form-group form-check">
                                <div class="checkbox">
                        <input type="checkbox" class="form-check-input" id="news2" name="news">
                        <label class="radj-med-10 agree" for="news2">
                            Deseo recibir información acerca de los servicios de Ayuda T un lugar todas las soluciones por medios electrónicos.
                        </label>
                    </div>

                            </div>
                            <input type="hidden" name="posicion" value="<?php echo $_SERVER['REQUEST_URI']?>/trabaja">
                            <button type="submit" id="submit2" class="btn btn-primary">QUIERO SABER MÁS</button>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <!--        ----------------------------------FIN FORMULARIO 2 --------------------------------------->

        <!--        ----------------------------------  FORMULARIOS 3--------------------------------------->
        <div class="row pre-formulario pre-formulario3 justify-content-sm-center">
            <h2>
                PROYECTOS
            </h2>
        </div>
        <div class="row formulario formulario3 justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Si tienes un proyecto en el cuál crees que puedes establecer sinergias o acuerdos con nosotros, no dudes en ponerte en contacto, nos encanta hablar y poder oír cualquier tipo de propuesta. </p>
                        <p class="texto-formulario">Pero lo que más nos gusta es: ¡¡Trabajar en equipo!!</p>


                    </div>
                    <div class="col-lg-4 ">
                                                <?php 
if(isset($_GET['gracias'])){
?>
<div>
    <p class="gracias1">¡Genial! tu mensaje ha sido enviado con exito</p>
    <p class="gracias2">En breve nos pondremos en contacto contigo</p>
</div>

<?php }else { ?>
                        <form id="form-section4" action="mail/send_mail.php" method="post">
                            <p class="texto-formulario2">INFÓRMATE Y CONTACTA</p>
                            <div class="form-group radj-reg-14 form-group-l">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="teléfono*">
                            </div>
                            <div class="form-group radj-reg-14 form-group-r">
                                <input type="email" name="email" class="form-control" id="email" placeholder="email">
                            </div>
                            <div class="form-group radj-reg-14">
                                <textarea class="form-control" name="comment" id="comment" placeholder="consulta"></textarea>
                            </div>

                            <div class="form-group form-check">
                                <div class="checkbox">
                                <input class="form-check-input styled" type="checkbox" id="aviso3" name="aviso" required>
                                <label class="radj-med-10 agree" for="aviso3">
                            Acepto las <a href="terminos_y_condiciones">términos y condiciones</a>, <a href="politica_de_privacidad" target="_blank">política de privacidad</a> y <a href="acuerdo_de_procesamiento_de_datos" target="_blank">procesamiento de datos</a>.*
                        </label>
                                </div>

                            </div>
                            <div class="form-group form-check">
                                <div class="checkbox">
                        <input type="checkbox" class="form-check-input" id="news3" name="news">
                        <label class="radj-med-10 agree" for="news3">
                            Deseo recibir información acerca de los servicios de Ayuda T un lugar todas las soluciones por medios electrónicos.
                        </label>
                    </div>

                            </div>
                            <input type="hidden" name="posicion" value="<?php echo $_SERVER['REQUEST_URI']?>/proyectos">
                            <button type="submit" id="submit3" class="btn btn-primary">QUIERO SABER MÁS</button>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <!--        ----------------------------------FIN FORMULARIO 3 --------------------------------------->






    </div>
    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->

    <script>
        $("#submit").bind('click', function() {
            var email = $("#email").val();
            var news = $("#news");
            //          if(email!=null && email!="") {
            //          
            ////              if(!news.is(':checked')) {
            ////                  console.log("No Check");
            ////                   $.ajax({
            ////                  url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/08fnjxubq7',
            ////                  data: {
            ////                      'groups[]': '38',
            ////                      'email': email
            ////                  },
            ////                  type: "POST",
            ////                  headers: {
            ////                      'Content-type': 'application/x-www-form-urlencoded'
            ////                  }
            ////              }).always(function () {
            ////                  $("#contactform").submit();
            ////              });
            ////              }else{
            //              
            //              $.ajax({
            //                  url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/08fnjxubq7',
            //                  data: {
            //                      'groups[]': '11',
            //                      'email': email
            //                  },
            //                  type: "POST",
            //                  headers: {
            //                      'Content-type': 'application/x-www-form-urlencoded'
            //                  }
            //              }).always(function () {
            //                  $("#contactform").submit();
            //              });
            ////              }
            //          } else
            $("#form-section1").submit();





        });

    </script>


    <?php
include 'required/footer.php';
?>
