<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    

   
    <title>Asesoría gestoria online líder en España - Ayuda-T Pymes</title>
    <meta property="og:title" content="Asesoría gestoria online líder en España - Ayuda-T Pymes" />
    <meta property="og:url" content="https://www.ayudatpymes.com/contacto-asesoria-ayuda-t-pymes" />
    <meta property="og:description" content="Más que una asesoría online o gestoría online para autónomos, pymes y empresas. La Gestoría asesoría barata que cambió la asesoría low cost." />
    <meta name="description" content="Más que una asesoría online o gestoría online para autónomos, pymes y empresas. La Gestoría asesoría barata que cambió la asesoría low cost." />
    
    <link rel="stylesheet" href="css/style-contacto.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            
    
    <span class="breadcrumbs"><span> | </span>CONTACTO</span>
    

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="contacto-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                   CONTACTO
                </h1>
                <h2>Infórmate sin compromiso!</h2>
                
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-1 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-orange.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->
 <div class="row slide-autonomo justify-content-sm-center">
            <h2>¿EN QUÉ PODEMOS AYUDARTE?</h2>
        </div>
        
        
        
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesoria justify-content-sm-center">
                                <img class="etiqueta-derecha" src="img/etiqueta.png" alt="etiquetas"/>
    <img class="etiqueta-izq" src="img/etiqueta.png" alt="etiquetas" />
                <div class="col-lg-5 contenido-gestoria">
                    <div class="row justify-content-sm-center">
                        
                                <p>Póngase en contacto mediante nuestro teléfono gratuito <a href="tel:900100162">900 100 162</a>. El horario de atención telefónica es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de <span>Lunes a Jueves de 9-18 y los Viernes de 9-14</span>. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>
                                <a class="boton-telefono" href="tel:900100162">900 100 162</a>
 </div>
                            
                        
                        

                    </div>
                </div>


            </div>



      
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->
 <script>
$("#submit").bind('click',function(){
          var email = $("#email").val();
          var news = $("#news");
//          if(email!=null && email!="") {
//          
////              if(!news.is(':checked')) {
////                  console.log("No Check");
////                   $.ajax({
////                  url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/08fnjxubq7',
////                  data: {
////                      'groups[]': '38',
////                      'email': email
////                  },
////                  type: "POST",
////                  headers: {
////                      'Content-type': 'application/x-www-form-urlencoded'
////                  }
////              }).always(function () {
////                  $("#contactform").submit();
////              });
////              }else{
//              
//              $.ajax({
//                  url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/08fnjxubq7',
//                  data: {
//                      'groups[]': '11',
//                      'email': email
//                  },
//                  type: "POST",
//                  headers: {
//                      'Content-type': 'application/x-www-form-urlencoded'
//                  }
//              }).always(function () {
//                  $("#contactform").submit();
//              });
////              }
//          } else
              $("#form-section1").submit();
		  
		 



      });


  </script>
    

    <?php
include 'required/footer.php';
?>