<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    <title>ACUERDO DE PROCESAMIENTO DE DATOS</title>
    <meta property="og:title" content="ACUERDO DE PROCESAMIENTO DE DATOS" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Acuerdo de procesamiento de datos AyudaTPymes" />
    <meta name="description" content="Acuerdo de procesamiento de datos AyudaTPymes" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
<style>
#legal-page .container {
    margin-top: 150px;
}

    #legal-page{
        font-family: 'Rajdhani';
        font-weight: normal;
        font-size: 14px;
        color: #808183;
    }
    #legal-page h1{
        text-align: center;
        color:#00AEC8;
        margin-bottom: 30px;
    }
</style>
    <div id="legal-page">
        <div class="container">
            <h1>ACUERDO DE PROCESAMIENTO DE DATOS</h1>
            <div align="justify">
<p>Este Acuerdo de Procesamiento de Datos ("APD") es la base de la relación entre tú, el cliente, como controlador de datos y <strong> Ayuda-T </strong> , el proveedor del servicio, como procesador de datos bajo la legislación de protección de datos, más concretamente, el Reglamento General de Protección de Datos ("RGPD").</p>
<p>Este es un acuerdo importante que constituye la base contractual para que procesemos los datos en tu nombre. Explica cómo pueden procesarse tus datos y su propósito. Procesamos tus datos personales sólo según lo requerido y según tus indicaciones, tal como se describe en este acuerdo.</p>
<p>Debido a nuestro volumen de clientes, sería imposible celebrar acuerdos firmados individualmente con todos nuestros usuarios. También es nuestro deseo que, al facilitar la celebración de este acuerdo (APD), se asegure que la aceptación de los nuevos Términos, en cumplimiento del RGPD, te tome menos tiempo como propietario que debe su principal ocupación a un negocio.</p>
<p>Este APD te asegura que nosotros, como tu procesador de datos, cumplimos con los requisitos estipulados en el RGPD. Además, puedes estar seguro de que mantenemos los acuerdos necesarios con terceras partes. Los detalles de tu empresa se completan automáticamente en tu cuenta cuando aceptas los Términos y Condiciones y la Política de Privacidad, incluido este APD. Tus datos siempre representarán la información más actualizada que nos hayas proporcionado. El APD se detalla a continuación para tu información.</p>
<p> </p>
<p>Acuerdo de Procesamiento de Datos</p>
<p>entre:</p>
<p>Nombre del cliente (“el cliente” o “controlador de datos”, en adelante) [Esta información se completará automáticamente una vez que hayas completado el registro]</p>
<p>y</p>
<p>“ <strong> Ayuda-T un Lugar todas las soluciones S.L.” </strong> , con CIF: B93011708, domicilio en Polígono industrial salinas de San José, Avenida Isaac Newton Edif. 287, CP. 11.500, El Puerto de Santa María (CÁDIZ) (“ <strong> Ayuda-T </strong> ” o “procesador de datos”, en adelante)</p>
<p>cada uno una "parte"; juntos "las partes",</p>
<p>HAN ACORDADO los términos de este Acuerdo de Procesamiento de Datos (en adelante "APD" o "Acuerdo") sobre protección de datos personales con respecto al procesamiento de datos personales cuando el cliente actúa como controlador de datos y <strong> Ayuda-T </strong> como procesador de datos para cumplir con las obligaciones de servicio descritas en el acuerdo de servicios (detallado más abajo). Como parte del cumplimiento de estas obligaciones de servicio, <strong> Ayuda-T </strong> procesará ciertos datos personales en nombre del controlador de datos, de conformidad con los términos de este contrato. Cada parte acuerda y se asegurará de que los términos de este contrato también sean plenamente aplicables a sus filiales, las cuales puedan estar involucradas en las operaciones de procesamiento de datos personales para el proyecto definido en el acuerdo de servicios. Específicamente, <strong> Ayuda-T </strong> se asegurará de que todos los subprocesadores operen dentro de los mismos términos que este Acuerdo al procesar los datos personales del cliente.</p>
<p> </p>
<p><strong> Introducción y definiciones: </strong></p>
<p>Los datos personales se definen como cualquier información relacionada con un interesado por la que se puede identificar en particular, directa o indirectamente, por referencia a un identificador como un nombre, un número de identificación, datos de ubicación, un identificador en línea o uno o más factores específicos de la identidad física, fisiológica, genética, mental, económica, cultural o social de esa persona física o jurídica (cuando corresponda).</p>
<p>Todas las demás definiciones mencionadas en este documento, incluidos los términos controlador de datos y procesador de datos, están determinadas por las leyes de protección de datos correspondientes, incluido el Reglamento General de Protección de Datos de la UE 2016/679 de 27 de abril de 2016 (en adelante, "RGPD").</p>
<p>Los datos personales confidenciales no se consideran procesados bajo el servicio de aplicación ofrecido por el procesador de datos y, por lo tanto, están excluidos de los términos de este Acuerdo. .</p>
<p>Al suscribirte para utilizar el programa <strong> Ayuda-T </strong> y aceptar los Términos y Condiciones, incluida la Política de Privacidad y este APD, las partes acuerdan bajo todas las leyes nacionales de protección de datos y bajo el RGPD que este Acuerdo rige la relación entre el controlador de datos y el procesador de datos, determinando el procesamiento de los datos personales por parte de <strong> Ayuda-T </strong> de los datos del cliente. Este Acuerdo tiene prioridad, a menos que haya sido reemplazado por otro APD firmado que comunica su primacía sobre este Acuerdo.</p>
<p>El propósito del procesamiento de <strong> Ayuda-T </strong> de datos personales para el cliente es garantizar el pleno uso del servicio por parte del cliente y permitir que este Acuerdo se cumpla. <strong> Ayuda-T </strong> garantizará que se mantenga la seguridad suficiente de los datos personales en todo momento.</p>
<p>Ambas partes confirman su autoridad para firmar el Acuerdo al hacerlo.</p>
<p> </p>
<p><strong> Responsabilidades del procesador de datos: </strong></p>
<p>El procesador de datos debe tratar todos los datos personales en nombre del controlador de datos y seguir sus instrucciones. Al celebrar este Acuerdo, <strong> Ayuda-T </strong> (y cualquier subprocesador con el que el procesador de datos tenga un acuerdo legal para ofrecer los servicios) tiene instrucciones de procesar los datos personales del cliente de la siguiente manera:</p>
<p>i) De acuerdo con todas las leyes nacionales y europeas; ii) cumplir con sus obligaciones bajo los términos de la aplicación de servicio; iii) según instrucciones del controlador de datos; iv) como se describe en este Acuerdo.</p>
<p>Como parte que provee la aplicación, se requiere que el procesador de datos siempre proporcione al cliente las soluciones adecuadas para acompañar el desarrollo continuo de su negocio mediante el uso del servicio. El procesador de datos rastrea cómo el cliente usa la aplicación para hacer las mejores sugerencias, proporcionar servicios relevantes en todo momento y comprometerse a enviar las comunicaciones más precisas con el fin de lograr la facilidad de uso y la satisfacción del cliente. En la medida en que el procesamiento de los datos personales de la aplicación forma parte de esto, se procesan solo de acuerdo con este APD y la ley aplicable y se comparten solo cuando sea necesario para proporcionar una mejor experiencia al cliente.</p>
<p>Teniendo en cuenta la tecnología disponible y el coste de implementación, así como el alcance, el contexto y el propósito del procesamiento, se requiere que el procesador de datos tome todas las medidas razonables, incluidas medidas técnicas y organizativas, para garantizar un nivel suficiente de seguridad en relación con el riesgo y la categoría de datos personales a proteger. El procesador de datos deberá ayudar al controlador de datos con las medidas técnicas y organizativas apropiadas según sea necesario y teniendo en cuenta la naturaleza del tratamiento y la categoría de información disponible para el procesador de datos para garantizar el cumplimiento de las obligaciones del controlador de datos bajo las leyes de protección de datos aplicables.</p>
<p>El procesador de datos notificará al controlador de datos sin demora indebida si el procesador de datos tiene conocimiento de una violación de la seguridad.</p>
<p>Además, el procesador de datos debe, en la medida de lo posible y de forma legal, informar al controlador de datos si se eleva una solicitud de datos (solicitud de acceso a datos) por parte de los organismos que deben proporcionarla. El procesador de datos responderá a tales solicitudes una vez que el controlador de datos lo autorice a hacerlo. El procesador de datos tampoco divulgará información sobre este Acuerdo a menos que el procesador de datos esté obligado por ley a hacerlo, como por orden judicial.</p>
<p>Si el controlador de datos requiere información o ayuda con respecto a la seguridad de los datos, la documentación o la información sobre cómo el procesador de datos procesa los datos personales en general, puede solicitar esta información del procesador.</p>
<p>El procesador de datos, sus empleados y cualquier afiliado o socio garantizará la confidencialidad en relación con los datos personales procesados en virtud del Acuerdo. Esta disposición continúa aplicándose después de la terminación del Acuerdo, independientemente de la causa de su terminación.</p>
<p> </p>
<p><strong> Responsabilidades del controlador de datos: </strong></p>
<p>El controlador de datos confirma, al firmar este Acuerdo, que, al utilizar la aplicación, podrá procesar libremente sus datos según todos los requisitos legales de protección de datos, incluido el RGPD. El controlador da su consentimiento explícito para el procesamiento de sus datos personales en todo momento al utilizar el servicio.</p>
<p>El controlador de datos puede revocar este consentimiento en cualquier momento, pero al hacerlo termina el Acuerdo vigente y el procesador de datos ya no podrá ofrecer el servicio.</p>
<p>El cliente tiene una base legal para procesar los datos personales con el procesador de datos (incluidos los subprocesadores) con el uso de los servicios de <strong> Ayuda-T. </strong></p>
<p>El controlador de datos es responsable en todo momento de la precisión, integridad, contenido y fiabilidad de los datos personales procesados por el procesador de datos. Ambos has cumplido todos los requisitos obligatorios en relación con la notificación u obtención de permiso de las autoridades públicas pertinentes con respecto al procesamiento de datos personales. Además, ambos han cumplido sus obligaciones de divulgación con las autoridades pertinentes con respecto al procesamiento de datos personales de acuerdo con toda la legislación de protección de datos aplicable.</p>
<p>El controlador de datos debe tener una lista precisa de las categorías de datos personales que procesa, particularmente si dicho procesamiento difiere de las categorías enumeradas por el procesador de datos en el Anexo A.</p>
<p> </p>
<p><strong> Acuerdo para la transferencia de datos y el uso de subcontratistas: </strong></p>
<p>Para proporcionar el servicio al controlador de datos, el procesador de datos usa subcontratistas. Estos subcontratistas pueden ser proveedores externos tanto dentro como fuera de la UE / EEE. El procesador de datos garantiza que todos los subcontratistas cumplan con las obligaciones y los requisitos de este Acuerdo; específicamente, que su nivel de protección de datos cumple con el estándar requerido por las leyes de protección de datos relevantes. Si una jurisdicción queda fuera de la UE / EEE y no está en la lista aprobada por la Comisión Europea de niveles de protección de datos satisfactorios bajo el RGPD, entonces se establece un acuerdo específico entre <strong> Ayuda-T </strong> y dicho subcontratista para asegurar que mantendrá todos los datos personales según los requisitos de las leyes vigentes de protección de datos de la UE.</p>
<p>Este Acuerdo constituye el consentimiento previo específico y explícito del controlador de datos para el uso por parte del procesador de datos de subcontratistas de procesadores de datos, que a veces pueden estar fuera de la UE / EEE o de territorios aprobados por la Comisión Europea.</p>
<p>El controlador de datos puede revocar este consentimiento en cualquier momento, pero al hacerlo termina la vigencia del Acuerdo y el procesador de datos ya no podrá ofrecer el servicio.</p>
<p>Si se establece un subdirector o almacena datos personales fuera de los territorios aprobados por la UE / EEE o la Comisión Europea, el procesador de datos tiene la responsabilidad de garantizar una base satisfactoria para transferir datos personales a un tercer país en nombre del controlador de datos, incluido el uso de los contratos estándar de la Comisión Europea o medidas específicas que hayan sido aprobadas previamente por la Comisión Europea.</p>
<p>El controlador de datos debe ser informado antes de que el procesador de datos reemplace a sus subcontratistas. El controlador de datos puede oponerse a un nuevo subprocesador que procesa sus datos personales en nombre del procesador de datos, pero solo si el subprocesador no procesa los datos de acuerdo con la legislación de protección de datos vigente. El procesador de datos puede demostrar el cumplimiento proporcionando al controlador de datos dando acceso a la evaluación de protección de datos realizada por el procesador de datos.</p>
<p>Si el controlador de datos aún se opone al uso del subcontratista, puede finalizar su suscripción al servicio, sin el período de notificación habitual requerido, y luego asegurarse de que el subcontratista no deseado no procese sus datos personales.</p>
<p> </p>
<p><strong> Duración del Acuerdo: </strong></p>
<p>El Acuerdo sigue siendo válido siempre que el procesador de datos procese datos personales con el uso del procesador de datos de la aplicación de servicio y a menos que sea reemplazado por otro APD firmado que comunique su primacía sobre este Acuerdo.</p>
<p> </p>
<p><strong> Terminación del Acuerdo: </strong></p>
<p>Tras la finalización de cualquier suscripción, cuando el Acuerdo finalice, el procesador de datos eliminará todos los datos personales, excepto aquellos que se requiera que conserve según los requisitos legales aplicables y en tal caso se guardarán de acuerdo con las garantías técnicas y organizativas.</p>
<p>El controlador de datos tiene capacidad completa para recuperar todos sus datos personales de la aplicación de servicio. Si el controlador de datos solicita asistencia para la recuperación de datos, los costes asociados se determinarán de común acuerdo entre las partes y se basarán en la complejidad del proceso solicitado y el tiempo para cumplirlo en el formato elegido.</p>
<p> </p>
<p><strong> Cambios en el Acuerdo: </strong></p>
<p>Los cambios en el Acuerdo deben adjuntarse en un anexo separado del Acuerdo. Si alguna de las disposiciones del Acuerdo se considera inválida, esto no afecta las disposiciones restantes. Las partes reemplazarán las disposiciones inválidas con una disposición legal que refleje el propósito de la disposición inválida.</p>
<p> </p>
<p><strong> Auditorías: </strong></p>
<p>El controlador de datos tiene derecho a iniciar una revisión de las obligaciones del procesador de datos bajo el acuerdo una vez al año. Si se requiere que el procesador de datos lo haga conforme a la legislación aplicable, las auditorías pueden repetirse una vez al año. Se debe proporcionar un plan de auditoría detallado que describa el alcance, la duración y la fecha de inicio al menos cuatro semanas antes de la fecha de inicio propuesta. Las partes deciden juntas si un tercero debe realizar la auditoría. Sin embargo, el controlador de datos puede permitir que el procesador de datos tenga la revisión de seguridad realizada por un tercero neutral a elección del procesador de datos, si se trata de un entorno de procesamiento donde se procesan datos de múltiples controladores de datos.</p>
<p>Si el alcance propuesto de la auditoría sigue un informe de certificación ISAE, ISO o similar realizado, dentro de los doce meses anteriores, por un auditor tercero calificado y el procesador de datos confirma que no ha habido cambios importantes en las medidas bajo revisión, esto satisfará cualquier solicitud recibida dentro de dicho plazo. Las auditorías no pueden interferir irracionalmente con las actividades habituales del procesador de datos. El controlador de datos es responsable de todos los costes asociados con su solicitud de revisión de auditoría.</p>
<p> </p>
<p><strong> Responsabilidades y jurisdicciones: </strong></p>
<p>La responsabilidad por acciones derivadas del incumplimiento de las disposiciones de este Acuerdo se rige por las disposiciones de responsabilidad e indemnización en los términos de la suscripción en la sección 13. Esto también se aplica a cualquier violación por parte de los subprocesadores del procesador de datos.</p>
<p>Este Acuerdo se rige por los tribunales del Reino de España, que tendrán jurisdicción exclusiva para determinar cualquier disputa sobre el mismo.</p>
<p> </p>
<p><strong> Anexo A. Categorías de información personal y categorías de procesamiento habitual </strong></p>
<p><strong> A. Categorías de información personal (la lista no es exhaustiva): </strong></p>
<p>Nombre.</p>
<p>Dirección.</p>
<p>Número(s) de teléfono.</p>
<p>Dirección(es) de correo electrónico.</p>
<p>Dirección(es).</p>
<p>Cualquier número de cuenta y/o detalles bancarios.</p>
<p><strong> B. Categorías de procesamiento habituales (la lista no es exhaustiva): </strong></p>
<p>Los empleados del controlador de datos.</p>
<p>Contactos del controlador de datos (teléfono / correo electrónico / direcciones / etc.).</p>
<p>Los clientes del controlador de datos.</p>
<p>La información bancaria del controlador de datos.</p>
<p>Los empleados de sus clientes.</p>
<p>Contactos de sus clientes (teléfono / correo electrónico / direcciones / etc.).</p>
<p>Los clientes de sus clientes.</p>
<p>Información bancaria de los clientes de sus clientes.</p>
</div>


</div>


        </div>







    <?php
include 'required/footer.php';
?>
