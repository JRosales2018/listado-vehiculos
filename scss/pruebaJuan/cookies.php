﻿<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    <title>POLÍTICA DE COOKIES</title>
    <meta property="og:title" content="POLÍTICA DE COOKIES" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Política de cookies AyudaTPymes" />
    <meta name="description" content="Política de cookies AyudaTPymes" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
<style>
#legal-page .container {
    margin-top: 150px;
}

    #legal-page{
        font-family: 'Rajdhani';
        font-weight: normal;
        font-size: 14px;
        color: #808183;
    }
    #legal-page h1{
        text-align: center;
        color:#00AEC8;
        margin-bottom: 30px;
    }
</style>
    <div id="legal-page">
        <div class="container page">
<h1>Política de cookies</h1>
  Una cookie es un fichero que se descarga en el ordenador/smartphone/tablet del usuario al acceder a determinadas páginas web para almacenar y recuperar información sobre la navegación que se efectúa desde dicho equipo. Para conocer más información sobre las cookies.
  <br/><br/>
  En este sitio web se usan las siguientes cookies que se detallan en el cuadro siguiente:
  <ul>
    <li>cookies estrictamente necesarias para la prestación de determinados servicios solicitados expresamente por el usuario: si se desactivan estas cookies, no podrá recibir correctamente nuestros contenidos y servicios; y</li>
    <li>cookies analíticas (para el seguimiento y análisis estadístico del comportamiento del conjunto de los usuarios), publicitarias (para la gestión de los espacios publicitarios en base a criterios como la frecuencia en la que se muestran los anuncios) y comportamentales (para la gestión de los espacios publicitarios según el perfil específico del usuario): si se desactivan estas cookies, el sitio web podrá seguir funcionando sin perjuicio de que la información captada por estas cookies sobre el uso de nuestra web y sobre el éxito de los anuncios mostrados en ella permite mejorar nuestros servicios y obtener ingresos que nos permiten ofrecerle de forma gratuita muchos contenidos.</li>
  </ul>
  <br/>
  <div class="cookies">
    <table>
      <thead>
        <tr>
          <th colspan="2">Cookies</th>
          <th>Informaci&oacute;n (*)</th>
          <th>Finalidad</th>
          <th>Output (*)</th>
        </tr>
        <tr>
          <th colspan="5" class="propias">COOKIES PROPIAS</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="2" class="necesarias"><strong>Estrictamente necesarias</strong></td>
          <td >Sesi&oacute;n, registro, datos  del carrito de compra, datos para distinguir los servicios gratuitos de los  servicios de pago</td>
          <td >Gesti&oacute;n del registro<br>Prestar servicios de la sociedad de la informaci&oacute;n solicitados por el  usuario y conforme a los t&eacute;rminos y condiciones aplicables</td>
          <td><strong>N/A</strong></td>
        </tr>
        <tr>
          <td colspan="2" class="publicidad"><strong>Publicidad</strong></td>
          <td>Frecuencia con la que se  muestran los anuncios</td>
          <td>Mejorar y gestionar la  exposici&oacute;n de anuncios publicitarios al usuario</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th colspan="5" class="terceros"><strong>COOKIES DE TERCEROS<br>
          </strong>(la informaci&oacute;n recogida a continuaci&oacute;n ha sido facilitada por esos  terceros)</th>
          </tr>
        <tr>
          <td  rowspan="3" class="analiticas" style="width:100px;"><strong>Anal&iacute;ticas</strong></td>
          <td class="analiticasBis"><p><a href="http://www.nielsen.com/content/corporate/policy/en/cookie-policy.html" target="_blank">Nielsen SiteCensus<br>(Nielsen <span class="asterisco">[*]</span>)</a></p></td>
          <td>N&uacute;mero de visitas,  p&aacute;ginas o secciones visitadas, tiempo de navegaci&oacute;n, sitios visitados antes de  entrar en esta p&aacute;gina, detalles sobre los navegadores usados</td>
          <td>Informes estad&iacute;sticos  sobre el tr&aacute;fico del sitio web, su audiencia total y la audiencia en una determinada  campa&ntilde;a publicitaria</td>
          <td><a href="http://www.nielsen.com/content/corporate/policy/en/cookie-policy.html" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td class="analiticasBis"><p><a href="http://www.adobe.com/es/privacy/policy.html#cookies" target="_blank">Adobe Analytics -Omniture-</a> <br>y <br><a href="http://www.adobe.com/es/privacy/analytics.html" target="_blank">Adobe</a></p></td>
          <td>Webs visitadas y tiempo  de navegaci&oacute;n, web que conduce al sitio, b&uacute;squedas realizadas</td>
          <td>Informes estad&iacute;sticos sobre como los usuarios  encuentran la p&aacute;gina web, c&oacute;mo la utilizan y si esta funciona correctamente</td>
          <td><a href="http://www.adobe.com/es/privacy/opt-out.html#4" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td class="analiticasBis"><p><a href="http://www.scorecardresearch.com/privacy.aspx" target="_blank">Score Card Research<br>(Full Circle Studies, Inc.)</a></p></td>
          <td>Tiempo de navegaci&oacute;n,  sitio web visitado e informaci&oacute;n relacionada con la propia navegaci&oacute;n</td>
          <td>Crear informes sobre  patrones generales de visitas al sitio web </td>
          <td><a href="http://www.scorecardresearch.com/privacy.aspx" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td rowspan="3" class="publicidad"><strong>Publicidad</strong></td>
          <td class="publicidadBis"><p><a href="http://www.cxense.com/privacy" target="_blank">CXense<br>(CXense, AS) </a></p></td>
          <td>Preferencias del  usuario, informaci&oacute;n sobre la interacci&oacute;n con el sitio web (servicio  solicitado, fecha y hora), tipo de navegador y lenguaje, localizaci&oacute;n  geogr&aacute;fica</td>
          <td rowspan="3">Mejorar y gestionar la  exposici&oacute;n de anuncios publicitarios al usuario evitando que aparezcan anuncios  que ya ha visto</td>
          <td><a href="http://www.cxense.com/privacy" target="_blank">Ver</a><a href="http://www.cxense.com/preferences?optOutStatus=false" target="_blank"></a></td>
        </tr>
        <tr>
          <td class="publicidadBis"><p><a href="https://support.google.com/adsense/answer/2839090?hl=en" target="_blank">Doubleclick<br>(Google, Inc.) </a></p></td>
          <td>Informaci&oacute;n sobre los  anuncios que han sido mostrados a un usuario, sobre los que le interesan y sobre  si visita la p&aacute;gina web del anunciante</td>
          <td><a href="https://support.google.com/adsense/answer/2839090?hl=en" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td class="publicidadBis"><p><a href="http://www.admeta.com/upload/Privacy%20Policy.pdf" target="_blank">Admeta<br>(Admeta AB)</a></p></td>
          <td>Preferencias del  usuario, p&aacute;ginas visitas y detalles del equipos del usuario, datos de flujo de  clics</td>
          <td><a href="http://www.admeta.com/upload/Privacy%20Policy.pdf" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td rowspan="2" class="comportamental"><strong>Publicidad comportamental</strong></td>
          <td class="comportamentalBis"><p><a href="http://www.audiencescience.com/node/16" target="_blank">Audience Science<br>(AudienceScience, Inc.) </a></p></td>
          <td>Informaci&oacute;n sobre las  preferencias o intereses en productos y/o servicios recogida durante la  navegaci&oacute;n</td>
          <td>Mostrar anuncios  personalizados seg&uacute;n el comportamiento o las preferencias mostradas por el  usuario y mejorar las campa&ntilde;as publicitarias</td>
          <td><a href="http://www.audiencescience.com/node/16" target="_blank">Ver</a></td>
        </tr>
        <tr>
          <td class="comportamentalBis"><p><a href="http://www.mediamind.com/privacy-policy" target="_blank">Media Mind<br>(Digital Generation, Inc.) </a></p></td>
          <td>Direcci&oacute;n IP, tipo de  sistema operativo, tipo de navegador, p&aacute;ginas visitadas</td>
          <td>Personalizar las  campa&ntilde;as publicitarias y mostrar anuncios que se ajusten a las preferencias de  los usuarios</td>
          <td><a href="HTTP://WWW.MEDIAMIND.COM/PRIVACY-POLICY" target="_blank">Ver</a></td>
        </tr>
      </tbody>
    </table>
  </div>
  <br/>
  (*) La información obtenida a través de estas cookies, referida al equipo del usuario, podrá ser combinada con sus datos personales sólo si Ud. está registrado en este sitio web.
  <br/><br/>
  (**) DESACTIVACIÓN DE COOKIES. El usuario podrá -en cualquier momento- elegir qué cookies quiere que funcionen en este sitio web mediante:
  <br/><br/>
  <ul>
    <li>la configuraci&oacute;n del <strong>navegador</strong>; por ejemplo:<br /><br />
      <ul class="normal">
        <li><strong>Chrome</strong>, desde <a href="http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647" target="_blank">http://support.google.com/chrome/bin/answer.py?hl=es&amp;answer=95647 </a></li>
        <li><strong>Explorer</strong>, desde <a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9" target="_blank">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>
        <li><strong>Firefox</strong>, desde <a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>
        <li>Safari, desde <a href="http://support.apple.com/kb/ph5042" target="_blank">http://support.apple.com/kb/ph5042</a><br /><br /></li>
      </ul>
    </li>
    <li>los sistemas de opt-out específicos indicados en la tabla anterior respecto de la cookie de que se trate (estos sistemas pueden conllevar que se instale en su equipo una cookie "de rechazo" para que funcione su elección de desactivación); o<br /><br /></li>
    <li>otras herramientas de terceros, disponibles on line, que permiten a los usuarios detectar las cookies en cada sitio web que visita y gestionar su desactivación (por ejemplo, Ghostery: <a href="http://www.ghostery.com/privacy-statement" target="_blank">http://www.ghostery.com/privacy-statement</a>, <a href="http://www.ghostery.com/faq" target="_blank">http://www.ghostery.com/faq</a>).</li>
  </ul>
  <br />
  Ayuda T Pymes no se hace responsable del contenido y veracidad de las políticas de privacidad de los terceros incluidas en esta política de cookies.
  Si tiene dudas sobre esta política de cookies, puede contactar con nosotros al respecto en la zona de contacto.
</div>


        </div>







    <?php
include 'required/footer.php';
?>
