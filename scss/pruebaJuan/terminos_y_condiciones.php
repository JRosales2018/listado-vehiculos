<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    <title>TÉRMINOS Y CONDICIONES</title>
    <meta property="og:title" content="TÉRMINOS Y CONDICIONES" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Términos y condiciones AyudaTPymes" />
    <meta name="description" content="Términos y condiciones AyudaTPymes" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
<style>
#legal-page .container {
    margin-top: 150px;
}

    #legal-page{
        font-family: 'Rajdhani';
        font-weight: normal;
        font-size: 14px;
        color: #808183;
    }
    #legal-page h1{
        text-align: center;
        color:#00AEC8;
        margin-bottom: 30px;
    }
</style>
    <div id="legal-page">
        <div class="container">
            <h1>TÉRMINOS Y CONDICIONES</h1>
            <div align="justify">
<p>Los siguientes Términos y Condiciones y Política de Privacidad se aplicarán inmediatamente a los usuarios que se registren en <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u> a partir del 25 de mayo de 2018. Para todos los usuarios existentes, entrarán en vigor a partir del 25 de mayo de 2018, fecha de entrada en vigor del RGPD, y debes dar tu consentimiento para continuar usando nuestro servicio. Ahora bien, ya puedes aceptar, a partir de ahora, estos nuevos Términos en cualquier momento al iniciar sesión en tu cuenta.</p>
<p><strong>1. Objeto y alcance de estos términos</strong></p>
<p>(1) Estos Términos y Condiciones rigen los derechos y obligaciones en relación con el uso de los servicios del proveedor <strong>Ayuda-T un Lugar todas las soluciones S.L.”</strong>, con CIF: B93011708, inscrita en el Registro Mercantil de Málaga, Tomo 4666, folio 17, inscripción 1º con hoja MA-102143 y dirección en Plaza Cristóbal Toral, nº 1, 2ºB, Antequera, Málaga, España, con domicilio en Polígono industrial salinas de San José, Avenida Isaac Newton Edif. 287, CP. 11.500, El Puerto de Santa María (CÁDIZ), España (en adelante, <strong>Ayuda-T</strong> o el proveedor de servicios), y el usuario en relación con el uso del servicio, que generalmente se puede acceder a través de internet en los sitios web de <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u>o en otras URL del proveedor del servicio.</p>
<p>(2) El servicio del proveedor consiste esencialmente en la concesión de la posibilidad de utilizar el servicio a través de internet en servidores que se encuentran dentro del ámbito de influencia del proveedor del servicio, al que el usuario, en la medida de lo necesario, recibe derechos de acceso y uso. Al utilizar el Software como Servicio (SaaS, en sus siglas en inglés), el usuario podrá introducir datos y usar diferentes funciones. Los servicios de <strong>Ayuda-T</strong> incluyen, entre otros, el sitio web, la aplicación <strong>Ayuda-T Pymes</strong>, las aplicaciones móviles y el blog.</p>
<p>(3) Una condición para el uso sin problemas del servicio es una conexión de internet continua y fiable hasta los servidores del proveedor del servicio. Depende del cliente establecer esta conexión con la ayuda de su dispositivo.</p>
<p>(4) Solo se aplican los Términos y Condiciones del proveedor del servicio. El proveedor del servicio no reconoce términos conflictivos o diferentes que el usuario pueda condicionar, a menos que su validez haya sido explícitamente acordada por escrito. Estos Términos y Condiciones también se aplican si el proveedor del servicio, sabiendo que los términos y condiciones del usuario entran en conflicto o se desvían de estos Términos y Condiciones, le presta incondicionalmente sus servicios.</p>
<p>(5) Si el término "el sitio web del proveedor del servicio" se utiliza a continuación, se entiende el sitio web o los sitios web del proveedor de servicios, según los cuales el servicio, en el sentido del párrafo 1, se hace generalmente accesible por el proveedor del servicio en internet.</p>
<p><strong>2. Formalización del contrato</strong></p>
<p>(1) A menos que se acuerde explícitamente de forma individual, un contrato solo se formaliza al completar correctamente el proceso de registro mediante una confirmación del proveedor del servicio al usuario, en forma escrita por correo electrónico o mediante la prestación del servicio.</p>
<p>(2) El usuario tiene la oportunidad de imprimir el texto del contrato desde el sitio web durante el proceso de registro y antes de formalizar el contrato.</p>
<p>(3) El usuario no tiene derecho a formalizar un contrato. El proveedor del servicio es libre de rechazar cualquier oferta de un usuario para formalizar un contrato sin dar ninguna razón.</p>
<p>(4) Al suscribirse a los servicios prestados en cualquiera de los sitios web de <strong>Ayuda-T</strong>, aceptas los Términos de suscripción ("los Términos") como se detalla a continuación, incluido tu consentimiento para procesar y compartir tus datos personales según sea necesario para que <strong>Ayuda-T</strong> te proporcione su servicio, y siempre en conformidad con toda la legislación de protección de datos.</p>
<p>(5) La aceptación de comunicaciones adicionales solamente del grupo de empresas <strong>Ayuda-T</strong>, no es necesario para comenzar tu suscripción, pero se recomienda para la mejora de la experiencia. La información que se distribuye en tales comunicaciones es relevante para el negocio.</p>
<p>(6) Para utilizar nuestros servicios, debes aceptar completamente la política de privacidad junto con los términos. Aceptas activamente que has leído y comprendido los términos y la política de privacidad al aceptarlos.</p>
<p>(7) El requisito previo para el registro es que el usuario es legalmente competente y tiene la edad mínima de 18 años, es emprendedor o profesional autónomo o propietario de un negocio y utiliza los servicios exclusivamente para uso comercial. Los menores tienen prohibido registrarse. En el caso de una entidad legal, el registro debe ser llevado a cabo por una persona física que tenga capacidad legal ilimitada y esté autorizada como representante.</p>
<p>(8) En caso de que una empresa realice la contabilidad de terceros en nombre del contratista y el tercero se especifique como parte contratante, la empresa de contabilidad está obligada a informar al tercero con antelación sobre los Términos y Condiciones y suscribirse solo con el consentimiento y el poder de representación. Si esto no sucede, le da derecho al proveedor del servicio a rescindir el contrato extraordinariamente.</p>
<p><strong>3. Los servicios del proveedor de servicios</strong></p>
<p>(1) El proveedor de servicios proporciona a los usuarios diversos servicios de asesoría laboral, fiscal y contable.</p>
<p><a href="https://www.ayudatpymes.com/asesoria">Pymes y autónomos</a>, <a href="https://www.ayudatpymes.com/para-despachos">Para despachos</a>, <a href="https://www.ayudatpymes.com/emprendedores">Startups</a>, <a href="https://www.ayudatpymes.com/empresas-sociales">Empresas sociales</a>, <a href="https://www.ayudatpymes.com/juridica">Asesoría jurídica</a>, <a href="https://www.ayudatpymes.com/curso-online">Cursos de formación</a>, <a href="https://www.ayudatpymes.com/patentes-y-marcas">Patentes y marcas</a>, <a href="https://www.ayudatpymes.com/servicio-rgpd">RGPD</a>, <a href="https://www.ayudatpymes.com/cambio-nombre-coche/">Transferencias de vehículos</a>, <a href="https://www.ayudatpymes.com/alta-autonomos">Alta autónomos</a>, <a href="https://www.ayudatpymes.com/crear-empresa">Crear empresa</a>, <a href="https://www.ayudatpymes.com/abrir-sucursal">Abrir sucursal</a>, <a href="http://etlsport.es/">Asesoría para deportistas</a></p>
<p>(2) El contenido y el alcance de los servicios se rigen por los acuerdos contractuales respectivos, además, exclusivamente de acuerdo con las funcionalidades del servicio descrito en la celebración del contrato en el sitio web del proveedor del servicio.</p>
<p>(3) El proveedor del servicio puede ofrecer versiones de prueba en forma de accesos de prueba. Durante el período de prueba especificado, el uso es gratis. Si el usuario desea continuar utilizando los servicios una vez finalizado el período de prueba, se requiere un contrato de suscripción de pago.</p>
<p>(4) Los servicios prestados por el proveedor del servicio incluyen, en particular, las áreas de asesoría contable, laboral y fiscal, que se ofrecen durante un período determinado como parte de una "suscripción".</p>
<p>(5) Solo el usuario respectivo tiene derecho a usar el servicio. La transferencia de la cuenta de usuario a terceros o cualquier otra opción de uso ofrecida por el usuario a terceros quedan prohibidos y autoriza al proveedor del servicio a la terminación extraordinaria del contrato.</p>
<p><strong>4. Obligaciones de los usuarios</strong></p>
<p>(1) El usuario está obligado a proporcionar información veraz sobre sí mismo o su empresa, en relación con el uso del servicio.</p>
<p>(2) Al utilizar el servicio, el usuario está obligado a cumplir con las leyes aplicables y a abstenerse de cualquier actividad que afecte o presione excesivamente la operación del servicio o la infraestructura técnica subyacente.</p>
<p>3) El usuario no está autorizado a transmitir sus datos de inicio de sesión a terceros. El usuario está obligado a tratar sus datos de inicio de sesión cuidadosamente y evitar el uso indebido de los datos de inicio de sesión por parte de terceros.</p>
<p>(4) El usuario es el único responsable de cumplir con sus obligaciones de conservación. Se asegurará de que sus documentos y datos se mantengan legales, cuando sea necesario, y que las autoridades fiscales tengan el acceso necesario a ellos.</p>
<p><strong>5. Aviso sobre el derecho de cancelación</strong></p>
<p>(1) El proveedor de servicios ofrece sus servicios exclusivamente a autónomos y empresas.</p>
<p>(2) Para todo uso previsto de los servicios prestados por el proveedor del servicio, no existe el derecho de cancelación.</p>
<p><strong>6. Duración del contrato</strong></p>
<p>(1) La suscripción comienza con la formalización del contrato y está en vigor indefinidamente.</p>
<p>(2) Cualquier acceso de prueba finaliza automáticamente al final del período de prueba respectivo. No se requiere un aviso por separado para el acceso de prueba.</p>
<p><strong>7. Precios y condiciones de pago, bloqueo de la cuenta, eliminación de cuenta, ajustes de precios</strong></p>
<p>(1) El proveedor de servicios ofrece sus servicios en diferentes variantes de pago. Los precios acordados se pueden encontrar en la información de precios y pago vigentes.</p>
<p>(2) El pago de una suscripción se realiza mensual o anualmente, según la duración del contrato ofrecido y elegido por el usuario, mediante tarjeta de crédito (Visa, Mastercard) o cuenta bancaria. El período de facturación se realiza por un mes o un año por adelantado, desde la fecha en que el usuario contrata los servicios. El proveedor del servicio se reserva el derecho de introducir la posibilidad de contratar suscripciones para diferentes períodos (por ejemplo, trimestralmente) o de presentar servicios relacionados que ofrezcan otros modelos de facturación (por ejemplo, basado en el uso).</p>
<p>(3) El adeudo de los respectivos cargos de la suscripción del usuario se abonará y posteriormente se remitirá la factura y se deducirá o retirará de la tarjeta de crédito o cuenta bancaria de forma mensual o anual, hasta la terminación.</p>
<p>(4) <strong>Ayuda-T</strong> se reserva el derecho de cambiar la entidad de facturación a una subsidiaria diferente del <strong>Grupo Ayuda-T</strong> según sea necesario.</p>
<p>(5) No se realizará un reembolso de las contribuciones mensuales o anuales en caso de terminación prematura por parte del usuario. Tras la rescisión del contrato, la versión del producto puede utilizarse en su totalidad hasta el final del período del contrato.</p>
<p>(6) Si los pagos de la suscripción mensual o anual no se pueden cobrar a tiempo desde la tarjeta de crédito o la cuenta bancaria - debido, por ejemplo, a la falta de cobertura de la cuenta - el acceso del usuario al sistema de facturación y contabilidad se bloqueará inmediatamente. Una vez recibido el pago, se desbloqueará el acceso al sistema. El coste por cargo rechazado es de 10 euros (IVA incluido) y se le cargará al usuario. El usuario debe transferir el importe total a la cuenta bancaria del proveedor del servicio para reactivar el mismo.</p>
<p>(7) El usuario acepta que el correo electrónico (utilizando una dirección de correo electrónico proporcionada por el usuario) se podrá utilizará como medio para enviar facturas y recordatorios de pago, además de la subida de las mismas a la plataforma en el caso de ser usuario de ella.</p>
<p>(8) El proveedor del servicio tiene derecho a cambiar las tarifas acordadas a su razonable discreción. El usuario puede rescindir este contrato de usuario en el plazo de un mes a partir de la recepción de la notificación de cambio, con efecto a partir del momento en que el aumento de las tarifas surta efecto.</p>
<p><strong>8. Terminación del contrato</strong></p>
<p>(1) Los usuarios pueden rescindir la suscripción sin un período de aviso al final del respectivo mes o año (u otros períodos de facturación), según la duración que el usuario haya elegido. <u><a href="https://ayudatpymes.mailrelay-iii.es/ccm/unsubscribe/index/email/--0304000d1a5e5921050a42000519412a0504451852065c0a0b0f56510101095007154f5b540351064a0753525403610d510157525c53545a?c=575347">https://ayudatpymes.mailrelay-iii.es/ccm/unsubscribe/index/email/--0304000d1a5e5921050a42000519412a0504451852065c0a0b0f56510101095007154f5b540351064a0753525403610d510157525c53545a?c=575347</a></u>. Si esto no es posible o razonable para el usuario, la terminación puede comunicarse, opcionalmente, de forma escrita por correo electrónico al proveedor del servicio.</p>
<p>(2) El derecho de cada parte a la terminación extraordinaria no se ve afectado.</p>
<p>(3) <strong>Ayuda-T</strong> se reserva el derecho de eliminar los datos del cliente después de la rescisión del contrato independientemente del motivo de la terminación, y <strong>Ayuda-T</strong> no está obligado a almacenar ningún dato del cliente después de dicho momento. <strong>Ayuda-T</strong> conservará solo los datos requeridos durante el período mínimo para cumplir con los requisitos legales pertinentes después de la terminación de la suscripción.</p>
<p>(4) <strong>Ayuda-T</strong> garantiza actuar siempre de conformidad con el Reglamento General de Protección de Datos (RGPD) y todas las exigencias legislativas de protección de datos en todo momento.</p>
<p><strong>9. Garantía y disponibilidad de servicios</strong></p>
<p>(1) La aplicación y el servicio se proporcionan "como es" y <strong>Ayuda-T</strong> niega expresamente cualquier representación, garantía, condición u otro término, expreso o implícito, por estatuto, colateral o de otro tipo, incluidas, entre otras, garantías implícitas, condiciones u otros términos de calidad satisfactoria, idoneidad para un propósito particular o cuidado y habilidad razonables.</p>
<p>(2) <strong>Ayuda-T</strong> tiene derecho a realizar cambios operativos en el sistema para mejoras u otras cosas (por ejemplo, mediante el desarrollo o la sustitución de equipos técnicos, mantenimiento o actualización de software) sin notificarlo previamente al cliente. En algunas circunstancias, puede ser necesario suspender el acceso al sistema, generalmente entre las 21h y las 6h CET. El aviso de tal suspensión se le dará al cliente con anticipación si es posible. <strong>Ayuda-T</strong> no será responsable de las consecuencias de tal suspensión.</p>
<p>(3) El proveedor del servicio no asume ninguna responsabilidad por la funcionalidad de la conexión a sus servidores, en caso de averías eléctricas y caídas de los servidores que no están dentro de su ámbito de influencia.</p>
<p><strong>10. Derechos de uso</strong></p>
<p>(1) El proveedor del servicio concede al usuario durante la vigencia del presente contrato un derecho simple, no restringido espacialmente, intransferible, no sublicenciable y personal para usar el software utilizado por el proveedor del servicio para la prestación de sus servicios según lo previsto en estos Términos y Condiciones generales.</p>
<p>(2) El usuario tiene derecho a acceder al software operado en los sistemas informáticos del proveedor de servicios para procesar sus datos.</p>
<p>(3) El usuario puede usar el software de procesamiento solo para fines comerciales propios y solo por su propio personal.</p>
<p>(4) No se asignan derechos de propiedad intelectual al cliente. El software individualmente personalizado relacionado con el sistema también sigue siendo propiedad de <strong>Ayuda-T</strong> a menos que se estipule lo contrario.</p>
<p>(5) En relación con cualquier o todo el material cargado por el cliente y todos y cada uno de los datos del cliente, el cliente otorga a <strong>Ayuda-T</strong>, sus proveedores y subcontratistas, una licencia no exclusiva e irrevocable a nivel mundial para proporcionar la aplicación y cualquier servicio requerido relacionado con el cliente. El cliente declara y garantiza que ningún material cargado o información del cliente infringirá los derechos de terceros ni los derechos de propiedad intelectual y no contendrá ningún material que sea obsceno, ofensivo, inapropiado o que infrinja cualquier ley aplicable.</p>
<p>(6) <strong>Ayuda-T</strong> tiene derecho a ceder sus derechos y obligaciones con respecto al cliente a una empresa del grupo o a un tercero. Si el cliente acepta la mejora de la relación al permitir servicios de comercialización, estos materiales se relacionarán solo con entidades relacionadas con empresas del grupo <strong>Ayuda-T</strong>.</p>
<p>(7) El cliente acepta que <strong>Ayuda-T</strong> tiene derecho a utilizar subcontratistas en todos los asuntos, incluso para la implementación y el funcionamiento de la aplicación y el almacenamiento de los datos del cliente.</p>
<p>(8) El proveedor del servicio no está obligado a proporcionar al usuario el código fuente del software.</p>
<p>(9) La aplicación y cualquier información provista por ella, que no sean los datos del cliente, están protegidos por derechos de autor y otros derechos de propiedad intelectual y es propiedad o está bajo licencia de <strong>Ayuda-T</strong>. Cualquier desarrollo o adaptación realizada a dicha propiedad intelectual por parte del cliente se otorgará a <strong>Ayuda-T.</strong> El cliente notificará a <strong>Ayuda-T</strong> cualquier infracción real o presunta de los derechos de propiedad intelectual y cualquier uso no autorizado de la aplicación del que el cliente tenga conocimiento.</p>
<p><strong>11. Privacidad y datos del cliente</strong></p>
<p>(1) El proveedor del servicio se asegurará de que los datos personales sean recopilados, almacenados y procesados por los usuarios solo en la medida en que sea necesario para la ejecución del contrato y esté permitido por la ley o lo que ordene el legislador. El proveedor del servicio tratará los datos personales de forma confidencial y según las disposiciones de la ley de protección de datos aplicable y no los divulgará a terceros, a menos que esto sea necesario para el cumplimiento de las obligaciones contractuales y/o exista una obligación legal de transmitirlo a terceros.</p>
<p>(2) Con el fin de garantizar el procesamiento auditable de datos, se registra la creación, modificación y eliminación de datos con información del nombre del usuario y la fecha de procesamiento.</p>
<p>(3) El uso del servicio puede requerir que el proveedor del servicio procese datos personales en nombre del usuario. Para esto, se requiere la formalización de un acuerdo por separado para el procesamiento de datos personales. Las partes acuerdan que el cliente es el controlador de datos para cualquier información cargada a la aplicación de <strong>Ayuda-T</strong> y que puede modificar o borrar esta información según sea necesario. <strong>Ayuda-T</strong> es en todo momento el procesador de datos y procesa los datos en nombre del cliente. Como annexo de estos términos, las partes formalizarán un Acuerdo de Procesamiento de Datos ("APD").</p>
<p>(4) El cliente confirma que está autorizado a ordenar a <strong>Ayuda-T</strong> que procese dicha información y que todas las instrucciones dadas serán legales.</p>
<p>(5) <strong>Ayuda-T</strong> solo procesará los datos del cliente de acuerdo con las instrucciones del cliente y no para un uso propio no autorizado.</p>
<p>(6) De las partes contratantes, el cliente será el propietario de todos los datos que proporcione a <strong>Ayuda-T</strong> o a la aplicación. La aplicación permite al cliente exportar registros y datos en poder de la aplicación y el cliente acepta exportar todos los datos antes de que finalice la suscripción.</p>
<p>(7) <strong>Ayuda-T</strong> comparte información para el procesamiento de datos solo cuando sea necesario para proporcionar los servicios al cliente o cuando sea requerido por cualquier tribunal o autoridad reguladora y en ese caso solo en la medida necesaria.</p>
<p>(8) El cliente acepta que una copia del certificado bancario emitido al cliente por su banco pueda almacenarse en la base de datos de <strong>Ayuda-T</strong> y en una base de datos externa. El cliente también acepta que los datos recuperados del banco del cliente a través de una fuente bancaria están disponibles y se almacenan en el sistema.</p>
<p>(9) <strong>Ayuda-T</strong> mantendrá la confidencialidad de toda la información confidencial del cliente que éste proporcione a <strong>Ayuda-T,</strong> salvo cuando dicha información haya pasado al dominio público sin haber incumplido esta cláusula, o cuando <strong>Ayuda-T</strong> haya obtenido la información de un tercero sin un deber de confidencialidad cuando requiera su divulgación un organismo regulador o gubernamental o un tribunal de jurisdicción competente y en ese caso solo en la medida necesaria.</p>
<p>(10) <strong>Ayuda-T</strong> tomará todas las medidas de seguridad técnicas y organizativas necesarias para garantizar el procesamiento seguro de los datos del cliente y evitará que se destruya, pierda o malogre accidentalmente o ilegalmente la información del sistema y para evitar que dicha información caiga en manos de cualquier parte no autorizada o de ser mal utilizado o tratado de una manera contraria a la legislación de protección de datos. <strong>Ayuda-T</strong> deberá cumplir con sus obligaciones con toda la legislación de protección de datos aplicable como procesador de datos y se guiará por el Reglamento General de Protección de Datos (RGPD).</p>
<p>(11) En caso de que el usuario obtenga declaraciones de consentimiento de protección de datos como parte del uso de los servicios del proveedor de servicios, se señala que el usuario puede revocarlas en cualquier momento.</p>
<p>(12) Además, nos remitimos a nuestra política de privacidad, disponible en: <a href="https://www.ayudatpymes.com/politica_de_privacidad">https://www.ayudatpymes.com/politica_de_privacidad</a></p>
<p><strong>12. Cambios en los servicios</strong></p>
<p>El proveedor del servicio ajusta periódicamente sus servicios prestados en internet, a su propia discreción, al desarrollo tecnológico y las necesidades del mercado para cumplir con el uso previsto de acuerdo con la descripción del producto. Esto puede cambiar el contenido del servicio, como una funcionalidad nueva o modificada y adaptaciones a nuevas tecnologías. Dado que estos cambios forman parte de la naturaleza del software, el usuario no puede derivar ningún derecho o reclamo de esto.</p>
<p>El proveedor del servicio también tiene derecho a ofrecer nuevos servicios de pago y a suspender la prestación de servicios gratuitos. Además, el proveedor de servicios puede añadir servicios de pago ampliados a servicios de pago. Al cambiar los servicios de pago, el proveedor de servicios prestará especial atención a los intereses legítimos del usuario y anunciará los cambios de los servicios de pago a su debido tiempo.</p>
<p><strong>13. Límite de responsabilidad</strong></p>
<p>(1) Las reclamaciones de daños por incumplimiento del contrato y acción ilegal solo se pueden efectuar si hay prueba de negligencia grave intencional por parte de <strong>Ayuda-T</strong> y/o sus agentes. El descargo de responsabilidad antes mencionado no se aplica a la violación de las obligaciones contractuales esenciales.</p>
<p>(2) Adicionalmente, la responsabilidad de <strong>Ayuda-T</strong> tampoco se ve afectada en caso de lesiones personales y disposiciones legales obligatorias.</p>
<p>(3) En el caso de los servicios gratuitos, el proveedor del servicio no tendrá ninguna responsabilidad que exceda de lo especificado en los párrafos 1 y 2.</p>
<p>(4) <strong>Ayuda-T</strong> no es responsable de las interrupciones del servicio debido a fuerza mayor, en particular durante una caída o sobrecarga de las redes de comunicaciones mundiales. Por esta razón, el cliente no puede reclamar una reducción de su obligación de servicio.</p>
<p>(5) <strong>Ayuda-T</strong> no es responsable de la información publicada sobre sus servicios. El emisor es responsable de su precisión, integridad e intemporalidad.</p>
<p>(6) El proveedor del servicio no es responsable de la pérdida de datos en la medida en que el daño se deba al hecho de que el usuario ha incumplido sus obligaciones legales de conservación (consulta la sección 4.4 de estos Términos y Condiciones generales) y, por lo tanto, los datos perdidos no se puedan restaurar sin un esfuerzo razonable.</p>
<p>(7) <strong>Ayuda-T</strong> no será responsable de los daños que el cliente pueda sufrir debido a la falta de medidas de seguridad en la transmisión de los datos.</p>
<p>(8) Cualquier responsabilidad por daños está limitada al importe de la cuota mensual. La responsabilidad por daños y perjuicios, debido a la pérdida de datos, se limita a la cantidad que habría resultado con la protección de datos adecuada, sin embargo, esto no puede exceder la cuota anual.</p>
<p>(9) Cualquier reclamo de compensación por parte del cliente vence un año después de que ocurra. Esta limitación no se aplica si <strong>Ayuda-T</strong> actuó con negligencia grave o con intención.</p>
<p>(10) La responsabilidad recogida por la directiva de responsabilidad por daños causados no se ve afectada.</p>
<p><strong>14. Cambios de los Términos y Condiciones</strong></p>
<p>El proveedor del servicio se reserva el derecho de cambiar estos términos y condiciones en cualquier momento con efectividad incluso dentro de las relaciones contractuales existentes, siempre que este cambio, teniendo en cuenta los intereses del proveedor del servicio, sea razonable para el usuario; este es particularmente el caso cuando el cambio no presenta desventajas legales o económicas significativas para el usuario, p. ej., cambios en el proceso de registro o cambios en la información de contacto.</p>
<p>Cualquier otro cambio será notificado por el proveedor del servicio a los usuarios registrados al menos cuatro semanas antes de la entrada en vigor planificada para el cambio. Los cambios se comunicarán al usuario por correo electrónico. A menos que el usuario se oponga dentro de las cuatro semanas posteriores a la recepción de la notificación, el acuerdo de uso continuará con la entrada en vigor de los cambios con los términos y condiciones modificados. En la notificación de cambio, el proveedor del servicio informará al usuario de su derecho de oposición y de las consecuencias de presentar una objeción. En caso de que se presente una objeción, el proveedor del servicio tiene derecho a rescindir la relación contractual con el usuario en la entrada en vigor planificada de los cambios.</p>
<p><strong>15. Disposiciones finales</strong></p>
<p>(1) Estos términos se regirán e interpretarán de conformidad con las leyes del Reino de España y los tribunales del Reino de España tendrán jurisdicción exclusiva para determinar cualquier disputa relacionada con estos términos o su ámbito.</p>
<p>(2) Si el usuario es un distribuidor, una entidad legal de derecho público o un fondo especial de derecho público, el domicilio social del proveedor del servicio es el lugar exclusivo de jurisdicción para todas las disputas que surjan de la relación contractual.</p>
<p>(3) En caso de que las disposiciones individuales de estos términos y condiciones sean o dejen de ser efectivas, esto no afectará la validez de las disposiciones restantes.</p>
<p><em>Desde el 25 de mayo de 2018</em></p>
<p> </p>
<p> </p>
<p><strong>Política de privacidad</strong></p>
<p><strong>Declaración de Privacidad de Datos</strong></p>
<p>Nosotros, “<strong>Ayuda-T un Lugar todas las soluciones S.L.”</strong>, con CIF: B93011708, domicilio en Polígono industrial salinas de San José, Avenida Isaac Newton Edif. 287, CP. 11.500, El Puerto de Santa María (CÁDIZ) e inscrita en el Registro Mercantil de Málaga, tomo 4666, folio 17, inscripción 1, con hoja MA-102143, escrituras de constitución formalizada ante Notario, en cuya representación actúa <strong>D. Alfredo Pérez Guerrero</strong>, con D.N.I. 48901576-B, somos el operador del sitio web <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u> , así como el proveedor de servicios de la aplicación <strong>Ayuda-T Pymes</strong> para iOS y Android, incluidos los demás servicios que se ofrecen a través de los sitios web y la aplicación <strong>Ayuda-T Pymes</strong>, en adelante <strong>Ayuda-T</strong>. Somos responsables de la recopilación, el procesamiento y el uso de los datos personales según la legislación de protección de datos, en concreto, el Reglamento General de Protección de Datos ("RGPD").</p>
<p>Tú, el cliente, eres el controlador de datos y <strong>Ayuda-T</strong>, el proveedor del servicio, es el procesador de datos en tu nombre. Solo usamos tus datos en cumplimiento de la legislación de protección de datos relevantes. <strong>Ayuda-T</strong> también tiene un Delegado de Protección de Datos designado (Data Protection Officer o "DPO", en sus siglas en inglés) que puede ser contactado por carta o por correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u>.</p>
<p>Con esta Declaración de Privacidad de Datos queremos informarte qué datos personales se recopilan y se guardan cuando visitas nuestro sitio web o utilizas nuestros servicios ofrecidos en el sitio web. Además, recibirás información sobre cómo usamos tus datos y qué derechos tienes con respecto al uso de ellos. Esta Declaración de Privacidad de Datos también se aplica para el acceso y el uso de la aplicación <strong>Ayuda-T Pymes</strong>, así como de los demás servicios disponibles.</p>
<p> </p>
<p><strong>1. Seguridad de datos</strong></p>
<p>Para protegerlos, todos los datos que nos proporcionas se cifran de acuerdo con el protocolo de seguridad TLS (Transport Layer Security). El TLS es un protocolo seguro y probado que se utiliza, por ejemplo, para la banca en línea. Puedes identificar la conexión TLS, por ejemplo, al observar la "s" después de la "http" en la URL que se muestra en tu navegador (https://..) o desde el icono de bloqueo que se muestra en la pestaña del navegador.</p>
<p>También tomamos medidas de seguridad técnicas y organizativas adecuadas para proteger tus datos contra manipulaciones aleatorias o deliberadas, pérdidas parciales o totales, destrucción y/o accesos no autorizados. Para evitar la pérdida de datos, configuramos una base de datos duplicada, lo que significa que tus datos siempre se almacenan en dos ubicaciones separadas. Además, actualizamos y almacenamos los datos cada hora en una copia de seguridad fuera del sitio y, en línea con los análisis de alto riesgo, realizamos continuamente pruebas de seguridad en nuestra infraestructura. Tu contraseña se almacena a través de un proceso seguro encriptado. Nunca te pediremos la contraseña, ni por correo electrónico ni por teléfono. Si olvidas tu contraseña, podemos restablecerla por ti. Mejoramos nuestras medidas de seguridad continuamente en función del desarrollo tecnológico.</p>
<p>Los datos personales que recopilamos se almacenan en un entorno seguro dentro de la UE y se tratan de forma confidencial. El acceso a esta información está limitado a los empleados y proveedores seleccionados del grupo <strong>Ayuda-T</strong>. Cumplimos, en todo momento, con los requisitos legislativos de protección de datos.</p>
<p>Hacemos todo lo posible para proteger tus datos, pero no podemos garantizar la seguridad de tus datos cuando los transfieres a través de Internet. Cuando esto ocurre, existe un cierto riesgo de que otros puedan acceder a tus datos de forma ilícita. En otras palabras, al transferir datos por internet, asumes la responsabilidad de la seguridad de estos como controlador de datos.</p>
<p> </p>
<p><strong>2. Recolección y almacenamiento de datos personales y naturaleza y propósito de su uso</strong></p>
<p><strong>a) Si visitas nuestro sitio web</strong></p>
<p>Puedes visitar el sitio web <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u> sin revelar tu identidad. Tu navegador solo envía información - que se recopila automáticamente - a los servidores de nuestro sitio web. Esta información se almacena temporalmente en un archivo de registro (<em>logfile</em>). Esta es la información que se recopila y almacena automáticamente hasta su eliminación, también automática:</p>
<p>Dirección IP del ordenador solicitante.</p>
<p>Fecha y hora del acceso.</p>
<p>Nombre y URL de los datos introducidos.</p>
<p>Sitio web, de donde proviene el acceso (URL de referencia).</p>
<p>Navegador de uso y, si es necesario, el sistema operativo de tu ordenador, así como el nombre de tu proveedor de acceso.</p>
<p>Estos datos se recopilan y procesan para hacer posible el uso de nuestro sitio web (establecimiento de conexión), con el fin de garantizar la seguridad y la estabilidad de nuestro sistema, así como para la administración técnica de la infraestructura de la red. Dichos datos no nos ofrecen ninguna información que pueda identificarte personalmente.</p>
<p>Además, empleamos cookies, así como herramientas de análisis y de marketing web. Puedes encontrar más información sobre este tema en las secciones 3 a 5.</p>
<p><strong>b) Si te registras en nuestros servicios online</strong></p>
<p>En nuestro sitio web ofrecemos servicios online de asesoría contable, laboral y fiscal. Para usar estos servicios, primero debes registrarte. Cuando te registras, debes introducir una dirección de correo electrónico y una contraseña; de este modo, podrás crearte una cuenta con nosotros e iniciar sesión.</p>
<p>Para emplear nuestros servicios al completo, puede que debas introducir más datos personales.</p>
<p>También utilizamos tu nombre y tus datos de contacto con la siguiente finalidad:</p>
<p>Para saber quién es nuestra parte contratante.</p>
<p>Para la justificación, estructura, procesamiento y cambio de la relación contractual contigo sobre el uso de nuestros servicios.</p>
<p>Para verificar la plausibilidad de la información introducida.</p>
<p>Para contactar contigo, si es necesario.</p>
<p><strong>c) Si te registras en nuestra newsletter o boletín informativo</strong></p>
<p>Si has aceptado recibir nuestra newsletter o boletín informativo, podemos utilizar tu dirección de correo electrónico para enviarte periódicamente newsletters, así como información sobre nuestros servicios. Para recibir newsletters, primero debemos obtener tu consentimiento. Este consentimiento se debe aceptar durante el registro. Puedes revocar tu consentimiento en cualquier momento, ya sea dentro de tu cuenta o enviándonos un correo electrónico.</p>
<p>También puedes darte de baja de nuestra newsletter en cualquier momento, por ejemplo, al hacer clic en el enlace de cancelar suscripción que se encuentra en la parte inferior de la newsletter. Por otro lado, también puedes enviarnos un correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u></p>
<p>Si cancelas tu suscripción a la newsletter o boletín informativo, mantendremos tu dirección de correo electrónico registrada únicamente para garantizar que ya no recibirás estos correos electrónicos.</p>
<p><strong>d) Desarrollador, cliente, proveedor, asesor y equipo</strong></p>
<p>Con nuestros servicios, puedes introducir datos de terceros, permitir el acceso de terceros a tu cuenta, conectar tu cuenta con terceros y ofrecer a terceros tus propias aplicaciones o aplicaciones de terceros. Por supuesto, también respetamos la privacidad de los datos de terceros, a los que podemos acceder a través del uso del servicio que te ofrecemos. A veces, esto puede requerir un contrato por separado contigo. Si crees que este es el caso, contacta con nosotros.</p>
<p>Según nuestros Términos y Condiciones, no tienes derecho a compartir tus datos de acceso con terceros y estás obligado a tratar dichos datos con el debido cuidado. Además, eres responsable de los datos de terceros que introduzcas en <strong>Ayuda-T</strong>. Ten en cuenta que no tenemos ninguna capacidad sobre el cumplimiento de las normas de seguridad y protección de datos fuera de nuestro sitio web, la aplicación <strong>Ayuda-T Pymes </strong>o los servicios proporcionados por nosotros. En los casos descritos, tú o el tercero a quien le has otorgado acceso a tus datos sois los responsables.</p>
<p> </p>
<p><strong>3. Consentimiento para la transferencia de datos</strong></p>
<p>Transmitimos tus datos personales a terceros si nos lo solicitas, solo si has dado tu consentimiento explícito o si existen obligaciones legislativas para hacerlo.</p>
<p>No se realiza ninguna transferencia de datos personales a terceros con otros fines. Tus datos no se divulgan a ningún tercero sin tu permiso; a menos que un tribunal exija su entrega, y cuando lo haga solo en la medida necesaria.</p>
<p><strong>Ayuda-T</strong> se reserva el derecho de compartir datos dentro de su grupo de empresas, “Grupo Ayuda-T”, según se requiera, para proporcionarte servicios. <strong>Ayuda-T</strong> también puede solicitar, de vez en cuando, compartir datos con una compañía hermana, por ejemplo, para permitir la facturación de tu cuenta desde una entidad diferente a <strong>Ayuda-T.</strong> La seguridad de los datos está asegurada en todo momento.</p>
<p>Al registrarte en <strong>Ayuda-T,</strong> das tu consentimiento para el procesamiento de tus datos. También das tu consentimiento explícito para compartir tus datos con terceros cuando sea necesario para ofrecerte nuestro servicio.</p>
<p>Confirmamos que sólo compartimos tus datos con terceros cuyos estándares de mantenimiento de datos nos satisfacen y cumplen con toda la legislación de protección de datos.</p>
<p>Concretamente, cuando compartimos datos con territorios fuera de la UE o del EEE u otro no incluido en la lista aprobada por la Comisión Europea, nos aseguramos plenamente de sus normas de seguridad y confidencialidad de datos así como de que mantienen todos los datos compartidos de manera homologable a los estándares de la UE. Estamos obligados a poner a disposición, previa solicitud, prueba de - o referencia a -, las salvaguardas apropiadas, y podemos hacerlo después de recibir una solicitud dirigida a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u> por correo electrónico.</p>
<p>Tienes el derecho a retirar, en cualquier momento, tu consentimiento para el procesamiento y/o intercambio de tus datos, ya sea cerrando tu cuenta, que tiene efecto inmediato, o poniéndote en contacto con nosotros para solicitar el cierre, para que lo hagamos tan pronto como sea posible. Una vez finalizada tu relación con <strong>Ayuda-T</strong>, mantenemos solamente los datos mínimos que debemos mantener para cumplir con todos los requisitos legales, y solo por el período mínimo requerido.</p>
<p>Si tienes alguna pregunta sobre el procesamiento de tus datos personales, o si deseas solicitar el acceso a tus datos, puedes ponerte en contacto con el Delegado de Protección de Datos (DPO) escribiendo a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u> o escribiendo al DPO en la dirección indicada anteriormente.</p>
<p>Si no estás satisfecho, tienes derecho a presentar una queja ante la autoridad de protección de datos correspondiente. <strong>Ayuda-T</strong> cooperará plenamente con cualquier investigación de este tipo y se esforzará por satisfacer todas las consultas de la manera más completa posible. La autoridad pertinente para cada país se puede encontrar en el sitio web de la Comisión Europea: <a href="http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080">http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080</a></p>
<p> </p>
<p><strong>4. Cookies</strong></p>
<p>Nuestro sitio web utiliza cookies. Las cookies son pequeños archivos que se crean automáticamente en tu navegador y se almacenan en tu dispositivo (ordenador portátil, tableta, teléfono inteligente, etc.) cuando visitas una página. Las cookies no dañan tu dispositivo y no contienen virus, troyanos u otro software malicioso.</p>
<p>Las cookies almacenan información relacionada con tu dispositivo. Ahora bien, esto no significa que recibamos ningún conocimiento detallado de tu identidad.</p>
<p>El uso de cookies tiene el propósito de crear un uso más satisfactorio de nuestros servicios. Por lo tanto, usamos las llamadas cookies de sesión para reconocer si has visitado anteriormente páginas únicas de nuestro sitio web o si ya has creado una cuenta de cliente. Tu navegador las borrará automáticamente cuando caduquen.</p>
<p>Para fines de usabilidad, empleamos cookies temporales, que se almacenan en tu dispositivo por un período de tiempo específico. Si vuelves a visitar nuestro sitio web para usar nuestros servicios, se reconocerá que ya has visitado nuestro sitio web anteriormente y qué actividades has realizado, para que no tengas que repetirlas.</p>
<p>También usamos cookies para rastrear estadísticamente el uso de nuestro sitio web y para optimizar nuestra oferta (ver sección 4), así como para mostrarte información personalizada (ver sección 5.). Cuando vuelves a visitar nuestro sitio web, estas cookies nos permiten reconocer automáticamente que ya has visitado anteriormente nuestro sitio web. Después de un período de tiempo determinado, las cookies se eliminarán automáticamente.</p>
<p>La mayoría de los navegadores acepta cookies automáticamente. Puedes configurar tu navegador de manera que no se guarden cookies en este o para que siempre aparezca una advertencia antes de que se cree una nueva cookie.</p>
<p>Sin embargo, ten en cuenta que la desactivación completa de las cookies también puede ocasionar una funcionalidad limitada de nuestro sitio web.</p>
<p> </p>
<p><strong>5. Análisis web</strong></p>
<p>Para diseñar y optimizar continuamente nuestro sitio web, usamos diversos servicios de análisis web. Por lo tanto, creamos perfiles de usuarios anónimos y usamos cookies (ver sección 4).</p>
<p>A continuación, puedes encontrar más información sobre nuestros servicios de análisis web y sus opciones de desactivación:</p>
<p><strong>a) Google Analytics</strong></p>
<p>Usamos Google Analytics. Este es un servicio de análisis web de Google Inc. La información sobre el uso de nuestro sitio web (incluida tu dirección IP), que se recopila a través de una cookie, se transfiere a un servidor de Google en Estados Unidos y se almacena allí. Las direcciones IP son anónimas, por lo que no es posible asignártelas (enmascaramiento de IP). La información se emplea para analizar el uso de nuestro sitio web, para crear informes sobre las actividades del sitio web y para proporcionarnos otros servicios que están conectados con el uso de nuestro sitio web e internet. Los datos que has introducido al usar nuestro servicio no se incorporarán, de ninguna forma, a otros datos que se recopilan a través de Google.</p>
<p>La transferencia de información por parte de Google a terceros solo se llevará a cabo si así lo exige la ley o si terceros procesan los datos en su nombre.</p>
<p>También usamos Google Optimize. Este es un servicio de análisis web de Google Inc, que está integrado en Google Analytics. Google Optimize nos permite realizar tests A/B y multivariantes. De esta manera podemos averiguar qué versión de nuestro sitio web es la preferida por los usuarios. Aquí puedes encontrar más información sobre este servicio.</p>
<p>Puedes evitar la recopilación de datos, que se lleva a cabo a través de la cookie, así como el procesamiento de datos de Google descargando e instalando un complemento de navegador aquí.</p>
<p>Como alternativa al complemento de navegador, especialmente para navegadores en dispositivos móviles, puedes evitar la recopilación de datos de Google Analytics haciendo clic <u><a href="https://tools.google.com/dlpage/gaoptout">en este enlace</a></u>. Se colocará una cookie de bloqueo, que impidirá la recopilación de datos cuando se visite un sitio web. La cookie de bloqueo es válida solo en este navegador y para nuestro sitio web, y se archivará en tu dispositivo. Si eliminas la cookie en tu navegador, deberás volver a colocar la cookie de bloqueo.</p>
<p>Puedes encontrar más información sobre protección de datos con Google Analytics en <u><a href="https://support.google.com/analytics/?hl=es#topic=3544906">Ayuda de Analytics</a></u>.</p>
<p>Además, usamos la API Vision de Google Cloud. El sistema OCR (OCR) sirve para el reconocimiento óptico de caracteres, esto es, permite el reconocimiento automático y el análisis de letras, así como la categorización de documentos. Puedes encontrar más información sobre este servicio <u><a href="https://cloud.google.com/vision/?hl=es">aquí</a></u>. El reconocimiento de caracteres basado en la API Vision de Google Cloud es esencial para el uso de nuestros servicios. Si no deseas que se use la API Vision de Google Cloud, tienes la posibilidad de crear gastos sin cargar documentos. En tal caso, no podrás usar los servicios de <strong>Ayuda-T</strong> al completo.</p>
<p>En el siguiente enlace, puedes encontrar más información sobre protección de datos de Google: <u><a href="https://policies.google.com/privacy?hl=es">https://policies.google.com/privacy?hl=es</a></u></p>
<p> </p>
<p><strong>6. Segmentación de audiencia (</strong><em><strong>targeting</strong></em><strong>)</strong></p>
<p>Usamos tecnologías de segmentación de audiencia de Google Inc. (por ejemplo, Doubleclick, AdSense, AdWords) en nuestro sitio web. Estas tecnologías nos permiten ofrecerte publicidad basada en tus intereses. Para este propósito, recopilamos y evaluamos información sobre tu comportamiento de usuario en nuestro sitio web gracias al uso de cookies.</p>
<p>La recopilación y evaluación se lleva a cabo de forma anónima y no nos permite identificarte. En otras palabras, no vinculamos esta información con tus datos personales.</p>
<p>Si no deseas recibir publicidad personalizada, puedes evitarlo a través de la configuración de cookies relevantes en tu navegador.</p>
<p>Puedes cambiar la configuración para mostrar publicidad personalizada a través de la configuración de anuncios de Google.</p>
<p>Puedes encontrar más información, así como las normas de privacidad de datos sobre publicidad y Google en su <u><a href="https://policies.google.com/privacy?hl=es">política de privacidad</a></u>.</p>
<p> </p>
<p><strong>7. Seguimiento de Facebook</strong></p>
<p>No usamos plugins sociales de Facebook u otras redes sociales. En relación con nuestra publicidad en Facebook, empleamos un mecanismo de seguimiento basado en píxeles. Este es un servicio de análisis web provisto por Facebook Ireland Ltd. La información se usa para rastrear conversiones procedentes de la plataforma Facebook.</p>
<p>Este servicio lo provee Facebook Ireland Ltd., por el que se aplica la ley de privacidad de datos de la Unión Europea. No compartimos ningún dato que introduces mientras usas nuestro servicio con Facebook.</p>
<p>Consulta la <u><a href="https://www.facebook.com/business/gdpr">información de protección de datos de Facebook</a></u> para obtener más información sobre el propósito y el alcance de la recopilación de datos, y el procesamiento y uso de los datos por parte de Facebook, así como tus derechos y opciones de configuración para la protección de la privacidad.</p>
<p> </p>
<p><strong>8. Información, corrección, bloqueo, eliminación</strong></p>
<p>Tienes derecho a la información sobre datos personales que almacenamos y el derecho a corregir o a enmendar datos incorrectos, así como a bloquearlos y eliminarlos.</p>
<p>Como controlador de datos, eres responsable del contenido que publicas. Tienes derecho a rectificar, bloquear o borrar, en cualquier momento, cualquiera de tus datos. Podemos eliminar contenido que has publicado a petición tuya, pero mantenemos nuestro derecho a no eliminar el contenido que ya está publicado o que debemos mantener para cumplir con determinados requisitos legales.</p>
<p>Para obtener información sobre tus datos personales, para la corrección de datos erróneos o para el bloqueo o eliminación de datos, así como para otras preguntas sobre el uso de tus datos personales, envía un correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u></p>
<p>Además, puedes ver y cambiar los datos que se almacenan en tu cuenta iniciando sesión en nuestro sitio web con tus datos de acceso. Puedes eliminar los datos de tu cuenta en todo momento. Esto se puede hacer mediante el uso de la opción correspondiente en tu cuenta. Advertimos que si eliminas tus datos, no podrás usar nuestro servicio al completo o en absoluto.</p>
<p> </p>
<p><strong>9. Cambios en la Declaración de Privacidad de Datos</strong></p>
<p>Esta Declaración de Privacidad de Datos está ahora vigente y se actualizó por última vez en mayo de 2018.</p>
<p>Debido al desarrollo posterior del sitio web, la aplicación <strong>Ayuda-T</strong> o cualquier otro servicio de <strong>Ayuda-T,</strong> o debido a cambios en los requisitos legales o reglamentarios, puede que esta Declaración de Privacidad de Datos deba modificarse en un futuro. Nuestra Declaración de Privacidad de Datos se puede acceder e imprimir en todo momento en nuestro sitio web.</p>
<p> </p>
<p><strong>Acuerdo de Procesamiento de Datos</strong></p>
<p>Este Acuerdo de Procesamiento de Datos ("APD") es la base de la relación entre tú, el cliente, como controlador de datos y <strong>Ayuda-T</strong>, el proveedor del servicio, como procesador de datos bajo la legislación de protección de datos, más concretamente, el Reglamento General de Protección de Datos ("RGPD").</p>
<p>Este es un acuerdo importante que constituye la base contractual para que procesemos los datos en tu nombre. Explica cómo pueden procesarse tus datos y su propósito. Procesamos tus datos personales sólo según lo requerido y según tus indicaciones, tal como se describe en este acuerdo.</p>
<p>Debido a nuestro volumen de clientes, sería imposible celebrar acuerdos firmados individualmente con todos nuestros usuarios. También es nuestro deseo que, al facilitar la celebración de este acuerdo (APD), se asegure que la aceptación de los nuevos Términos, en cumplimiento del RGPD, te tome menos tiempo como propietario que debe su principal ocupación a un negocio.</p>
<p>Este APD te asegura que nosotros, como tu procesador de datos, cumplimos con los requisitos estipulados en el RGPD. Además, puedes estar seguro de que mantenemos los acuerdos necesarios con terceras partes. Los detalles de tu empresa se completan automáticamente en tu cuenta cuando aceptas los Términos y Condiciones y la Política de Privacidad, incluido este APD. Tus datos siempre representarán la información más actualizada que nos hayas proporcionado. El APD se detalla a continuación para tu información.</p>
<p> </p>
<p>Acuerdo de Procesamiento de Datos</p>
<p>entre:</p>
<p>Nombre del cliente (“el cliente” o “controlador de datos”, en adelante) [Esta información se completará automáticamente una vez que hayas completado el registro]</p>
<p>y</p>
<p>“<strong>Ayuda-T un Lugar todas las soluciones S.L.”</strong>, con CIF: B93011708, domicilio en Polígono industrial salinas de San José, Avenida Isaac Newton Edif. 287, CP. 11.500, El Puerto de Santa María (CÁDIZ) (“<strong>Ayuda-T</strong>” o “procesador de datos”, en adelante)</p>
<p>cada uno una "parte"; juntos "las partes",</p>
<p>HAN ACORDADO los términos de este Acuerdo de Procesamiento de Datos (en adelante "APD" o "Acuerdo") sobre protección de datos personales con respecto al procesamiento de datos personales cuando el cliente actúa como controlador de datos y <strong>Ayuda-T</strong> como procesador de datos para cumplir con las obligaciones de servicio descritas en el acuerdo de servicios (detallado más abajo). Como parte del cumplimiento de estas obligaciones de servicio, <strong>Ayuda-T</strong> procesará ciertos datos personales en nombre del controlador de datos, de conformidad con los términos de este contrato. Cada parte acuerda y se asegurará de que los términos de este contrato también sean plenamente aplicables a sus filiales, las cuales puedan estar involucradas en las operaciones de procesamiento de datos personales para el proyecto definido en el acuerdo de servicios. Específicamente, <strong>Ayuda-T</strong> se asegurará de que todos los subprocesadores operen dentro de los mismos términos que este Acuerdo al procesar los datos personales del cliente.</p>
<p> </p>
<p><strong>Introducción y definiciones:</strong></p>
<p>Los datos personales se definen como cualquier información relacionada con un interesado por la que se puede identificar en particular, directa o indirectamente, por referencia a un identificador como un nombre, un número de identificación, datos de ubicación, un identificador en línea o uno o más factores específicos de la identidad física, fisiológica, genética, mental, económica, cultural o social de esa persona física o jurídica (cuando corresponda).</p>
<p>Todas las demás definiciones mencionadas en este documento, incluidos los términos controlador de datos y procesador de datos, están determinadas por las leyes de protección de datos correspondientes, incluido el Reglamento General de Protección de Datos de la UE 2016/679 de 27 de abril de 2016 (en adelante, "RGPD").</p>
<p>Los datos personales confidenciales no se consideran procesados bajo el servicio de aplicación ofrecido por el procesador de datos y, por lo tanto, están excluidos de los términos de este Acuerdo. .</p>
<p>Al suscribirte para utilizar el programa <strong>Ayuda-T</strong> y aceptar los Términos y Condiciones, incluida la Política de Privacidad y este APD, las partes acuerdan bajo todas las leyes nacionales de protección de datos y bajo el RGPD que este Acuerdo rige la relación entre el controlador de datos y el procesador de datos, determinando el procesamiento de los datos personales por parte de <strong>Ayuda-T</strong> de los datos del cliente. Este Acuerdo tiene prioridad, a menos que haya sido reemplazado por otro APD firmado que comunica su primacía sobre este Acuerdo.</p>
<p>El propósito del procesamiento de <strong>Ayuda-T</strong> de datos personales para el cliente es garantizar el pleno uso del servicio por parte del cliente y permitir que este Acuerdo se cumpla. <strong>Ayuda-T</strong> garantizará que se mantenga la seguridad suficiente de los datos personales en todo momento.</p>
<p>Ambas partes confirman su autoridad para firmar el Acuerdo al hacerlo.</p>
<p> </p>
<p><strong>Responsabilidades del procesador de datos:</strong></p>
<p>El procesador de datos debe tratar todos los datos personales en nombre del controlador de datos y seguir sus instrucciones. Al celebrar este Acuerdo, <strong>Ayuda-T</strong> (y cualquier subprocesador con el que el procesador de datos tenga un acuerdo legal para ofrecer los servicios) tiene instrucciones de procesar los datos personales del cliente de la siguiente manera:</p>
<p>i) de acuerdo con todas las leyes nacionales y europeas; ii) cumplir con sus obligaciones bajo los términos de la aplicación de servicio; iii) según instrucciones del controlador de datos; iv) como se describe en este Acuerdo.</p>
<p>Como parte que provee la aplicación, se requiere que el procesador de datos siempre proporcione al cliente las soluciones adecuadas para acompañar el desarrollo continuo de su negocio mediante el uso del servicio. El procesador de datos rastrea cómo el cliente usa la aplicación para hacer las mejores sugerencias, proporcionar servicios relevantes en todo momento y comprometerse a enviar las comunicaciones más precisas con el fin de lograr la facilidad de uso y la satisfacción del cliente. En la medida en que el procesamiento de los datos personales de la aplicación forma parte de esto, se procesan solo de acuerdo con este APD y la ley aplicable y se comparten solo cuando sea necesario para proporcionar una mejor experiencia al cliente.</p>
<p>Teniendo en cuenta la tecnología disponible y el coste de implementación, así como el alcance, el contexto y el propósito del procesamiento, se requiere que el procesador de datos tome todas las medidas razonables, incluidas medidas técnicas y organizativas, para garantizar un nivel suficiente de seguridad en relación con el riesgo y la categoría de datos personales a proteger. El procesador de datos deberá ayudar al controlador de datos con las medidas técnicas y organizativas apropiadas según sea necesario y teniendo en cuenta la naturaleza del tratamiento y la categoría de información disponible para el procesador de datos para garantizar el cumplimiento de las obligaciones del controlador de datos bajo las leyes de protección de datos aplicables.</p>
<p>El procesador de datos notificará al controlador de datos sin demora indebida si el procesador de datos tiene conocimiento de una violación de la seguridad.</p>
<p>Además, el procesador de datos debe, en la medida de lo posible y de forma legal, informar al controlador de datos si se eleva una solicitud de datos (solicitud de acceso a datos) por parte de los organismos que deben proporcionarla. El procesador de datos responderá a tales solicitudes una vez que el controlador de datos lo autorice a hacerlo. El procesador de datos tampoco divulgará información sobre este Acuerdo a menos que el procesador de datos esté obligado por ley a hacerlo, como por orden judicial.</p>
<p>Si el controlador de datos requiere información o ayuda con respecto a la seguridad de los datos, la documentación o la información sobre cómo el procesador de datos procesa los datos personales en general, puede solicitar esta información del procesador.</p>
<p>El procesador de datos, sus empleados y cualquier afiliado o socio garantizará la confidencialidad en relación con los datos personales procesados en virtud del Acuerdo. Esta disposición continúa aplicándose después de la terminación del Acuerdo, independientemente de la causa de su terminación.</p>
<p> </p>
<p><strong>Responsabilidades del controlador de datos:</strong></p>
<p>El controlador de datos confirma, al firmar este Acuerdo, que, al utilizar la aplicación, podrá procesar libremente sus datos según todos los requisitos legales de protección de datos, incluido el RGPD. El controlador da su consentimiento explícito para el procesamiento de sus datos personales en todo momento al utilizar el servicio.</p>
<p>El controlador de datos puede revocar este consentimiento en cualquier momento, pero al hacerlo termina el Acuerdo vigente y el procesador de datos ya no podrá ofrecer el servicio.</p>
<p>El cliente tiene una base legal para procesar los datos personales con el procesador de datos (incluidos los subprocesadores) con el uso de los servicios de <strong>Ayuda-T.</strong></p>
<p>El controlador de datos es responsable en todo momento de la precisión, integridad, contenido y fiabilidad de los datos personales procesados por el procesador de datos. Ambos has cumplido todos los requisitos obligatorios en relación con la notificación u obtención de permiso de las autoridades públicas pertinentes con respecto al procesamiento de datos personales. Además, ambos han cumplido sus obligaciones de divulgación con las autoridades pertinentes con respecto al procesamiento de datos personales de acuerdo con toda la legislación de protección de datos aplicable.</p>
<p>El controlador de datos debe tener una lista precisa de las categorías de datos personales que procesa, particularmente si dicho procesamiento difiere de las categorías enumeradas por el procesador de datos en el Anexo A.</p>
<p> </p>
<p><strong>Acuerdo para la transferencia de datos y el uso de subcontratistas:</strong></p>
<p>Para proporcionar el servicio al controlador de datos, el procesador de datos usa subcontratistas. Estos subcontratistas pueden ser proveedores externos tanto dentro como fuera de la UE / EEE. El procesador de datos garantiza que todos los subcontratistas cumplan con las obligaciones y los requisitos de este Acuerdo; específicamente, que su nivel de protección de datos cumple con el estándar requerido por las leyes de protección de datos relevantes. Si una jurisdicción queda fuera de la UE / EEE y no está en la lista aprobada por la Comisión Europea de niveles de protección de datos satisfactorios bajo el RGPD, entonces se establece un acuerdo específico entre <strong>Ayuda-T</strong> y dicho subcontratista para asegurar que mantendrá todos los datos personales según los requisitos de las leyes vigentes de protección de datos de la UE.</p>
<p>Este Acuerdo constituye el consentimiento previo específico y explícito del controlador de datos para el uso por parte del procesador de datos de subcontratistas de procesadores de datos, que a veces pueden estar fuera de la UE / EEE o de territorios aprobados por la Comisión Europea.</p>
<p>El controlador de datos puede revocar este consentimiento en cualquier momento, pero al hacerlo termina la vigencia del Acuerdo y el procesador de datos ya no podrá ofrecer el servicio.</p>
<p>Si se establece un subdirector o almacena datos personales fuera de los territorios aprobados por la UE / EEE o la Comisión Europea, el procesador de datos tiene la responsabilidad de garantizar una base satisfactoria para transferir datos personales a un tercer país en nombre del controlador de datos, incluido el uso de los contratos estándar de la Comisión Europea o medidas específicas que hayan sido aprobadas previamente por la Comisión Europea.</p>
<p>El controlador de datos debe ser informado antes de que el procesador de datos reemplace a sus subcontratistas. El controlador de datos puede oponerse a un nuevo subprocesador que procesa sus datos personales en nombre del procesador de datos, pero solo si el subprocesador no procesa los datos de acuerdo con la legislación de protección de datos vigente. El procesador de datos puede demostrar el cumplimiento proporcionando al controlador de datos dando acceso a la evaluación de protección de datos realizada por el procesador de datos.</p>
<p>Si el controlador de datos aún se opone al uso del subcontratista, puede finalizar su suscripción al servicio, sin el período de notificación habitual requerido, y luego asegurarse de que el subcontratista no deseado no procese sus datos personales.</p>
<p> </p>
<p><strong>Duración del Acuerdo:</strong></p>
<p>El Acuerdo sigue siendo válido siempre que el procesador de datos procese datos personales con el uso del procesador de datos de la aplicación de servicio y a menos que sea reemplazado por otro APD firmado que comunique su primacía sobre este Acuerdo.</p>
<p> </p>
<p><strong>Terminación del Acuerdo:</strong></p>
<p>Tras la finalización de cualquier suscripción, cuando el Acuerdo finalice, el procesador de datos eliminará todos los datos personales, excepto aquellos que se requiera que conserve según los requisitos legales aplicables y en tal caso se guardarán de acuerdo con las garantías técnicas y organizativas.</p>
<p>El controlador de datos tiene capacidad completa para recuperar todos sus datos personales de la aplicación de servicio. Si el controlador de datos solicita asistencia para la recuperación de datos, los costes asociados se determinarán de común acuerdo entre las partes y se basarán en la complejidad del proceso solicitado y el tiempo para cumplirlo en el formato elegido.</p>
<p> </p>
<p><strong>Cambios en el Acuerdo:</strong></p>
<p>Los cambios en el Acuerdo deben adjuntarse en un anexo separado del Acuerdo. Si alguna de las disposiciones del Acuerdo se considera inválida, esto no afecta las disposiciones restantes. Las partes reemplazarán las disposiciones inválidas con una disposición legal que refleje el propósito de la disposición inválida.</p>
<p> </p>
<p><strong>Auditorías:</strong></p>
<p>El controlador de datos tiene derecho a iniciar una revisión de las obligaciones del procesador de datos bajo el acuerdo una vez al año. Si se requiere que el procesador de datos lo haga conforme a la legislación aplicable, las auditorías pueden repetirse una vez al año. Se debe proporcionar un plan de auditoría detallado que describa el alcance, la duración y la fecha de inicio al menos cuatro semanas antes de la fecha de inicio propuesta. Las partes deciden juntas si un tercero debe realizar la auditoría. Sin embargo, el controlador de datos puede permitir que el procesador de datos tenga la revisión de seguridad realizada por un tercero neutral a elección del procesador de datos, si se trata de un entorno de procesamiento donde se procesan datos de múltiples controladores de datos.</p>
<p>Si el alcance propuesto de la auditoría sigue un informe de certificación ISAE, ISO o similar realizado, dentro de los doce meses anteriores, por un auditor tercero calificado y el procesador de datos confirma que no ha habido cambios importantes en las medidas bajo revisión, esto satisfará cualquier solicitud recibida dentro de dicho plazo. Las auditorías no pueden interferir irracionalmente con las actividades habituales del procesador de datos. El controlador de datos es responsable de todos los costes asociados con su solicitud de revisión de auditoría.</p>
<p> </p>
<p><strong>Responsabilidades y jurisdicciones:</strong></p>
<p>La responsabilidad por acciones derivadas del incumplimiento de las disposiciones de este Acuerdo se rige por las disposiciones de responsabilidad e indemnización en los términos de la suscripción en la sección 13. Esto también se aplica a cualquier violación por parte de los subprocesadores del procesador de datos.</p>
<p>Este Acuerdo se rige por los tribunales del Reino de España, que tendrán jurisdicción exclusiva para determinar cualquier disputa sobre el mismo.</p>
<p> </p>
<p><strong>Anexo A. Categorías de información personal y categorías de procesamiento habitual</strong></p>
<p><strong>A. Categorías de información personal (la lista no es exhaustiva):</strong></p>
<p>Nombre.</p>
<p>Dirección.</p>
<p>Número(s) de teléfono.</p>
<p>Dirección(es) de correo electrónico.</p>
<p>Dirección(es).</p>
<p>Cualquier número de cuenta y/o detalles bancarios.</p>
<p><strong>B. Categorías de procesamiento habituales (la lista no es exhaustiva):</strong></p>
<p>Los empleados del controlador de datos.</p>
<p>Contactos del controlador de datos (teléfono / correo electrónico / direcciones / etc.).</p>
<p>Los clientes del controlador de datos.</p>
<p>La información bancaria del controlador de datos.</p>
<p>Los empleados de sus clientes.</p>
<p>Contactos de sus clientes (teléfono / correo electrónico / direcciones / etc.).</p>
<p>Los clientes de sus clientes.</p>
<p>Información bancaria de los clientes de sus clientes.</p>
</div>
</div>


        </div>







    <?php
include 'required/footer.php';
?>
