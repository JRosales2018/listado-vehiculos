<?php
include 'required/header.php';
$what = 'alta-autonomos';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="alta-autonomos-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    ALTA AUTÓNOMOS EN 3 HORAS
                </h1>
                <h2>
                    Con tu servicio de asesoría
                </h2>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">
                    
                    <div class="row">
                        <div class="precio">
                            <img src="img/precios/precio-alta-autonomo.png" alt="Precio alta autónomo" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-gestoria justify-content-sm-center">
            <h2><span>SERVICIO ALTA AUTÓNOMOS EXPRÉS EN 3 HORAS DESDE 25 EUROS</span><br> TRABAJAMOS CON MÁS DE <?php echo $clientes ?> CLIENTES PYMES Y AUTÓNOMOS EN TODA ESPAÑA</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row gestoria justify-content-sm-center">

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-md-center">
                        <div class="col-sm-6 col-md-4 col-lg-5 img-section1">

                            <div class="row">
                                <h2>ALTA<br>AUTÓNOMOS<br>
                                    EXPRÉS</h2>
                                <div class="precio">
                                    <img src="img/precios/precio-alta-autonomo.png" alt="Precio alta autónomo" />
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-6">
                            <p>Date de alta como autónomo en menos de 3 horas y olvídate para siempre de todas tus obligaciones fiscales, laborales y contables. Trabaja con las mejores herramientas de gestión y haz crecer tu negocio.</p>
                             <p class="boton-asesorias"><a class="ir-formulario">lo quiero!</a></p>
                        </div>

                    </div>
                </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito <a href="tel:900100162">900 100 162</a>. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                       <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row pre-modulo-empresas justify-content-sm-center">
            <div class="col-lg-8">
            <h2>¿NO CONTROLAS EL PROCESO DE DARSE DE ALTA COMO AUTÓNOMO?<br>
<span>ALTA AUTÓNOMOS Y TODAS LAS OBLIGACIONES CUBIERTAS</span></h2>
            <div class="row justify-content-sm-center">
                <div class="col-lg-6 col-md-6 col-sm-10 pre-mod-left">
                    <h2 class="subtitle-pre-mod">ALTA AUTÓNOMOS</h2>
                    <p>Al contratar el servicio de asesoría te incluimos gratis el alta de autónomo, aunque si necesitas hacerlo lo antes posible con el servicio exprés tendrás tu alta tramitada en 3 horas.</p>
                    
                    <h2 class="subtitle-pre-mod">SERVICIO INTEGRAL</h2>
                    <p>Todas las obligaciones fiscales, laborales y contables de tu negocio bajo control.</p>
                    
                    <h2 class="subtitle-pre-mod">ASESOR PERSONAL</h2>
                    <p>Desde que decidas dar el paso de darse de alta como autónomo hasta...bueno, ¡esperamos que por siempre jamás! Tu asesor personal te acompañará en cada paso y velará por que todo marche de forma correcta.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-10 pre-mod-right">
                    <h2 class="subtitle-pre-mod">LA MEJOR TECNOLOGÍA PARA TERMINAR CON EL PAPELEO, GRATIS.</h2>
                    <p>Somos una asesoría de base tecnológica. Nos dedicamos a hacer más sencillo procesos y tareas con el papeleo mediante el desarrollo de herramientas. Y sí, tenemos las mejores: facturar, vigilar cómo van las ventas, la contabilidad o incluso la evolución de tu negocio día a día, cada mes, trimestre o año, serán pan comido.</p>
                    
                    <h2 class="subtitle-pre-mod">EXPERIENCIA DEMOSTRABLE</h2>
                    <p>¡Sabemos lo que hacemos! Somos la asesoría líder en nuestro sector. Trabajamos con miles de autónomos alrededor de toda España, sabemos lo dura que se puede poner la cosa al tramitar tu alta en el RETA por primera vez (y la segunda, y la tercera,...). Nosotros iremos allanando el terreno, no te preocupes. :)</p>
                </div>
            </div>
            </div>
        </div>
            <div class="row modulo-empresas justify-content-sm-center">
                 <h2 class="tit-gestores2">DARSE DE ALTA COMO AUTÓNOMO <br class="visible-xs">SERÁ MUCHO MÁS SENCILLO<br><span>CON LA AYUDA ADECUADA</span></h2>
                    <div class="col-sm-6 col-md-5 col-lg-4">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 float-right item-emp2">
                                <img src="img/alta-autonomos-ayudatpymes.jpg" class="gestor1" alt="Alta autónomos AyudaT PYMES"/>
                                <p class="gestor-name">Elena | Consultora comercial</p>
                                <p class="gestor-description">Érase una vez una madrileña que se enamoró de Cádiz.</p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4 text-gestores">
                   
                        <p class="text-gestores1">Hacer crecer un negocio ya es tarea difícil, deja todo el rollo del papeleo en manos expertas.
                        </p>
                        <p class="text-gestores1">Nos encargaremos de que conozcas bien los requisitos para ser autónomo, las obligaciones fiscales que tendrán que afrontar – de las que nos encargaremos nosotros- antes de que des el paso de darse de alta como autónomo.</p>
                    </div>
                    
                    <div class="col-sm-6 col-md-6 col-lg-4 visible-lg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 float-right item-emp2">
                                <img src="img/alta-autonomos.jpg" class="gestor2" alt="Alta autónomos"/>
                                <p class="gestor-name">Borja | Consultor comercial</p>
                                <p class="gestor-description">Un tipo muy presumido al que le encanta pasar tiempo con su hijo</p>
                            </div>
                        </div> 
                    </div>
                
                </div>
                
                
            
         
            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

            <!--        ---------------------------------- OPINIONES --------------------------------------->

        </div>


    <div class="container">
        
        <!--        ---------------------------------- FIN OPINIONES --------------------------------------->
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php
include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->
<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= $('.pre-mod-right').offset().top + $('.pre-mod-right').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor2", 1,{opacity:1,x:0});
    tl4.to(".gestor1", 1,{opacity:1,x:0},"-=1");
        }
        
             if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestoria .precio", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>




    <?php
include 'required/footer.php';
?>
