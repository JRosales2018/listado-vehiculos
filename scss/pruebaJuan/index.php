﻿<?php
include 'required/header.php';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
<script src="js/animation-home.js"></script>
    <div id="index-page">
        <div class="container">
            <div id="cabecera">
                <!--------- CABECERA Escritorio -------->
                <div id="title-black">
                    <h1 class="radj-b-30 title-black">ASESORÍA ONLINE Y PRESENCIAL</h1>
                    <h2 class="radj-b-30 title-black">AUTÓNOMOS Y EMPRESAS</h2>
                    <div class="row justify-content-sm-center">
                        <div class="col-sm-4 col-lg-3 img-section1">
                            <h2 class="radj-sb-23 tit-precio">Todas tus obligaciones cubiertas</h2>
                            <div class="row">
                                <div class="precio"><img src="img/precios/tarifa-asesoria.png" alt="Tarifa asesoría" /></div>
                                <div class="datosprecio"><span class="llama">LLAMA AHORA</span><span class="gratis">GRATIS</span><a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a></div>
                            </div>
                            <p class="legal-precio">Servicio de presentación de impuestos</p>
                        </div>
                        <div class="col-sm-2 col-lg-2 columncenter">
                            <div class="arrow"></div>
                        </div>
                        <div class="col-sm-4 col-lg-3 bloque2-section1">
                            <?php include "required/form-cabecera.php";?>
                        </div>
                    </div>
                </div>
                <!--------- CABECERA Mobile -------->
                <div id="title-black-mobile">
                    <h2 class="radj-b-30 title-black1">ASESORÍA ONLINE Y PRESENCIAL</h2>
                    <h2 class="radj-b-30 title-black2">AUTÓNOMOS Y EMPRESAS</h2>
                </div>
                <div class="row justify-content-sm-center cabecera-mobile">
                    <div class="col-sm-6 col-md-5 col-lg-3 img-section1">
                        <h2 class="radj-sb-23 tit-precio">Precio asesoría tarifa plana</h2>
                        <div class="row">
                            <div class="precio"> <img src="img/precios/tarifa-asesoria.png" alt="Precio gestoría online" /></div>
                            <div class="datosprecio"><span class="llama">LLAMA AHORA</span><span class="gratis">GRATIS</span><a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a></div>
                        </div>
                        <p class="legal-precio">Servicio de presentador de impuestos</p>
                    </div>
                    <div class="col-sm-2 col-lg-2 columncenter">
                        <div class="arrow"><img src="img/arrow-orange.png" alt="flecha"></div>
                    </div>
                    <div class="col-sm-5 col-md-4 col-lg-3 bloque2-section1">
                        <?php include "required/form-cabecera.php";?>
                    </div>
                </div>
            </div>




            <!--        ---------------------------------- FIN CABECERA --------------------------------------->

            <!--        ---------------------------------- ABOUT --------------------------------------->

            <div id="about"></div>
            <div class="row mar-top-17">
                <h2 class="title-section2 radj-b-23">GESTORÍA ONLINE Y PRESENCIAL ESPECIALIZADA PARA TU NEGOCIO</h2>
                <h2 class="subtitle-section2">MÁS QUE UNA GESTORÍA BARATA, ASESORÍA ONLINE TECNOLÓGICA, <br>LÍDER EN ESPAÑA CON MÁS DE
                    <?php echo $clientes ?> CLIENTES </h2>
                <h2 class="link-section2">ASESORAMIENTO <a href="juridica">FISCAL</a> | <a href="juridica">LABORAL</a> | <a href="juridica">CONTABLE</a> | <a href="juridica">JURÍDICO</a> </h2>

                <div class="row tabs-section2">
                    <div class="col link1">
                        <a href="asesoria" id="link1" class="desplegable">ASESORÍA ONLINE</a>
                        <div class="line-sections2"></div>
                        <p class="text-collapse">SIN DESPLAZAMIENTOS<br> Precio gestoría barata<br> desde 9,99€ </p>
                    </div>
                    <div class="col link2">
                        <a href="asesoria" id="link2" class="desplegable">AUTÓNOMOS Y EMPRESAS</a>
                        <div class="line-sections2"></div>
                        <p class="text-collapse">ASESORÍA PRESENCIAL<br> También gestoría online</p>
                    </div>
                    <div class="col link3">
                        <a href="https://www.ayudatpymes.com/para-despachos/" id="link3" class="desplegable">PARA DESPACHOS</a>
                        <div class="line-sections2"></div>
                        <p class="text-collapse">MARCA BLANCA DESPACHOS<br> Laboral, fiscal, contable y jurídico.</p>
                    </div>
                    <div class="col link4">
                        <a href="emprendedores" id="link4" class="desplegable">GESTORÍA STARTUPS</a>
                        <div class="line-sections2"></div>
                        <p class="text-collapse">PARA EMPRENDEDORES<br> Precio asesoría barata<br> desde 9,99€</p>
                    </div>
                    <div class="col link5">
                        <a href="empresas-sociales" id="link5" class="desplegable">EMPRESAS SOCIALES</a>
                        <div class="line-sections2"></div>
                        <p class=" text-collapse">GESTORÍA ONLINE<br> Asesoría desde 49,99€</p>
                    </div>
                </div>

            </div>
        </div>
        <!--        ---------------------------------- FIN ABOUT --------------------------------------->

        <!--        ---------------------------------- MEDIOS --------------------------------------->

        <div class="row medios" id="medios">
            <div class="col medios-img">
                <a href="http://www.elmundo.es/economia/2017/08/02/5981eff7468aeb07788b4599.html" target="_blank" rel=”nofollow”><img src="img/logo-mundo.png" alt="El Mundo" /></a>
            </div>
            <div class="col medios-img">
                <a href="http://www.expansion.com/pymes/2017/07/10/595a848f468aeb03178b45f0.html" target="_blank" rel=”nofollow”><img src="img/logo-expansion.png" alt="Expansión" /></a>
            </div>
            <div class="col col-text-medios">
                <p class="text-medios"> MIRA LO QUE DICEN <a href="asesoria-online-ayuda-t-pymes"><span>DE NOSOTROS </span></a></p>
            </div>
            <div class="col medios-img">
                <a href="https://www.eldiario.es/murcia/espacioedm/Ayuda-T-Pymes-solucion-ansiada-realidad_6_654494579.html" target="_blank" rel=”nofollow”><img src="img/logo-diario.png" alt="Diario" /></a>
            </div>
            <div class="col medios-img">
                <a href="https://www.youtube.com/channel/UCXwwWpdaOYeHetfNz7DxwPw" target="_blank" rel=”nofollow”><img src="img/logo-youtube.png" alt="Youtube" /></a>
            </div>
        </div>

        <div class="row medios-mobile">
            <div class="row">
                <div class="col-sm-12 col-text-medios">
                    <p class="text-medios"> MIRA LO QUE DICEN <span>DE NOSOTROS </span></p>
                </div>
            </div>
            <div class="row">
                <div class="col medios-img">
                    <a href="http://www.elmundo.es/economia/2017/08/02/5981eff7468aeb07788b4599.html" target="_blank" rel=”nofollow”><img src="img/logo-mundo.png" alt="El Mundo" /></a>
                </div>
                <div class="col medios-img">
                    <a href="http://www.expansion.com/pymes/2017/07/10/595a848f468aeb03178b45f0.html" target="_blank" rel=”nofollow”><img src="img/logo-expansion.png" alt="Expansión" /></a>
                </div>
                <div class="col medios-img">
                    <a href="https://www.eldiario.es/murcia/espacioedm/Ayuda-T-Pymes-solucion-ansiada-realidad_6_654494579.html" target="_blank" rel=”nofollow”><img src="img/logo-diario.png" alt="Diario"  /></a>
                </div>
                <div class="col medios-img">
                    <a href="https://www.youtube.com/channel/UCXwwWpdaOYeHetfNz7DxwPw" target="_blank" rel=”nofollow”><img src="img/logo-youtube.png" alt="Youtube" /></a>
                </div>
            </div>


        </div>

        <!--        ---------------------------------- FIN MEDIOS --------------------------------------->
        <!--        ---------------------------------- DETALLES --------------------------------------->
        <div class="container">


            <!--        ---------------------------------- FIN DETALLES --------------------------------------->
            <!--        ---------------------------------- VIDEO --------------------------------------->

            <div class="row video">
                <img class="etiqueta-derecha" src="img/etiqueta.png" alt="Etiqueta" />
                <img class="etiqueta-izq" src="img/etiqueta.png" alt="Etiqueta" />

                <h2>MUCHO MÁS ASESORÍA ONLINE Y PRESENCIAL LÍDER EN ESPAÑA
                </h2>
                <h3>Especialistas en la gestión de Pymes y autónomos
                </h3>



                <div id="tv" class="col-sm-10 col-lg-6">
                    <div class="tv-responsive">
                        <iframe allowfullscreen="allowfullscreen" src="https://www.youtube.com/embed/fHY4COLeiEo?vq=hd720">
</iframe>
                    </div>
                </div>
            </div>

            <!--        ---------------------------------- FIN VIDEO --------------------------------------->

            <!--        ---------------------------------- WHY --------------------------------------->

            <div class="row why">
                <div class="col-sm-12 col-lg-12 panel-why">
                    <a class="desplegable-why" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> <h2>¿Por qué somos diferentes?</h2><br>Descubrir<i class="fa fa-caret-right"></i></a>


                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="row justify-content-md-center">

                            <div class="col-sm-9 col-lg-4 subpanel-why">
                                <p>Mucho más que una asesoría barata. Somos la asesoría online y presencial que hará que dejes de preocuparte por el papeleo de tu negocio. Tendrás un servicio integral con tarifa plana a tu disposición, al mismo precio de una asesoría barata, pero contando con la excelencia de un servicio especializado, respaldado por la confianza de miles de clientes como tú.</p>
                                <p>Puedes dejar en nuestras manos todas las gestiones laborales, fiscales, contables y de asesoramiento jurídico, porque todas están incluidas dentro de tu cuota mensual con el servicio de asesoría online y presencial.</p>
                            </div>
                            <div class="col-sm-9 col-lg-4 subpanel-why">

                                <p>Tenemos una metodología de trabajo propia, desarrollamos tecnología como Selfconta, el programa de facturación y contabilidad gratuito para facilitar el día a día de cualquier pequeño y mediano negocio como el tuyo y que puedes usar sin límite en el momento que decidas, incluso si no quieres o no necesitas contratar nuestro servicio.</p>
                                <p>Se acabó acumular papeleo, sólo con descargándote nuestra app podrás digitalizar las facturas, envíalas a tu asesor fiscal con una simple fotografía y acceder a tus documentos desde cualquier parte.</p>


                            </div>
                            <div class="col-sm-9 col-lg-4 subpanel-why">
                                <p>No somos ni una simple gestoría online ni una "gestoría barata", somos la gestoría que está revolucionando el sector para hacer la vida más fácil a toda autónomo o empresa que quiera simplificar la gestión de su negocio. Elige entre una amplia gama de tarifas a tu medida, tú decides si quieres comunicarte con tu asesor personal vía email, telefónica o cara a cara yendo directamente a la oficina. Siempre obtendrás un trato cercano, un servicio rápido, cómodo y sencillo.</p>

                            </div>
                            <h2>¿Prefieres el servicio de asesoría online o presencial?</h2>
                            <div class="img-why">
                                <img src="img/face.jpg" alt=":D" />
                            </div>
                        </div>
                    </div>
                </div>




            </div>


            <!--        ---------------------------------- FIN WHY --------------------------------------->

            <!--        ---------------------------------- SERVICES --------------------------------------->

            <?php
include 'required/services-module.php';
?>
                <!--        ---------------------------------- FIN SERVICES --------------------------------------->
                <!--        ---------------------------------- PUBLICIDAD --------------------------------------->

                <?php
include 'required/bannerSelfconta.php';
?>
                    <!--        ---------------------------------- FIN PUBLICIDAD --------------------------------------->


                    <!--        ---------------------------------- CLIENTES --------------------------------------->






                    <div class="col-sm-12 col-lg-9 range-clientes-mobile" id="clientes-mobile">
                        <img src="img/bg-mobile.jpg" id="bg-clientes-mobile" alt="Fondo clientes" />
                        <img id="cuadro-range-mobile" src="img/range-mobile.png" alt="Rango clientes" />

                        <div class="row contenido-slide-clientes ">
                            <div class="col-sm-12 col-lg-7 datos-clientes">
                                <div class="datos-range-clientes">
                                    <div class="float-left divClientes">
                                        <img src="img/icon-user.png" alt="Icono usuario"><span id="numClientes-mobile">0</span>
                                    </div>
                                    <div class="linea float-left"></div>
                                    <div class="float-left">
                                        <p class="text1-range-clientes">clientes</p>
                                        <p class="text2-range-clientes" id="fecha"></p>
                                    </div>
                                </div>
                                <div class="datos-range-clientes">

                                    <p class="button-range-clientes"><a class="ir-formulario">Yo también quiero!!</a></p>
                                </div>
                            </div>

                        </div>

                    </div>










                    <div class="row clients" id="clients">



                        <h2 class="titulo-clientes">MILES DE CLIENTES CONFÍAN EN NOSOTROS</h2>


                        <div class="col-sm-8 col-md-5 col-lg-3 opiniones">
                            <a href="https://www.ayudatpymes.com/opiniones/" target="_blank"><p class="title-opiniones">Opiniones de Ayuda T clientes</p></a>
                            <div id="demo" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <p class="comentario">"Me gusta por la sencillez y rapidez de nuestro gestor y sobre todo la facilidad de tener todo disponible en cualquier momento”</p>
                                        <p class="autor">Juan Villa Carrasco.</p>
                                        <p class="empresa">Gerente de <span>Voip telecom S.L.</span></p>
                                        <p class="url"><a >voiptelecom.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Me gusta Ayuda-T-Pymes " por su rápida respuesta y acertada" la recomiendo por su precio."</p>
                                        <p class="autor">Gerente de solupcdoctor</p>
                                        <p class="url"><a>solupcdoctor.es</a></p>

                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por su sencilla plataforma y su sencillez de uso, y por la excelente atención al cliente."</p>
                                        <p class="autor">Daniel Tomas</p>
                                        <p class="empresa">Gerente de <span>dtconsultor</span></p>
                                        <p class="url"><a>dtconsultor.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Ayuda-T nos está ayudando a sacar adelante nuestro proyecto, con sus consejos y su trabajo efectivo."</p>
                                        <p class="autor">Javier Calleja</p>
                                        <p class="url"><a>flunorte.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Siempre están cuando los necesito. Además de mucha profesionalidad y buen servicio. Y todo esto a un precio increíble."</p>
                                        <p class="autor">Inmaculada Ruiz</p>
                                        <p class="empresa">Gerente R.D. de <span>Nutricion Jerez S.L.</span></p>
                                        <p class="url"><a>Nutricion Jerez S.L.</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Mi asesor es el mejor!"</p>
                                        <p class="autor">Javier Infantes Martín </p>
                                        <p class="url"><a>Nutricion Jerez S.L.</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Muy buena. Siempre me ha atendido que prontitud y dado soluciones."</p>
                                        <p class="autor">Luis Alberto Encina Rojas </p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"No tenemos hasta el momento sugerencias de mejora. Nuestra súper asesora Laura cumple perfectamente con nuestras expectativas."</p>
                                        <p class="autor">Juri-Dileyc SL</p>
                                        <p class="url"><a>juri-dileyc.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por su profesionalidad, trato personalizado y empatía con el cliente."</p>
                                        <p class="autor">Gerente PresumeBox Ecommerce Report S.L.</p>
                                        <p class="url"><a>presumedebebe.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"me gusta por la atención y por el precio final, y se lo recomendaría por lo mismo."</p>
                                        <p class="autor">Gerente Laredo Express S.L.</p>
                                        <p class="url"><a >laredoexpress.com</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Por la rapidez en la respuesta y la cercanía en el trato a pesar de la distancia.”</p>
                                        <p class="autor">Javier</p>
                                        <p class="empresa">Administrador de <span>inbade.es</span></p>
                                        <p class="url"><a>inbade.es</a></p>
                                    </div>
                                    <div class="carousel-item">
                                        <p class="comentario">"Me simplifican mucho las cosas. Estoy empezando y se agradece."</p>
                                        <p class="autor">Angel Canas</p>
                                        <p class="url"><a>Field Engineer Autónomo</a></p>
                                    </div>

                                </div>
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-caret-left flecha-opiniones-izquierda"></i>
  </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-caret-right flecha-opiniones-derecha"></i>
  </a>
                            </div>
                        </div>
                        <h2 class="text-oculto-clientes">MILES DE CLIENTES CONFÍAN EN NOSOTROS</h2>
                        <div class="col-sm-12 col-lg-9 range-clientes">
                            <img src="img/bg.jpg" id="bg-clientes" alt="Fondo clientes" />
                            <img id="cuadro-range" src="img/range.png" alt="Rango clientes" />

                            <div class="row contenido-slide-clientes ">
                                <div class="col-sm-12 col-lg-7 datos-clientes">
                                    <div class="datos-range-clientes">
                                        <div class="float-left divClientes">
                                            <img src="img/icon-user.png" alt="Icono usuario"><span id="numClientes">0</span>
                                        </div>
                                        <div class="linea float-left"></div>
                                        <div class="float-left">
                                            <p class="text1-range-clientes">clientes</p>
                                            <p class="text2-range-clientes" id="fecha2"></p>
                                        </div>
                                    </div>
                                    <div class="datos-range-clientes">

                                        <p class="text3-range-clientes" id="profesion">Arquitectos</p>
                                        <p class="button-range-clientes"><a class="ir-formulario">Yo también quiero!!</a></p>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-3 foto-cliente">
                                    <img id="foto1" class="foto-cliente-item" src="img/gestoria-online-alfarero.png" alt="Gestoría OnlinAlfarero">
                                    <img id="foto2" class="foto-cliente-item" src="img/gestoria-online-artesano.png" alt="Gestoría Online Artesano">
                                    <img id="foto3" class="foto-cliente-item" src="img/gestoria-online-cocinero.png" alt="Gestoría Online Cocinero">
                                    <img id="foto4" class="foto-cliente-item" src="img/gestoria-online-escritor.png" alt="Gestoría Online Escritor">
                                    <img id="foto5" class="foto-cliente-item" src="img/gestoria-online-forjador.png" alt="Gestoría Online Forjador">
                                    <img id="foto6" class="foto-cliente-item" src="img/gestoria-online-fotografa.png" alt="Gestoría Online Fotógrafa">
                                    <img id="foto7" class="foto-cliente-item" src="img/gestoria-online-fotografo.png" alt="Gestoría Online Fotógrafo">
                                    <img id="foto8" class="foto-cliente-item" src="img/gestoria-online-grafitero.png" alt="Gestoría Online Grafitero">
                                    <img id="foto9" class="foto-cliente-item" src="img/gestoria-online-modelo.png" alt="Gestoría Online Modelo">
                                    <img id="foto10" class="foto-cliente-item" src="img/gestoria-online-tatuador.png" alt="Gestoría Online Tatuador">
                                </div>
                            </div>

                        </div>

                    </div>


                    <!--        ---------------------------------- FIN CLIENTES --------------------------------------->

                    <!--        ---------------------------------- PREMIOS --------------------------------------->


                    <div class="row premios justify-content-sm-center">
                        <p class="titulo-escritorio">También nos han dado <span>ALGÚN QUE OTRO PREMIO</span></p>
                        <p class="titulo-mobile">También nos han dado <br><span>ALGÚN QUE OTRO PREMIO</span></p>
                        <div class="container">
                            <section class="customer-logos slider">
                                <div class="slide"><a href="https://www.20minutos.es/fotos/actualidad/ganadores-de-la-xi-edicion-de-los-premios-20blogs-13089/9/" rel="nofollow" target="_blank"><img src="img/premio.jpg" alt="premio"></a></div>
                                
                                <div class="slide"><a href="http://www.europapress.es/esandalucia/cadiz/noticia-jimenez-barrios-destaca-estatuto-autonomia-inspirado-alguna-leyes-mas-avanzadas-pais-20170224161036.html" rel="nofollow" target="_blank"><img src="img/premio2.jpg" alt="premio2"></a></div>
                                
                                <div class="slide"><a href="https://www.20minutos.es/noticia/2369088/0/ayuda-t-pymes-omniumlab-ganadoras-premios-aje-trayectoria-empresarial-iniciativa-emprendedora/" rel="nofollow" target="_blank"><img src="img/premio4.jpg" alt="premio4"></a></div>
                                
                                <div class="slide"><a href="https://www.lavozdigital.es/cadiz/lvdi-entrega-premios-cadizesdigital-201611222116_noticia.html" rel="nofollow" target="_blank"><img src="img/premio5.jpg" alt="premio5"></a></div>
                                
                                <div class="slide"><a href="https://www.diariodesevilla.es/andalucia/limites-nuevo-talento-andaluz_0_669533416.html" rel="nofollow" target="_blank"><img src="img/premio6.jpg" alt="premio6"></a></div>
                                
                                <div class="slide"><a href="https://www.elmira.es/01/12/2017/mira-comunicacion-healthy-blue-bits-y-ayuda-t-pymes-se-alzan-con-los-premios-cadiztic/" rel="nofollow" target="_blank"><img src="img/premio7.jpg" alt="premio7"></a></div>
                                
                                <div class="slide"><a href="https://www.lavozdigital.es/cadiz/lvdi-entrega-premios-cadizesdigital-201611222116_noticia.html" rel="nofollow" target="_blank"><img src="img/premio8.jpg" alt="premio8"></a></div>
                            </section>
                        </div>
                    </div>
        </div>

        <!--        ---------------------------------- FIN PREMIOS --------------------------------------->

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row formulario  justify-content-sm-center">
            <p class="tit-formulario">¿QUIERES ZANJAR CUALQUIER DUDA? </p>
            <h3>Deja tus datos y te llamamos sin compromiso</h3>
            <?php 
                        include 'required/form-body.php';
                        ?>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- EMPRESAS --------------------------------------->
    <script>
        $("#submit").bind('click', function() {
            var email = $("#email").val();
            var news = $("#news");
            $("#form-section1").submit();




        });
        $("#submit2").bind('click', function() {
            var email = $("#email2").val();
            var news = $("#news2");
            $("#form-section2").submit();




        });

    </script>
    <?php
include 'required/footer.php';
?>
