﻿    <div class="container">    
         <div class="row empresas justify-content-md-center">
             <a href="https://www.ayudatpymes.com"><div class="item-empresas item-empresas1"></div></a> 
             <a href="http://ayudat.es/legal"><div class="item-empresas item-empresas2"></div></a> 
             <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank"><div class="item-empresas item-empresas3"></div></a> 
             <a href="http://ayudat.es/learning" target="_blank"><div class="item-empresas item-empresas4"></div></a> 
             <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><div class="item-empresas item-empresas5"></div></a> 
             <a href="http://liquidoo.es/" target="_blank"><div class="item-empresas item-empresas6"></div></a> 
             <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank"><div class="item-empresas item-empresas7"></div></a> 
             <a href="http://ayudat.es/alma" target="_blank"><div class="item-empresas item-empresas8"></div></a> 
            
        </div>
    
<!--        ---------------------------------- FIN EMPRESAS --------------------------------------->
        
<!--        ---------------------------------- FOOTER --------------------------------------->      

        <footer>
            <div class="logo-footer"><img src="img/logo-footer.jpg" alt="AyudaT Pymes"/></div>
            <div class="row footer">
                
                <div class="col-sm-12 col-lg-7">
                    <div class="col-sm-4 col-lg-4 web-map">
                        <a href="/">Inicio</a>
                        <a href="servicios">Servicios</a>
                        <a href="asesoria">Autónomo y empresas</a>
                        <a href="alta-autonomos">Alta autónomo</a>
                        <a href="crear-empresa">Crear empresa</a>
                        <a href="abrir-sucursal">Abrir sucursal</a>
                        <a href="emprendedores">Emprendedores</a>
                        <a href="empresas-sociales">Empresas sociales</a>
                        <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
                    </div>
                    <div class="col-sm-4 col-lg-4 web-map">
                        <a href="juridica">Asesoría jurídica</a>
                        <a href="patentes-y-marcas">Registro de marcas</a>
                        <a href="servicio-rgpd">RGPD</a>
                        <a href="curso-online">Cursos de formación</a>
                        <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
                        <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
                        <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
                        <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
                        <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>
                        
                    </div>
                    <div class="col-sm-3 col-lg-3 contact">
                        <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                        <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                        <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
                        <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    
                    <div class="col-sm-12 col-lg-12 contact-img">
                        <p>Conectar</p>
                        <div class="social">
                            <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="img/fb_icon.png" alt="facebook" /></a>
                            <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="img/tw_icon.png" alt="tweeter"/></a>
                            <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="img/ins_icon.png" alt="instagram" /></a>
                            <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="img/lin_icon.png" alt="linkedin"/></a>
                        </div>
                        <p>¿Alguna duda laboral, fiscal o contable?</p>
                        <a href="http://www.ayudapedia.es/" target="_blank"><img src="img/ayudapedia.jpg" alt="Ayudapedia" /></a>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2 footer-phone">
                    <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
                <p class="footer-phone-txt">Infórmate sin compromiso</p>
                 
                <p class="footer-phone-txt2">Atención al cliente</p>    
                    <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
                <p class="footer-phone-txt3">clientes@ayudatpymes.com</p>
                
                </div>
            </div>
            <div class="row legal justify-content-md-center">
                <img src="img/logo-footer-legal.jpg" alt="AyudaT"/>
                <p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>
                <p class="condiciones-legal">*Los precios que aparecen en la web no incluyen IVA, para saber qué incluye el servicio desde 9,99€, el presentador de impuestos, consulte condiciones con el comercial.</p>
            </div>
        </footer>
<!--         --------------------------------------------------------------------------------------------------------------------------------------TEST-------------------------------------------------------------------------------------------------------------------------------------------->

<span class="ir-arriba icon-arrow-up2"></span>  
        
<?php
     if(isset($_GET['gracias'])){   
?>
        <!-- Google Code for Formulario OK Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_conversion_language = "es";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "74JwCNaUrQQQsNGj7wM";
var google_conversion_value = 1.00;
var google_conversion_currency = "EUR";
var google_remarketing_only = false;
/* ]]> */
    
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1038674096/?value=1.00&amp;currency_code=EUR&amp;label=74JwCNaUrQQQsNGj7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
        
    </div>

<style>
.tabs-section2 .col {
    opacity: 1;
}
</style>
    
<?php } ?>
<!-- Google Code para etiquetas de remarketing -->
			<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
<script src="js/script.js"></script>
 <?php 
    if ($_SERVER['REQUEST_URI'] === '/' || $_SERVER['REQUEST_URI'] === '/?gracias') { 
    ?>

<?php } ?>

</html>