
<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    

    <?php
    if (strpos($_SERVER['REQUEST_URI'], 'servicios')) { 
    ?>
     
    <title>Servicios asesoría</title>
    <meta property="og:title" content="Asesoría online y presencial líder España + de <?php echo $clientes ?> clientes" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Asesoría online y presencial, declaración de impuestos desde 9,99€. Gestión laboral, fiscal, contable y jurídica + software de facturación GRATIS." />
    <meta name="description" content="Asesoría online y presencial, declaración de impuestos desde 9,99€. Gestión laboral, fiscal, contable y jurídica + software de facturación GRATIS." />
    
    <link rel="stylesheet" href="css/style-servicios.css">
    <?php } 
    else if (strpos($_SERVER['REQUEST_URI'], 'asesoria') || strpos($_SERVER['REQUEST_URI'], 'display')) { ?>
    
     <title>Gestoría para autónomos y empresas líder España desde 9,99€</title>
    <meta property="og:title" content="Gestoría para autónomos y empresas líder España desde 9,99€" />
    <meta property="og:url" content="https://www.ayudatpymes.com/asesoria" />
    <meta property="og:description" content="Asesoría para autónomos y empresas: gestión laboral, fiscal, contable y jurídica + programa facturación GRATIS. Sin permanencia." />
    <meta name="description" content="Asesoría para autónomos y empresas: gestión laboral, fiscal, contable y jurídica + programa facturación GRATIS. Sin permanencia." />
    
    <link rel="stylesheet" href="css/style-asesoria.css">
        
    <?php } 
    else if (strpos($_SERVER['REQUEST_URI'], 'empresas-sociales')) { 
    ?>
    
    <title>Asesoría para fundaciones, asociaciones y empresas sociales</title>
    <meta property="og:title" content="Asesoría para fundaciones, asociaciones y empresas sociales" />
    <meta property="og:url" content="https://www.ayudatpymes.com/empresas-sociales" />
    <meta property="og:description" content="Asesoría para asociaciones, ONG y fundaciones. Gestión laboral, fiscal, contable y jurídica + programa de facturación GRATIS. Sin permanencia." />
    <meta name="description" content="Asesoría para asociaciones, ONG y fundaciones. Gestión laboral, fiscal, contable y jurídica + programa de facturación GRATIS. Sin permanencia." />
    
    <link rel="stylesheet" href="css/style-empresas-sociales.css">
    <?php } 
    else if (strpos($_SERVER['REQUEST_URI'], 'abrir-sucursal')) { 
    ?>
    <title>Abrir sucursal</title>
    <meta property="og:title" content="Abrir sucursal" />
    <meta property="og:url" content="https://www.ayudatpymes.com/abrir-sucursal" />
    <meta property="og:description" content="Servicio integral para abrir una sucursal en España. Conoce todos los requisitos y déjalo en nuestras manos" />
    <meta name="description" content="Servicio integral para abrir una sucursal en España. Conoce todos los requisitos y déjalo en nuestras manos" />
    
    <link rel="stylesheet" href="css/style-abrir-sucursal.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'crear-empresa')) { 
    ?>
    <title>Crear empresa</title>
    <meta property="og:title" content="Crear empresa" />
    <meta property="og:url" content="https://www.ayudatpymes.com/crear-empresa" />
    <meta property="og:description" content="Si quieres crear una empresa sin mover un dedo déjalo en nuestras manos. Nos encargamos de todos el papeleo" />
    <meta name="description" content="Si quieres crear una empresa sin mover un dedo déjalo en nuestras manos. Nos encargamos de todos el papeleo" />
    
    
    <link rel="stylesheet" href="css/style-crear-empresa.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'alta-autonomos')) { 
    ?>
    <title>Alta autónomos</title>
    <meta property="og:title" content="Alta autónomos" />
    <meta property="og:url" content="https://www.ayudatpymes.com/alta-autonomos" />
    <meta property="og:description" content="Alta autónomos en 3 horas. Olvídate para siempre de todas tus obligaciones fiscales, laborales, contables y jurídicas." />
    <meta name="description" content="Alta autónomos en 3 horas. Olvídate para siempre de todas tus obligaciones fiscales, laborales, contables y jurídicas." />
    
    <link rel="stylesheet" href="css/style-alta-autonomos.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'emprendedores')) { 
    ?>
    
    <title>Asesoría emprendedores. Alta autónomo.</title>
    <meta property="og:title" content="Asesoría emprendedores. Alta autónomo." />
    <meta property="og:url" content="https://www.ayudatpymes.com/startups" />
    <meta property="og:description" content="Asesoría para emprendedores y Startups. Asesoría desde 29,99€. Laboral, fiscal, contable y jurídico + programa de facturación GRATIS." />
    <meta name="description" content="Asesoría para emprendedores y Startups. Asesoría desde 29,99€. Laboral, fiscal, contable y jurídico + programa de facturación GRATIS." />
    
    <link rel="stylesheet" href="css/style-startups.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'juridica')) { 
    ?>
    <title>Asesoría jurídica</title>
    <meta property="og:title" content="Asesoría jurídica" />
    <meta property="og:url" content="https://www.ayudatpymes.com/juridica" />
    <meta property="og:description" content="Asesoría jurídica: constituir tu sociedad, realizar estatutos sociales, gestión de pactos y acuerdos societarios..." />
    <meta name="description" content="Asesoría jurídica: constituir tu sociedad, realizar estatutos sociales, gestión de pactos y acuerdos societarios..." />
    
    <link rel="stylesheet" href="css/style-asesoria-juridica.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'patentes-y-marcas')) { 
    ?>
    <title>Registro de patentes y marcas con investigación gratuita</title>
    <meta property="og:title" content="Registro de patentes y marcas con investigación gratuita" />
    <meta property="og:url" content="https://www.ayudatpymes.com/patentes-y-marcas" />
    <meta property="og:description" content="Registro de marcas y patentes con investigación gratis, comprueba que nadie se haya registrado con tu logo o marca" />
    <meta name="description" content="Registro de marcas y patentes con investigación gratis, comprueba que nadie se haya registrado con tu logo o marca" />
    
    <link rel="stylesheet" href="css/style-patentes.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'servicio-rgpd')) { 
    ?>
    <title>Servicios RGPD</title>
    <meta property="og:title" content="Servicios RGPD" />
    <meta property="og:url" content="https://www.ayudatpymes.com/servicio-rgpd" />
    <meta property="og:description" content="Servicio de protección de datos personales integral: auditoría RGPD, consultoría, implantación y registro" />
    <meta name="description" content="Servicio de protección de datos personales integral: auditoría RGPD, consultoría, implantación y registro" />
    
    <link rel="stylesheet" href="css/style-rgpd.css">
    <?php } 
    else if  (strpos($_SERVER['REQUEST_URI'], 'curso-online')) { 
    ?>
    <title>Curso online de formación bonificada</title>
    <meta property="og:title" content="Curso online de formación bonificada" />
    <meta property="og:url" content="https://www.ayudatpymes.com/curso-online" />
    <meta property="og:description" content="+ de 4.000 cursos online bonificados para empresas y sus trabajadores a coste 0. Formación acreditada por FUNDAE" />
    <meta name="description" content="+ de 4.000 cursos online bonificados para empresas y sus trabajadores a coste 0. Formación acreditada por FUNDAE" />
    
    <link rel="stylesheet" href="css/style-cursos.css">
    <?php } 
    else { 
    ?>
    <title>ASESORÍA ONLINE Y PRESENCIAL | AUTÓNOMOS Y EMPRESAS</title>
    <meta property="og:title" content="Asesoría online y presencial líder España + de <?php echo $clientes ?> clientes" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Asesoría para pymes y autónomos, declaración de impuestos desde 9,99€. Gestión laboral, fiscal, contable y jurídica + software de facturación GRATIS." />
    <meta name="description" content="Asesoría para pymes y autónomos, declaración de impuestos desde 9,99€. Gestión laboral, fiscal, contable y jurídica + software de facturación GRATIS." />
    <?php } ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            <?php 
     if (strpos($_SERVER['REQUEST_URI'], 'servicios')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>SERVICIOS</span>
    <?php } 
   if (strpos($_SERVER['REQUEST_URI'], 'asesoria') || strpos($_SERVER['REQUEST_URI'], 'display')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>AUTÓNOMOS Y EMPRESAS</span>
    <?php } 
    if (strpos($_SERVER['REQUEST_URI'], 'empresas-sociales')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>EMPRESAS SOCIALES</span>
    <?php } 
    if (strpos($_SERVER['REQUEST_URI'], 'abrir-sucursal')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>ABRIR SUCURSAL</span>
    <?php } 
   if  (strpos($_SERVER['REQUEST_URI'], 'crear-empresa')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>CREAR EMPRESA</span>
    <?php } 
     if  (strpos($_SERVER['REQUEST_URI'], 'alta-autonomos')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>ALTA AUTÓNOMO</span>
    <?php } 
    if  (strpos($_SERVER['REQUEST_URI'], 'emprendedores')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>EMPRENDEDORES</span>
    <?php } 
     if  (strpos($_SERVER['REQUEST_URI'], 'juridica')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>ASESORÍA JURÍDICA</span>
    <?php } 
     if  (strpos($_SERVER['REQUEST_URI'], 'patentes-y-marcas')) {  
    ?>
    <span class="breadcrumbs"><span> | </span>PATENTES Y MARCAS</span>
    <?php } 
    if  (strpos($_SERVER['REQUEST_URI'], 'servicio-rgpd')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>SERVICIO RGPD</span>
    <?php } 
    if  (strpos($_SERVER['REQUEST_URI'], 'curso-online')) { 
    ?>
    <span class="breadcrumbs"><span> | </span>CURSOS ONLINE</span>
    <?php } ?>

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
