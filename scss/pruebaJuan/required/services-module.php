<div class="row services justify-content-sm-center">
            <div class="col-sm-12 col-lg-12 col-services">
                <h2>Somos una asesoría, pero…</h2>
                <h3 class="subtitle-services">No iba en broma lo de que aquí encontrarás<br>
                    todas las soluciones para tu negocio.</h3>
                
                
                
                
                <div class="row justify-content-sm-center">
                        <div class="container">
                            <section class="customer-servicios slider">
                                <div class="slide">
                                <h3 class="titulo-servicios1"><a href="juridica">ASESORAMIENTO JURÍDICO</a></h3>
                    <p class="texto-servicios2">La ventaja de la asesoría jurídica<br> especializada en negocios</p>
                                </div>
                                <div class="slide"><h3 class="titulo-servicios2"><a href="servicio-rgpd">SERVICIO RGPD</a></h3>
                    <p class="texto-servicios2">¿Cumples con<br>la normativa?</p></div>
                                <div class="slide"><h3 class="titulo-servicios3"><a href="patentes-y-marcas">PATENTES Y MARCAS</a></h3>
                    <p class="texto-servicios2">Registra tu marca en cualquier<br>país al mismo precio</p></div>
                                
                                <div class="slide"><h3 class="titulo-servicios4"><a href="crear-empresa">CREAR EMPRESAS EXPRÉS</a></h3>
                    <p class="texto-servicios2">En 48 horas, tu negocio<br> totalmente operativo.<br> Mejor precio garantizado.</p></div>
                                
                                <div class="slide"><h3 class="titulo-servicios5"><a href="curso-online">FORMACIÓN</a></h3>
                    <p class="texto-servicios2">Privada o programada,<br> bonificadas y a coste 0.<br> Fórmate tú y a tus empleados.</p></div>
                                
                                <div class="slide"><h3 class="titulo-servicios6"><a href="cambio-nombre-coche/">TRANSFERENCIA DE VEHÍCULOS</a></h3>
                    <p class="texto-servicios2">Empieza a usar tu<br> coche nuevo, el papeleo<br> es cosa nuestra.</p></div>
                            </section>
                        </div>
                    </div>
                
<!--
                <div class="row  justify-content-md-center">
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <h3 class="titulo-servicios1"><a href="juridica">ASESORAMIENTO JURÍDICO</a></h3>
                    <p class="texto-servicios2">La ventaja de la asesoría jurídica<br> especializada en negocios</p>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <h3 class="titulo-servicios2"><a href="servicio-rgpd">SERVICIO RGPD</a></h3>
                    <p class="texto-servicios2">¿Cumples con<br>la normativa?</p>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <h3 class="titulo-servicios3"><a href="patentes-y-marcas">PATENTES Y MARCAS</a></h3>
                    <p class="texto-servicios2">Registra tu marca en cualquier<br>país al mismo precio</p>
                </div>
                </div>    
-->
            </div>
</div>