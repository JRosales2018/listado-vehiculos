<div class="row publi">
            

            <div class="row row-title-publi">
                
                    <h3 class="green title-publi">PROGRAMA DE FACTURACIÓN <br class="visible-xs">Y CONTABILIDAD GRATIS</h3>
                    <h2 class=" subtitle-publi">SEAS CLIENTE O NO,<br class="visible-xs"> PARA SIEMPRE</h2>
                <a class="link-img-banner" href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">
                 <div class="boton-selfconta">
                        <span class="btn-green" target="_blank">Regístrate y úsalo GRATIS</span>
                    </div>   
                <img class="img-publi" src="img/selfconta/aplicacion-gratis.jpg" alt="Aplicación gratis"></a>
                <a class="link-img-banner" href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><img class="img-publi-mobile" src="img/selfconta/consigue-aplicacion-gratis.jpg" alt="Consigue aplicación gratis"></a>
                
            </div>
            <div class="row row-title-publi justify-content-sm-center">
                <div class="col-sm-12 col-md-8 col-lg-8 panel">
                    <a class="desplegable-publi" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        Sí, sí. Has leído bien. <br class="visible-xs">Para Pymes y autónomos como tú. <i class="fa fa-caret-right"></i>

        </a>


                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 panel-body">
                            Selfconta te permite <span>expedir facturas y presupuestos personalizados,</span> con tu logo y elementos corporativos, con un formato adecuado a las premisas de la AEAT. Puedes programar y <span>automatizar el envío de facturas recurrentes</span> y guardar un directorio con tus clientes y proveedores, para completar los datos de las facturas sin que tengas que hacer nada.
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 panel-body">
                            Además de ayudarte a tener una visión clara de los ingresos, gastos y movimientos en las cuentas de tu negocio, Selfconta tiene un <span>sistema de control de morosidad</span> y te permite enviar <span>remesas a tus clientes</span> para cobrarlas directamente. Hay un montón de funciones más que tendrás que descubrir tú solito. ¿Útil, no? <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" class="green" target="_blank">¿Quieres saber más? Infórmate aquí</a>
                        </div>
                            </div>
                    </div>
                    
                </div>
            </div>
          
        </div>
<script>
    $('.link-img-banner').mouseover(function(){
        $('.btn-green').addClass('btn-green-hover').fadeIn( "slow" );
    })
    $('.link-img-banner').mouseout(function(){
        $('.btn-green').removeClass('btn-green-hover').fadeIn( "slow" );
    })
</script>