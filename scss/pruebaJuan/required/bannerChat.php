<style>
/* ////////////////
////  DETALLES ////
/////////////////*/


/* IMG ANIMACION*/

#ad {
    display: block;
    overflow: hidden;
    width: 260px;
    height: 227px;
    position: relative;
    margin: 0 auto;
}

#rest{
    position: absolute;
     width: 260px;
    height: 227px;
    opacity: 1;
    background-color: #fff;
    z-index: 2;
}


    #movil{
        position: absolute;
        opacity: 1;
    }
#sombra{
        position: absolute;
        opacity: 1;
    }
#copy2{
        position: absolute;
        opacity: 1;
    }
#logo-animado{
        position: absolute;
        opacity: 1;
    }

#chat1{
    position: absolute;
    opacity: 1;
    display: none;
    top:150px;
    left: 10px;
}
#chat2{
    position: absolute;
    opacity: 1;
    display: none;
    top:150px;
    left: 10px;
}

#archivo{
    position: absolute;
    opacity: 1;
    display: none;
    top:150px;
    left: 10px;
}

#chat3{
    position: absolute;
    opacity: 1;
    display: none;
    top:170px;
    right: 10px;
}

#chat4{
    position: absolute;
    opacity: 1;
    display: none;
    top:170px;
    right: 10px;
}

#cierre{
     position: absolute;
        opacity: 1;
    transform: scale(1);
}

/* DIVS */


.detalles{
    margin-top: 100px;
}
.detalles h2{
    font-family: 'Rajdhani';
    font-weight: 600;
    font-size: 29px;
    width: 100%;
    text-align: center;
    color: #F35B33;
}

.detalles h2 span{
    font-weight: bold;
}
.carousel-indicators li {
    position: relative;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    width: auto;
    margin-right: 3px;
    margin-left: 3px;
    text-indent: 0;
    font-family: 'Rajdhani';
    font-weight: normal;
    font-size: 12px;
    color:#808183;
    cursor: pointer;
}

.carousel-indicators .active{
    font-weight: bold;
}

.carousel-item img{
    float:right;
    padding-top: 7px;
}

.detalles .texto-detalles{
    padding-left: 0px;
}

.detalles .texto-detalles p{
    font-family: 'Rajdhani';
    font-weight: normal;
    font-size: 18px;
    color:#808183;
}

.detalles .texto-detalles p span{
    font-weight: bold;
}

#carousel-inner-detalles{
    min-height: 170px;
}

.contenido-carousel{
    margin-top: 30px;
}

.boton-descripcion{text-align: center;
    width: 100%;
margin-top: 10px;
color:#fff;
cursor: pointer}
.boton-descripcion a{
        padding: 5px 15px;
    color: #fff;
    font-family: 'Rajdhani';
    font-size: 16px;
    font-weight: 600;
    background-color: #F35B33;
    border-radius: 3px;
    
}

</style>
<div class="row detalles justify-content-sm-center" id="detalles">
                <h2><span>DESPREOCÚPATE<br class="visible-xs"> DEL PAPELEO.</span><br class="visible-md">VAMOS A PONÉRTELO<br class="visible-xs"> MUY FÁCIL</h2>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div id="ad">
                        <div id="rest"></div>
                        <img src="img/animacion/sombra.png" id="sombra" alt="sombra" />
                        <img src="img/animacion/movil.png" id="movil" alt="movil" />
                        <img src="img/animacion/copy2.png" id="copy2" alt="copy2" />
                        <img src="img/animacion/logo-animado.png" id="logo-animado" alt="logo-animado" />
                        <img src="img/animacion/chat1.png" id="chat1" alt="chat1" />
                        <img src="img/animacion/chat2.png" id="chat2" alt="chat2" />
                        <img src="img/animacion/archivo.png" id="archivo" alt="archivo" />
                        <img src="img/animacion/chat3.png" id="chat3" alt="chat3" />
                        <img src="img/animacion/chat4.png" id="chat4" alt="chat4" />
                        <img src="img/animacion/cierre.png" id="cierre" alt="cierre" />
                    </div>
                    <script>
                        var tl3 = new TimelineLite({
                            onComplete: function() {
                                this.restart();
                            }
                        });

                        tl3.to("#rest", .1, {
                            opacity: 0
                        });
                        tl3.from("#sombra", 1.5, {
                            opacity: 0
                        });
                        tl3.from("#movil", 1.5, {
                            top: -100,
                            ease: Power4.easeInOut
                        }, "-=1.5");
                        tl3.from("#copy2", .5, {
                            opacity: 0
                        });
                        tl3.from("#logo-animado", .5, {
                            scale: 0,
                            top: -20
                        });

                        tl3.to("#sombra", .5, {
                            opacity: 0,
                            delay: 1
                        });
                        tl3.to("#movil", .5, {
                            opacity: 0
                        }, "-=.3");
                        tl3.to("#copy2", .5, {
                            opacity: 0
                        }, "-=.3");
                        tl3.to("#logo-animado", .5, {
                            opacity: 0
                        }, "-=.3");

                        tl3.from("#chat1", .3, {
                            top: 230
                        });
                        tl3.to("#chat1", .1, {
                            display: "block"
                        }, "-=.2");
                        tl3.to("#chat1", .3, {
                            top: 100,
                            delay: .7
                        });

                        tl3.from("#chat2", .3, {
                            top: 230
                        }, "-=.3");
                        tl3.to("#chat2", .1, {
                            display: "block"
                        }, "-=.2");
                        tl3.to("#chat2", .3, {
                            top: 100,
                            delay: .7
                        });
                        tl3.to("#chat1", .3, {
                            top: 50
                        }, "-=.3");



                        tl3.from("#archivo", .3, {
                            scale: 0
                        }, "-=.3");
                        tl3.to("#archivo", .1, {
                            display: "block"
                        }, "-=.2");
                        tl3.to("#archivo", .3, {
                            top: 100,
                            delay: 2
                        });
                        tl3.to("#chat2", .3, {
                            top: 50
                        }, "-=.3");
                        tl3.to("#chat1", .3, {
                            top: 0
                        }, "-=.3");


                        tl3.from("#chat3", .3, {
                            top: 230
                        }, "-=.3");
                        tl3.to("#chat3", .1, {
                            display: "block"
                        }, "-=.2");
                        tl3.to("#chat3", .3, {
                            top: 120,
                            delay: .7
                        });
                        tl3.to("#archivo", .3, {
                            top: 50
                        }, "-=.3");
                        tl3.to("#chat2", .3, {
                            top: 0
                        }, "-=.3");
                        tl3.to("#chat1", .1, {
                            display: "none"
                        }, "-=.6");

                        tl3.from("#chat4", .3, {
                            top: 230
                        }, "-=.3");
                        tl3.to("#chat4", .1, {
                            display: "block"
                        }, "-=.2");

                        tl3.to("#chat2", .5, {
                            opacity: 0,
                            delay: 1.5
                        });
                        tl3.to("#archivo", .5, {
                            opacity: 0
                        }, "-=.5");
                        tl3.to("#chat3", .5, {
                            opacity: 0
                        }, "-=.5");
                        tl3.to("#chat4", .5, {
                            opacity: 0
                        }, "-=.5");


                        tl3.from("#cierre", .7, {
                            scale: 0,
                            ease: Elastic.easeOut.config(1, 0.5)
                        });
                        tl3.to("#cierre", .7, {
                            scale: 0,
                            ease: Elastic.easeOut.config(1, 0.5),
                            delay: 10
                        });

                    </script>

                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">


                    <div id="carouselExampleIndicators" class="carousel slide chat-slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">01 -</li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1">02 -</li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2">03 -</li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3">04 -</li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4">05 -</li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="5">06</li>
                        </ol>
                        <div id="carousel-inner-detalles" class="carousel-inner ">
                            <div class="carousel-item active">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/01.jpg" alt="01" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p>Haz consultas a tu asesor y envía documentación <span>instantáneamente desde el chat con tu movil</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/02.jpg" alt="02" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p>Trabaja con un <span>asesor personal</span> que te conoce a ti y a tu negocio.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/03.jpg" alt="03" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p>Acceso ilimitado y gratuito a <span>herramientas súper útiles.</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/04.jpg" alt="04" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p><span>Se acabaron los desplazamientos,</span> podrás trabajar con tu asesor desde cualquier rincón de España.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/05.jpg" alt="05" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p><span>Sube facturas con una simple foto,</span> ponte en contacto con tu asesor instantáneamente y envíale documentación.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row justify-content-sm-center contenido-carousel">
                                    <div class="col-sm-2 col-lg-2">
                                        <img src="img/06.jpg" alt="06" />
                                    </div>
                                    <div class="col-sm-8 col-md-10 col-lg-10 texto-detalles">
                                        <p>Siempre estamos ahí: <span>servicio de atención al cliente y soporte técnico 24h.</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="boton-descripcion">
                    <a class="ir-formulario">¡Empieza ya!</a>
                </div>
            </div>
            <script>
                $('.chat-slide').carousel({
                    interval: 15000
                })

            </script>