<?php
include 'required/header.php';
$what = 'servicios';
?>
<!--        ---------------------------------- FIN MENU --------------------------------------->
        
<!--        ---------------------------------- CABECERA --------------------------------------->
<div id="servicios-page">
    <div class="container">
        <!--------- CABECERA Escritorio -------->
        <div id="title-black">
            <h1 class="radj-b-30 title-black">
               ¿LA MEJOR GESTORÍA PARA TU NEGOCIO?
            </h1>
        </div>
        <div class="row justify-content-sm-center cabecera">

                
                <div class="col-sm-12 col-lg-3 col-md-6 img-section1">
                    <h2 class="radj-sb-23 tit-precio">SI BUSCAS<br>
<span>MÁS  QUE UNA<br> GESTORÍA BARATA,</span><br> ESTÁS EN EL <br class="visible-md">LUGAR INDICADO  </h2>

                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-orange.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-5 col-lg-3 bloque2-section1">
                    
                    <?php 
                        include 'required/form-cabecera.php';
                        ?>
                </div>
                       <div class="row mar-top-17">
            <h2 class="title-section2 radj-b-23">En Ayuda T Pymes ofrecemos un <span>servicio integral.</span> Tendrás a tu disposición un gestor laboral, gestor fiscal, gestor contable y gestor jurídico, todo al alcance de tu empresa en el mismo lugar.
            </h2>
            <h3>Solicita tu presupuesto de gestoría y seguro que estarás tentado a quedarte ;)</h3>
            <p>Ayuda T Pymes lleva casi una década siendo la gestoría más escogida por el pequeño y mediano negocio en España, ¿no quieres saber por qué?
Tenemos mucho que ofrecer a tu empresa</p>
                           <div class="col-sm-12 justify-content-sm-center face-services"><img src="img/face-services.jpg" alt=":D"/></div>
            </div>
    </div>
            
        
 
<!--        ---------------------------------- FIN ABOUT --------------------------------------->
        
<!--        ---------------------------------- DESCRIPCION --------------------------------------->
        
        <div class="row descripcion justify-content-sm-center">
            <div class="col-lg-8 ">
                 <div class="row justify-content-sm-center">
                     <div class="col-lg-4 col-md-4 col-sm-12 img-servicios1-mobile">
                    
                    <h2>GESTORÍA<br>ESPECIALIZADA </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <p class="txt-descripcion1">Ayuda T Pymes es una gestoría dedicada íntegramente a trabajar con autónomos y sociedades. Nuestros profesionales cuentan con una amplia experiencia en la gestión de trámites de empresa. </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 img-servicios1">
                    
                    <h2>GESTORÍA<br>ESPECIALIZADA </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                <p class="txt-descripcion2">La especialización de nuestra gestoría pone a tu disposición la excelencia de un servicio específico, adaptado a las características y necesidades concretas de un negocio.</p>
                </div>
                     </div>
            </div>
            </div>
       
    
  <!--        ---------------------------------- FIN DESCRIPCION ---------------------------------------> 
<!--        ---------------------------------- GESTORES --------------------------------------->        
        <div class="row gestores justify-content-sm-center">
        
            
            <div class="col-lg-6 col-md-12 col-sm-12 item-left">
                <div class="row justify-content-sm-center">
                <div class="col-lg-7 col-md-7 col-sm-12 img-gestor"><img class="gestor1" src="img/gestor-laboral.jpg" alt="Gestor Laboral" /></div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>GESTIÓN LABORAL</h2>
                    <h3>+ DE 90 Gestores</h3>
                    <p>Todos los trámites necesarios en tu negocio en materia de gestión laboral, corren de nuestra cuenta. Con nuestro servicio contarás con un gestor laboral que se encargará de solucionar todo el papeleo necesario y de asesorarte en todo momento para tomar las decisiones más oportunas para tu negocio.</p>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 img-gestor-mobile"><img class="gestor1" src="img/gestor-laboral.jpg" alt="Gestor Laboral" /></div>
                <div class="col-lg-12">
                <p class="gestor-name">Julia | Coordinadora gestión laboral</p>
                <p class="gestor-description">El alma de la fiesta y una it girl en la oficina</p>
                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 item-right">
                <div class="row justify-content-sm-center">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>GESTOR FISCAL</h2>
                    <h3>+ DE 20 Gestores</h3>
                    <p>Un gestor fiscal personal, asignado para ti y tu empresa. Tus trámites, modelos y declaraciones ante la Agencia Tributaria siempre estarán a cargo de una misma persona que se encargará de cumplir plazos y de que la gestión sea la adecuada.</p>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12"><img class="gestor2" src="img/gestor-fiscal.jpg" alt="Gestor Fiscal" /></div>
                <div class="col-lg-12">
                <p class="gestor-name">Almudena | Coordinadora y formadora</p>
                <p class="gestor-description">Ha hecho el Camino de Santiago 3 veces</p>
                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 item-left">
                <div class="row justify-content-sm-center">
                <div class="col-lg-7 col-md-7 col-sm-12 img-gestor"><img class="gestor3" src="img/gestor-contable.jpg" alt="Gestor Contable" /></div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>GESTOR CONTABLE</h2>
                    <h3>EQUIPO CONTABLE</h3>
                    <p>Imagina dar carpetazo a toda la contabilidad de tu negocio con la tranquilidad de dejarlo en manos de la gestoría que trabaja con más pymes y autónomos a nivel nacional. Tenemos algo de experiencia al respecto, puedes estar tranquilo. </p>
                </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 img-gestor-mobile"><img src="img/gestor-contable.jpg" class="gestor3" alt="Gestor Contable" /></div>
                <div class="col-lg-12">
                <p class="gestor-name">Eva | Gestora contable</p>
                <p class="gestor-description">Le flipa bailar flamenco</p>
                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 item-right">
                <div class="row justify-content-sm-center">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>GESTOR JURÍDICO</h2>
                    <h3>EQUIPO JURÍDICO</h3>
                    <p>¿Necesitas asesoramiento jurídico en tu negocio? Ayuda-T Pymes pondrá a tu disposición un gestor jurídico que se encargue de supervisar que todos los aspectos legales que afectan a tu actividad estén a punto.</p>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12"><img class="gestor4" src="img/gestor-juridico.jpg" alt="Gestor Jurídico" /></div>
                <div class="col-lg-12">
                <p class="gestor-name">Julio | Director Ayuda T Legal</p>
                    <p class="gestor-description">Un tipo muy legal.</p>
                </div>
                </div>
            </div>
            
            
            </div>
        
<!--        ---------------------------------- FIN GESTORES ---------------------------------------> 
   
 <!--        ---------------------------------- SERVICES --------------------------------------->

        <?php
include 'required/services-module.php';
include 'required/bannerSelfconta.php';
?>
<!--        ---------------------------------- FIN SERVICES --------------------------------------->  
    
    
</div>
    
    
        

        
<!--        ---------------------------------- PUBLICIDAD --------------------------------------->


<!--        ---------------------------------- FIN PREMIOS --------------------------------------->
        
<!--        ---------------------------------- FORMULARIOS --------------------------------------->    
        
    <div class="row pre-formulario  justify-content-md-center">
    <h3>
        <span>En Ayuda T Pymes</span> estamos especializados en atender a <span>todo tipo de negocios. Si buscas algo más que una gestoría barata para tu empresa, ¡bingo!</span> Has llegado a tu sitio.</h3>
    </div>
        <div class="row formulario  justify-content-md-center">
            
            <h3>Deja tus datos y te llamamos sin compromiso</h3>
            <?php 
                        include 'required/form-body.php';
                        ?>
        </div>
</div>
<!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->
        
<!--        ---------------------------------- EMPRESAS --------------------------------------->    
<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop()-500 >= $('.descripcion').offset().top + $('.descripcion').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
    tl4.to(".gestor3", 1,{opacity:1,x:0},"-=.5");
    tl4.to(".gestor4", 1,{opacity:1,x:0},"-=.5");
        }
    });
});
</script>
<?php
include 'required/footer.php';
?>