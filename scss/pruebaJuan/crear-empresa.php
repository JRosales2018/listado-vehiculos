﻿<?php
include 'required/header.php';
$what = 'crear-empresa';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="crear-empresa-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    CREAR EMPRESA SERVICIO PREMIUM
                </h1>
                <h2>Crear una empresa está a sólo una firma</h2>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <div class="precio">
                            <img src="img/precios/precio-crear-empresa.png" alt="Precio crear empresa" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-gestoria justify-content-sm-center">
            <h2><span>CREAR EMPRESA SERVICIO PREMIUM, LA SOLUCIÓN DEFINITIVA</span><br> ¿CÓMO CREAR UNA EMPRESA LIBRÁNDOTE DE TODOS LOS TRÁMITES?</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row gestoria justify-content-sm-center">

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-sm-center">
                        <div class="col-sm-6 col-md-4 col-lg-6">

                            <div class="row">
                                <h2><span>CREAR EMPRESA</span><br>SERVICIO PREMIUM</h2>
                                <img src="img/precios/precio-crear-empresa.png" alt="Precio crear empresa" />
                                <p>Nuestro servicio incluye <span>la creación de empresas (constitución),</span> la búsqueda y comparativa de <span>la mejor oferta de notaría</span> en tu zona, <span>la asistencia jurídica</span> que necesites, <span>generación y formalización de documentos jurídicos.</span> TÚ SÓLO debes aportar UNA FIRMA.</p>

                            </div>

                        </div>


                    </div>
                </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="col-lg-6 pre-item-sucursal">
                <h2>CREAR EMPRESA | SERVICIO PREMIUM</h2>
                <h3>Crear una empresa NUNCA, JAMÁS fue tan sencillo</h3>
                <p><span>¿Qué debes hacer para saber cómo montar una empresa?</span><br> Escucha atento: <span>NADA.</span><br>Si ya has decidido crear una empresa y dejarlo en nuestras manos, puedes empezar a relaja</p>
                <p>Los únicos requisitos para abrir un negocio que necesitas cumplir son ponerte en contacto con AYUDA T PYMES y echar una firma al final del proceso. ¿Qué más se puede pedir?</p>
                <p>Si contratas los servicios de asesoría la creación de la sociedad son 200€.<br>De lo contrario serían 300€<br> (siempre tenemos ofertas especiales para clientes)</p>
            </div>
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img src="img/crear-empresa.jpg" class="gestor1" alt="Crear empresa" />
                    <p class="gestor-name">Juampi y Nadia | Coordinación </p>
                    <p class="gestor-description">No se les pasa una</p>
                </div>
                <div class="col-lg-5">
                    <h2 class="tit-gestores">CREA TU EMPRESA CON SÓLO A UNA FIRMA, SIN TRÁMITES, SIN PAPELEO<br><span>¿Qué no es posible crear una empresa con solo una firma? Te digo yo a ti que sí</span></h2>
                    <p class="text-gestores1">Con tu <span>SERVICIO PREMIUM,</span> nuestra gestoría se encargará de todos los pasos a seguir para abrir un negocio.</p>

                    <li>Empezamos con la creación de empresas y su constitución.</li>
                    <li>Nos hacemos cargo de la inscripción en el Registro Mercantil.</li>
                    <li>Analizamos, filtramos y nos ponemos en contacto con las ofertas de notaría en tu zona.</li>
                    <li>Gestionamos todos los trámites.</li>
                    <li>Generación de documentos jurídicos y formalización de pacto entre socios.</li>

                    <p class="text-gestores1">Tu sociedad recibirá asistencia jurídica especializada en cada momento hasta finalizar el proceso de constitución de la empresa.</p>

                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->



        </div>






        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php
include 'required/services-module.php';
include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->

<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= $('.pre-item-sucursal').offset().top + $('.pre-item-sucursal').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
        }
        
             if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestoria img", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>


    <?php
include 'required/footer.php';
?>
