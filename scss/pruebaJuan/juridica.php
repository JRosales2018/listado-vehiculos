<?php
include 'required/header.php';
$what = 'juridica';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="asesoria-juridica-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                   PROBABLEMENTE LA ASESORÍA JURÍDICA<br>MÁS UTILIZADA POR PYMES Y AUTÓNOMOS
                </h1>
                
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-1 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-deepblue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesoria justify-content-sm-center">

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-sm-center">
                        <div class="col-sm-6 col-md-5 col-lg-6">

                            <div class="row">
                                <h2>ASESORÍA JURÍDICA<br><span>PARA AUTÓNOMOS<br>Y EMPRESAS</span></h2>
                                
                                <p>En Ayuda T Pymes te ofrecemos <span>mucho más que una asesoría jurídica legal.</span> Ponemos a la disposición de tu negocio la experiencia de trabajar con <span>más de <?php echo $clientes ?> clientes en toda España</span>, de ser la asesoría que más Pymes y autónomos gestiona a nivel nacional.</p>
                                <p>Si quieres proporcionar a tu empresa el <span>asesoramiento jurídico de expertos en la gestión de negocios como el tuyo</span>, será un placer trabajar codo con codo.</p>
                                <p><span>Asesoría jurídica legal integral:</span> constitución de sociedades, revisiones de contratos, pactos con socios y realización de Estatutos, asesoramiento jurídico y mercantil especializado.</p>

                            </div>
                            
                        </div>
                        

                    </div>
                </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito <a href="tel:900100162">900 100 162</a>. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="col-lg-6 pre-item-sucursal">
                <h2>LA VENTAJA DE LA ASESORÍA JURÍDICA ESPECIALIZADA EN NEGOCIOS</h2>
                
                <p><span>¿Conoces la ventaja de trabajar con expertos?</span> Gestionaremos cada uno de los trámites de tu empresa. Tu negocio estará a punto en todo aspecto sujeto a aplicación de normativas, leyes y reglamentos en materia de Derecho que necesite en función a la naturaleza de tu actividad.</p>
                <p>En <span>Ayuda T Pymes</span> encontrarás respuesta a todas las necesidades de tu negocio, <br>también materia fiscal:</p>
                <div class="row justify-content-sm-center">
                <div class="col-lg-4">
                    <li>Constitución de sociedades</li>
                    <li>Registro de <br>patentes y marcas</li>
                    <li>Realización<br> de Estatutos Sociales</li>
                </div>
                <div class="col-lg-4">
                    <li>Elaboración de documentos<br> jurídicos</li>
                    <li>Revisión de contratos</li>
                    <li>Gestión de pactos <br>y acuerdos societarios</li>
                </div>
                <div class="col-lg-4">
                    <li>Asesoramiento jurídico<br> legal especializado</li>
                </div>
                </div>
            </div>
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img src="img/asesoria-juridica-oline.jpg" class="gestor1" alt="Asesoría Jurídica Online" />
                    <p class="gestor-name">Julio | Director Ayuda T Legal</p>
                    <p class="gestor-description">Un tipo muy legal.</p>
                </div>
                <div class="col-lg-4">
                    <h2 class="tit-gestores">¿UN LUGAR DONDE ENCUENTRES TODAS LAS SOLUCIONES QUE NECESITA TU NEGOCIO?</h2>
                    <p class="text-gestores1">Vamos a proporcionar a tu empresa información y asesoramiento eficiente, soluciones ajustadas a tus necesidades en materia legal.</p>
                    <p class="text-gestores1">Desde la <span>constitución de tu sociedad</span> y a la realización de <span>Estatutos Sociales</span>, pasando por el registro de la marca y patentes en tu empresa; desarrollo y puesta a punto de todo tipo de documentos jurídicos; revisión de acuerdos y contratos; gestión de los pactos societarios para evitar malentendidos y que todo se realice de acuerdo con la normativa vigente, velado por los derechos e intereses de tu negocio y de ti mismo como empresario.</p>
                </div>
            </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img src="img/asesoria-juridica-ayudat-pymes.jpg" class="gestor2" alt="Asesoría jurídica Ayuda T Pymes"/>
                        <p class="gestor-name">Javi | Asesor Ayuda T Legal</p>
                    <p class="gestor-description">Extrovertido y cinéfilo,<br> nadie se pierde sus auditorías</p>
                    </div>
                    <div class="col-lg-5 float-right">
                        <h2 class="tit-gestores">TRABAJA CON LA ASESORÍA AUTÓNOMOS LÍDER EN ESPAÑA<br>
Tarifa plana autónomos: Laboral | Fiscal | Contable</h2>
                        <p class="text-gestores1">En definitiva, todas las soluciones en cuanto asesoramiento jurídico que exige el desarrollo de toda actividad mercantil satisfechas por un equipo de personas que conocen las necesidades de tu negocio, con la experiencia de dar cobertura durante casi una década a más de #cliente clientes Pymes y autónomos alrededor de toda España.</p>
                        <p class="text-gestores1">Si necesitas <span>asesoramiento jurídico</span> de la mano de <span>expertos</span>, has llegado al sitio indicado. Deja tus datos y te informamos sin ningún tipo de compromiso.</p>
                    </div>
                </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

           

        </div>




        
      
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php

include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->


<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= $('.pre-item-sucursal').offset().top + $('.pre-item-sucursal').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
        }

    });
    
});
</script>

    <?php
include 'required/footer.php';
?>