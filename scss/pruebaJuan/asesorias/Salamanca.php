﻿<?php $clientes = "8500";
$ciudad = 'Salamanca';
?>

<!DOCTYPE html>
<html lang="es">

<head>

    <link rel="icon" type="image/png" href="../img/Pymes-ico.png">
    <link rel="manifest" href="../manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">

    <meta http-equiv="Last-Modified" content="0">

    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">

    <meta http-equiv="Pragma" content="no-cache">
    <!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="../lib/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="../img/logo.png" />



    <title>Tu gestoría en Salamanca para autónomos y pymes por 9,99€/mes</title>
    <meta property="og:title" content="Tu gestoría en Salamanca para autónomos y pymes por 9,99€/mes" />
    <meta property="og:url" content="https://www.ayudatpymes.com/asesoria/Salamanca" />
    <meta property="og:description" content="Mucho más que una gestoría barata, somos la asesoría líder en España. Autónomos desde 9,99€/mes y Pymes desde 24,99€/mes. Incluido contable, laboral y fiscal. ¡TODO!" />
    <meta name="description" content="Mucho más que una gestoría barata, somos la asesoría líder en España. Autónomos desde 9,99€/mes y Pymes desde 24,99€/mes. Incluido contable, laboral y fiscal. ¡TODO!" />

    <link rel="stylesheet" href="../css/style-asesoria.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../css/build.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="../lib/jquery-latest.js"></script>
    <script src="../lib/jquery.min.js"></script>
    <script src="../lib/popper.min.js"></script>
    <script src="../lib/bootstrap.min.js"></script>
    <script src="../js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../lib/TweenMax.min.js"></script>
    <script src="../lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="../lib/cookieBar.css">
    <script type="text/javascript">
        $(document).ready(function() {
            $('.cookie-message').cookieBar({
                closeButton: '.my-close-button',
                hideOnClose: false
            });
            $('.cookie-message').on('cookieBar-close', function() {
                $(this).slideUp();
            });
        });

    </script>

    <?php
    require '../chat_smartsupp.php';
  ?>

        <!-- ======================================================================= -->
        <!-- ===============================PIXELES================================= -->
        <!-- ======================================================================= -->

        <!-- Facebook Pixel Code -->
        <?php
     if(isset($_GET['gracias'])){   
?>
            <script>
                ! function(f, b, e, v, n, t, s) {
                    if (f.fbq) return;
                    n = f.fbq = function() {
                        n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq) f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s)
                }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '660233234086506');
                fbq('track', 'PageView');
                fbq('track', 'CompleteRegistration');

            </script>
            <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>

            <?php } else{?>

            <script>
                ! function(f, b, e, v, n, t, s) {
                    if (f.fbq) return;
                    n = f.fbq = function() {
                        n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq) f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s)
                }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '660233234086506');
                fbq('track', 'PageView');

            </script>
            <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
            <!-- End Facebook Pixel Code -->
            <?php } ?>

            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());

                gtag('config', 'UA-8047470-2');

            </script>



</head>

<body>
    <div class="cookie-message">
        <p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
    </div>
    <div class="topbar"></div>
    <span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="../img/logo.png" alt="AyudaT Pymes" /></a>


            <span class="breadcrumbs"><span> | </span>AUTÓNOMOS Y EMPRESAS</span>


            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="../img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                            <div class="col web-map">
                                <p>ASESORÍA</p>
                                <a href="../servicios">Servicios</a>
                                <a href="../asesoria">Autónomo y empresas</a>
                                <a href="../alta-autonomos">Alta autónomo</a>
                                <a href="../emprendedores">Emprendedores</a>
                                <a href="../empresas-sociales">Empresas sociales</a>
                            </div>
                            <div class="col web-map">
                                <p>LEGAL</p>
                                <a href="../juridica">Asesoría jurídica</a>
                                <a href="../patentes-y-marcas">Registro de marcas</a>
                                <a href="../servicio-rgpd">RGPD</a>
                                <a href="../crear-empresa">Crear empresa</a>
                                <a href="../abrir-sucursal">Abrir sucursal</a>


                            </div>
                            <div class="col web-map">
                                <p>DESPACHOS</p>
                                <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
                                <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>
                            </div>
                            <div class="col web-map">
                                <p>MÁS</p>
                                <a href="../curso-online">Cursos de formación</a>
                                <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
                                <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
                                <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
                                <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
                            </div>
                            <div class="col contact">
                                <a href="/">Inicio</a>
                                <a href="../asesoria-online-ayuda-t-pymes">Conócenos</a>
                                <a href="../contacto-asesoria-ayuda-t-pymes">Contacto</a>
                                <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
                                <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                            </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                                <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1">ASESORÍA<i class="fa fa-caret-down"></i></a>
                                <div id="menu1" class="panel-collapse collapse in contenido-link show">
                                    <a href="../servicios">Servicios</a>
                                    <a href="../asesoria">Autónomo y empresas</a>
                                    <a href="../alta-autonomos">Alta autónomo</a>
                                    <a href="../emprendedores">Emprendedores</a>
                                    <a href="../empresas-sociales">Empresas sociales</a>
                                </div>
                            </div>
                            <div class="col web-map">
                                <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                                <div id="menu2" class="panel-collapse collapse in contenido-link">
                                    <a href="../juridica">Asesoría jurídica</a>
                                    <a href="../patentes-y-marcas">Registro de marcas</a>
                                    <a href="../servicio-rgpd">RGPD</a>
                                    <a href="../crear-empresa">Crear empresa</a>
                                    <a href="../abrir-sucursal">Abrir sucursal</a>
                                </div>
                            </div>
                            <div class="col web-map">
                                <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                                <div id="menu3" class="panel-collapse collapse in contenido-link">
                                    <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
                                    <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>
                                </div>
                            </div>
                            <div class="col web-map">
                                <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                                <div id="menu4" class="panel-collapse collapse in contenido-link">
                                    <a href="../curso-online">Cursos de formación</a>
                                    <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
                                    <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
                                    <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
                                    <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
                                </div>
                            </div>
                            <div class="col contact">
                                <a href="/">Inicio</a>
                                <a href="../asesoria-online-ayuda-t-pymes">Conócenos</a>
                                <a href="../contacto-asesoria-ayuda-t-pymes">Contacto</a>
                                <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
                                <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="autonomos-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                  ASESORÍA SALAMANCA, ¿ERES AUTÓNOMO O EMPRESA?
                </h1>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">
                    <h3 class="radj-sb-23 tit-precio">Todas tus obligaciones cubiertas</h3>
                    <div class="row">
                        <div class="precio">
                            <img src="../img/precios/precio-gestoria-online.png" alt="Precio Gestoría Online" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Servicio de presentación de impuestos</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="../img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">

                        <?php 




if(isset($_GET['gracias'])){
?>
                        <div>
                            <p class="gracias1"><span>¡Genial!</span><br> tu mensaje ha sido<br> enviado con exito</p>
                            <p class="gracias2">En breve nos pondremos<br> en contacto contigo</p>
                        </div>

                        <?php }else { ?>

                        <form id="form-section1" action="../mail/send_mail.php" method="post">
                            <div class="form-group radj-reg-14 form-group-l">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="teléfono*">
                            </div>
                            <div class="form-group radj-reg-14 form-group-r">
                                <input type="email" name="email" class="form-control" id="email" placeholder="email">
                            </div>
                            <div class="form-group radj-reg-14">
                                <textarea class="form-control" name="comment" id="comment" placeholder="consulta"></textarea>
                            </div>

                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input class="form-check-input styled" type="checkbox" id="aviso" name="aviso" required>
                                    <label class="radj-med-10 agree" for="aviso">
                            Acepto las <a href="../terminos_y_condiciones">términos y condiciones</a>, <a href="../politica_de_privacidad" target="_blank">política de privacidad</a> y <a href="../acuerdo_de_procesamiento_de_datos" target="_blank">procesamiento de datos</a>.*
                        </label>
                                </div>

                            </div>
                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input type="checkbox" class="form-check-input" id="news" name="news">
                                    <label class="radj-med-10 agree" for="news">
                            Deseo recibir información acerca de los servicios de Ayuda T un lugar todas las soluciones por medios electrónicos.
                        </label>
                                </div>

                            </div>
                            <input type="hidden" name="posicion" value="<?php echo $_SERVER['REQUEST_URI']?>/cabecera">
                            <button type="submit" id="submit" class="btn btn-primary">QUIERO SABER MÁS</button>
                        </form>


                        <style>


                        </style>


                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-autonomo justify-content-sm-center">
            <h2><span>LA GESTORÍA MÁS INNOVADORA Y TECNOLÓGICA EN SALAMANCA</span><br>AVALADA POR MÁS DE <?php echo $clientes ?> CLIENTES SATISFECHOS</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesorias justify-content-sm-center">
                <h3>Ahorra tiempo encontrando todas las soluciones <br>para tu negocio en un mismo lugar</h3>

                <div class="col-lg-4 item-asesorias item-left-asesorias">
                    <h2>ASESORÍA<br><span>AUTÓNOMOS<br>
SALAMANCA</span></h2>
                    <div class="precio">
                        <img src="../img/precios/precio-asesoria-autonomo.png" alt="Precio asesoría autónomos">
                    </div>
                    <p>Desde presentar tus impuestos, hasta asesorarte y gestionar de forma integral <span>todas tus obligaciones como autónomo a nivel fiscal, laboral, contable y jurídico.</span> Depende de lo que estés buscando, nosotros nos adaptamos a ti y a tu negocio.
                    </p>

                    <p class="boton-asesorias"><a class="ir-formulario">¡Me interesa!</a></p>

                </div>
                <div class="col-lg-4 item-asesorias item-right-asesorias">
                    <h2>ASESORÍA<br><span>EMPRESAS<br>
SALAMANCA</span></h2>
                    <div class="precio">
                        <img src="../img/precios/precio-asesoria-empresas.png" alt="Precio asesoría empresas">
                    </div>
                    <p>Desde la presentación de impuestos trimestrales y anuales de tu negocio, hasta hacernos cargo de cada una de las gestiones de tu sociedad: <span>constitución, impuesto de sociedades, cuentas anuales,</span> laboral, fiscal, contable y jurídico.</p>

                    <p class="boton-asesorias"><a class="ir-formulario">¡Quiero + info!</a></p>
                </div>



            </div>




            <!--        ---------------------------------- FIN ASESORIAS --------------------------------------->

            <!--        ---------------------------------- INTERESAR --------------------------------------->
            <div class="row interesar justify-content-sm-center">
                <h3>
                    Pero, ¡ESPERA! También podemos ayudarte con:
                </h3>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <p><a href="../servicio-rgpd">CUMPLIR CON LA LEY<br>DE PROTECCIÓN DE DATOS</a></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <p><a href="../curso-online">FORMAR SIN COSTES<br>A TU EQUIPO</a></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <p><a href="../patentes-y-marcas">REGISTRAR<br>TU MARCA</a></p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <p><a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">USAR UN PROGRAMA<br>DE FACTURACIÓN GRATIS</a></p>
                </div>
            </div>

        </div>
        <!--        ---------------------------------- FIN INTERESAR --------------------------------------->

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <br class="visible-xs"><a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra empresa es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 




if(isset($_GET['gracias'])){
?>
                        <div>
                            <p class="gracias1"><span>¡Genial!</span><br> tu mensaje ha sido<br> enviado con exito</p>
                            <p class="gracias2">En breve nos pondremos<br> en contacto contigo</p>
                        </div>

                        <?php }else { ?>

                        <form id="form-section2" action="../mail/send_mail.php" method="post">
                            <div class="form-group radj-reg-14 form-group-l">
                                <input type="text" name="phone" class="form-control" id="phone2" placeholder="teléfono*">
                            </div>
                            <div class="form-group radj-reg-14 form-group-r">
                                <input type="email" name="email" class="form-control" id="email2" placeholder="email">
                            </div>
                            <div class="form-group radj-reg-14">
                                <textarea class="form-control" name="comment" id="comment2" placeholder="consulta"></textarea>
                            </div>

                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input class="form-check-input styled" type="checkbox" id="aviso" name="aviso" required>
                                    <label class="radj-med-10 agree" for="aviso">
                            Acepto las <a href="../terminos_y_condiciones">términos y condiciones</a>, <a href="../politica_de_privacidad" target="_blank">política de privacidad</a> y <a href="../acuerdo_de_procesamiento_de_datos" target="_blank">procesamiento de datos</a>.*
                        </label>
                                </div>

                            </div>
                            <div class="form-group form-check">
                                <div class="checkbox">
                                    <input type="checkbox" class="form-check-input" id="news" name="news">
                                    <label class="radj-med-10 agree" for="news">
                            Deseo recibir información acerca de los servicios de Ayuda T un lugar todas las soluciones por medios electrónicos.
                        </label>
                                </div>

                            </div>
                            <input type="hidden" name="posicion" value="<?php echo $_SERVER['REQUEST_URI']?>/body">
                            <button type="submit" id="submit2" class="btn btn-primary">QUIERO SABER MÁS</button>
                        </form>


                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img class="gestor1" src="../img/asesoria-empresas.jpg" alt="Asesoría empresas" />
                    <p class="gestor-name">Jorge | Director de Operaciones</p>
                    <p class="gestor-description">Le flipa el deporte y es súper competitivo, en el buen sentido ;)</p>
                </div>
                <div class="col-lg-5">
                    <h2 class="tit-gestores">ASESORÍA EMPRESAS ESPECIALIZADA EN SALAMANCA<br> LA EMPRESAS MÁS INNOVADORA Y TECNOLÓGICA</h2>
                    <p class="text-gestores2">¿Cansado de buscar asesoría en Salamanca y no encontrar lo que necesitas?</p>
                    <div class="panel1">
                        <a class="text-gestores1" data-toggle="collapse" data-parent="#accordion" href="#collapseAuto">Nosotros nos encargamos de todo mientras tú dedicas toda esa energía<br> a atender tu negocio. <i class="fa fa-caret-right"></i>

        </a>


                        <div id="collapseAuto" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Llevamos una década desarrollando herramientas tecnológicas y soluciones a medida a las necesidades administrativas de tu empresa. <span>Somos la asesoría online y gestoría presencial en Salamanca que revolucionó el sector introduciendo el salto digital.</span>
                            </div>
                            <div class="panel-body">
                                Desde enviar facturas a tu asesor para que las mecanice con una simple fotografía, a <span>realizar consultas 24/7</span> directamente desde un chat al que acceder desde tu propio dispositivo móvil.
                            </div>
                            <div class="panel-body">
                                <span>Todo está pensado para ahorrarte tiempo,</span> esfuerzo, desplazamientos y complicaciones. Somos gente práctica y el sistema es sencillo, ir un paso más allá: ponértelo fácil y adaptarnos a tus necesidades, para que puedas dedicarte a tu negocio mientras nosotros nos encargamos de todo el papeleo.

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img class="gestor2" src="../img/asesoria-autonomos.jpg" alt="Asesoría autónomos" />
                        <p class="gestor-name">Alex | Director Área Administrativa</p>
                        <p class="gestor-description">Le encanta viajar y plantarse en el quinto pino cada 2x3.</p>
                    </div>
                    <div class="col-lg-5 float-right">
                        <h2 class="tit-gestores">TRABAJA CON LA ASESORÍA <br class="visible-xs">EN SALAMANCA LÍDER EN ESPAÑA<br> Más de
                            <?php echo $clientes ?> clientes ya confían en nosotros,<br class="visible-xs"> ¿te unes?</h2>
                        <p class="text-gestores1">Si no necesitas el asesoramiento de un gestor, <span>no pagues de más.</span> ¿Sólo quieres que presentemos tus impuestos? Genial, tenemos justo lo que estás buscando desde 9,99€* mes. Tal vez prefieres <span>despreocuparte y que un experto se encargue de todo.</span> Sin problema, seas autónomo o empresa, puedes dejarlo todo en nuestras manos. Desde la presentación de impuestos (declaraciones de IVA, Retenciones IRPF, RENTA, liquidación del Impuesto de Sociedades), pasando por la gestión laboral y contable, hasta recibir asesoramiento legal especializado.
                        </p>
                    </div>
                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

            <!--        ---------------------------------- OPINIONES --------------------------------------->

        </div>
    </div>

    <div class="row slide-opiniones justify-content-sm-center">
        <h2>MILES DE CLIENTES CONFÍAN EN NOSOTROS</h2>
    </div>


    <div class="container">
        <div class="row opiniones-autonomo justify-content-sm-center">
            <div class="col-sm-5 col-lg-4">
                <a href="https://www.ayudatpymes.com/opiniones/" target="_blank">
                    <p class="title-opiniones1">Opiniones de Ayuda T clientes</p>
                </a>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <p class="comentario">"Me gusta por la sencillez y rapidez de nuestro gestor y sobre todo la facilidad de tener todo disponible en cualquier momento”</p>
                            <p class="autor">Juan Villa Carrasco.</p>
                            <p class="empresa">Gerente de <span>Voip telecom S.L.</span></p>
                            <p class="url"><a>voiptelecom.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me gusta Ayuda-T-Pymes " por su rápida respuesta y acertada" la recomiendo por su precio."</p>
                            <p class="autor">Gerente de solupcdoctor</p>
                            <p class="url"><a>solupcdoctor.es</a></p>

                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su sencilla plataforma y su sencillez de uso, y por la excelente atención al cliente."</p>
                            <p class="autor">Daniel Tomas</p>
                            <p class="empresa">Gerente de <span>dtconsultor</span></p>
                            <p class="url"><a>dtconsultor.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Ayuda-T nos está ayudando a sacar adelante nuestro proyecto, con sus consejos y su trabajo efectivo."</p>
                            <p class="autor">Javier Calleja</p>
                            <p class="url"><a>flunorte.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Siempre están cuando los necesito. Además de mucha profesionalidad y buen servicio. Y todo esto a un precio increíble."</p>
                            <p class="autor">Inmaculada Ruiz</p>
                            <p class="empresa">Gerente R.D. de <span>Nutricion Jerez S.L.</span></p>
                            <p class="url"><a>Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Mi asesor es el mejor!"</p>
                            <p class="autor">Javier Infantes Martín </p>
                            <p class="url"><a>Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Muy buena. Siempre me ha atendido que prontitud y dado soluciones."</p>
                            <p class="autor">Luis Alberto Encina Rojas </p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"No tenemos hasta el momento sugerencias de mejora. Nuestra súper asesora Laura cumple perfectamente con nuestras expectativas."</p>
                            <p class="autor">Juri-Dileyc SL</p>
                            <p class="url"><a>juri-dileyc.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su profesionalidad, trato personalizado y empatía con el cliente."</p>
                            <p class="autor">Gerente PresumeBox Ecommerce Report S.L.</p>
                            <p class="url"><a>presumedebebe.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"me gusta por la atención y por el precio final, y se lo recomendaría por lo mismo."</p>
                            <p class="autor">Gerente Laredo Express S.L.</p>
                            <p class="url"><a>laredoexpress.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por la rapidez en la respuesta y la cercanía en el trato a pesar de la distancia.”</p>
                            <p class="autor">Javier</p>
                            <p class="empresa">Administrador de <span>inbade.es</span></p>
                            <p class="url"><a>inbade.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me simplifican mucho las cosas. Estoy empezando y se agradece."</p>
                            <p class="autor">Angel Canas</p>
                            <p class="url"><a>Field Engineer Autónomo</a></p>
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-caret-left flecha-opiniones-izquierda"></i>
  </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-caret-right flecha-opiniones-derecha"></i>
  </a>
                </div>
            </div>
        </div>
        <!--        ---------------------------------- FIN OPINIONES --------------------------------------->
        <!--        ---------------------------------- BANNER --------------------------------------->

        <div class="row publi">


            <div class="row row-title-publi">

                <h3 class="green title-publi">PROGRAMA DE FACTURACIÓN <br class="visible-xs">Y CONTABILIDAD GRATIS</h3>
                <h2 class=" subtitle-publi">SEAS CLIENTE O NO,<br class="visible-xs"> PARA SIEMPRE</h2>
                <a class="link-img-banner" href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">
                    <div class="boton-selfconta">
                        <span class="btn-green" target="_blank">Regístrate y úsalo GRATIS</span>
                    </div>
                    <img class="img-publi" src="../img/selfconta/aplicacion-gratis.jpg" alt="Aplicación gratis"></a>
                <a class="link-img-banner" href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><img class="img-publi-mobile" src="../img/selfconta/consigue-aplicacion-gratis.jpg" alt="Consigue aplicación gratis"></a>

            </div>
            <div class="row row-title-publi justify-content-sm-center">
                <div class="col-sm-12 col-md-8 col-lg-8 panel">
                    <a class="desplegable-publi" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        Sí, sí. Has leído bien. <br class="visible-xs">Para Pymes y autónomos como tú. <i class="fa fa-caret-right"></i>

        </a>


                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 panel-body">
                                Selfconta te permite <span>expedir facturas y presupuestos personalizados,</span> con tu logo y elementos corporativos, con un formato adecuado a las premisas de la AEAT. Puedes programar y <span>automatizar el envío de facturas recurrentes</span> y guardar un directorio con tus clientes y proveedores, para completar los datos de las facturas sin que tengas que hacer nada.
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 panel-body">
                                Además de ayudarte a tener una visión clara de los ingresos, gastos y movimientos en las cuentas de tu negocio, Selfconta tiene un <span>sistema de control de morosidad</span> y te permite enviar <span>remesas a tus clientes</span> para cobrarlas directamente. Hay un montón de funciones más que tendrás que descubrir tú solito. ¿Útil, no? <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" class="green" target="_blank">¿Quieres saber más? Infórmate aquí</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <script>
            $('.link-img-banner').mouseover(function() {
                $('.btn-green').addClass('btn-green-hover').fadeIn("slow");
            })
            $('.link-img-banner').mouseout(function() {
                $('.btn-green').removeClass('btn-green-hover').fadeIn("slow");
            })

        </script>
        <!--        ---------------------------------- FIN BANNER --------------------------------------->
        <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>

    <div class="row slide-ciudades justify-content-sm-center">
        <h2>GESTORÍA AUTÓNOMOS Y PYMES, SERVICIO PRESENCIAL Y ONLINE EN TODA ESPAÑA</h2>
    </div>

    <div class="row ciudades justify-content-sm-center">
        <div class="col-xl-10 col-lg-10 col-sm-12 col-md-12">
            <p class="tit-ciudades"><span>Gestoría con todo incluido:</span> laboral, fiscal y contable en toda las localidades españolas, ciudades y pueblos. También tienes el <span>servicio jurídico especializado en negocios a tu disposición. ;)</span>
            </p>
            <div class="panel-why">
                <a class="desplegable-ciudades" data-toggle="collapse" data-parent="#accordion" href="#collapseciudades">Trabaja con nosotros, estés donde estés, sin moverte de casa:<br><i class="fa fa-caret-down"></i></a>


                <div id="collapseciudades" class="panel-collapse collapse in">
                    <div class="row justify-content-sm-center">

                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Acoruna">A Coruña</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Alava">Álava</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Albacete">Albacete</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Alicante">Alicante</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Almeria">Almería</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Asturias">Asturias</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Avila">Ávila</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Badajoz">Badajoz</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Barcelona">Barcelona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Burgos">Burgos</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Caceres">Cáceres</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cadiz">Cádiz</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cantabria">Cantabria</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Castellon">Castellón</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ceuta">Ceuta</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ciudadreal">Ciudad Real</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cordoba">Córdoba</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cuenca">Cuenca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Girona">Girona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Granada">Granada</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Guadalajara">Guadalajara</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Guipuzcoa">Guipúzcoa</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Huelva">Huelva</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Huesca">Huesca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Mallorca">Islas Baleares</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Jaen">Jaén</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Larioja">La Rioja</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Laspalmas">Las Palmas</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Leon">León</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Lleida">Lleida</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Lugo">Lugo</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Madrid">Madrid</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Malaga">Málaga</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Melilla">Melilla</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Murcia">Murcia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Navarra">Navarra</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ourense">Ourense</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Palencia">Palencia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Pontevedra">Pontevedra</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Salamanca">Salamanca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Tenerife">Tenerife</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Sevilla">Sevilla</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Soria">Soria</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Tarragona">Tarragona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Teruel">Teruel</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Toledo">Toledo</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Valencia">Valencia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Valladolid">Valladolid</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Vizcaya">Vizcaya</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Zamora">Zamora</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Zaragoza">Zaragoza</a>
                        </div>

                    </div>
                </div>
            <p class="tit-ciudades2">Si estás en una localidad pequeña, no te preocupes porque nuestro servicio gestoría autónomos y empresas llega también a todos los pueblos y pedanías.
            </p>
        </div>


    </div>
    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->


    <script>
        $(document).ready(function() {

            $(window).bind('scroll', function() {
                if ($(window).scrollTop() + 1000 >= $('.modulo-empresas').offset().top + $('.modulo-empresas').outerHeight() - window.innerHeight) {
                    var tl4 = new TimelineLite({});

                    tl4.to(".gestor1", 1, {
                        opacity: 1,
                        x: 0
                    });
                    tl4.to(".gestor2", 1, {
                        opacity: 1,
                        x: 0
                    }, "-=.5");
                }

                if ($(window).scrollTop() + 500 >= $('.item-asesorias').offset().top + $('.item-asesorias').outerHeight() - window.innerHeight) {
                    var tl4 = new TimelineLite({});

                    tl4.to(".asesorias .precio", 1, {
                        scale: 1,
                        ease: Back.easeOut
                    });
                }
            });

        });

    </script>

    <div class="container">
        <div class="row empresas justify-content-md-center">
            <a href="https://www.ayudatpymes.com">
                <div class="item-empresas item-empresas1"></div>
            </a>
            <a href="https://www.ayudatlegal.com">
                <div class="item-empresas item-empresas2"></div>
            </a>
            <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">
                <div class="item-empresas item-empresas3"></div>
            </a>
            <a href="http://ayudat.es/learning" target="_blank">
                <div class="item-empresas item-empresas4"></div>
            </a>
            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">
                <div class="item-empresas item-empresas5"></div>
            </a>
            <a href="http://liquidoo.es/" target="_blank">
                <div class="item-empresas item-empresas6"></div>
            </a>
            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">
                <div class="item-empresas item-empresas7"></div>
            </a>
            <a href="http://ayudat.es/alma" target="_blank">
                <div class="item-empresas item-empresas8"></div>
            </a>

        </div>

        <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

        <!--        ---------------------------------- FOOTER --------------------------------------->

        <footer>
            <div class="logo-footer"><img src="../img/logo-footer.jpg" alt="AyudaT Pymes" /></div>
            <div class="row footer">

                <div class="col-sm-12 col-lg-7">
                    <div class="col-sm-4 col-lg-4 web-map">
                        <a href="/">Inicio</a>
                        <a href="../servicios">Servicios</a>
                        <a href="../asesoria">Autónomo y empresas</a>
                        <a href="../alta-autonomos">Alta autónomo</a>
                        <a href="../crear-empresa">Crear empresa</a>
                        <a href="../abrir-sucursal">Abrir sucursal</a>
                        <a href="../emprendedores">Emprendedores</a>
                        <a href="../empresas-sociales">Empresas sociales</a>
                        <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
                    </div>
                    <div class="col-sm-4 col-lg-4 web-map">
                        <a href="../juridica">Asesoría jurídica</a>
                        <a href="../patentes-y-marcas">Registro de marcas</a>
                        <a href="../servicio-rgpd">RGPD</a>
                        <a href="../curso-online">Cursos de formación</a>
                        <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
                        <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
                        <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
                        <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
                        <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>

                    </div>
                    <div class="col-sm-3 col-lg-3 contact">
                        <a href="../asesoria-online-ayuda-t-pymes">Conócenos</a>
                        <a href="../contacto-asesoria-ayuda-t-pymes">Contacto</a>
                        <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
                        <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">

                    <div class="col-sm-12 col-lg-12 contact-img">
                        <p>Conectar</p>
                        <div class="social">
                            <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="../img/fb_icon.png" alt="facebook" /></a>
                            <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="../img/tw_icon.png" alt="tweeter"/></a>
                            <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="../img/ins_icon.png" alt="instagram" /></a>
                            <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="../img/lin_icon.png" alt="linkedin"/></a>
                        </div>
                        <p>¿Alguna duda laboral, fiscal o contable?</p>
                        <a href="http://www.ayudapedia.es/" target="_blank"><img src="../img/ayudapedia.jpg" alt="Ayudapedia" /></a>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-2 footer-phone">
                    <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
                    <p class="footer-phone-txt">Infórmate sin compromiso</p>

                    <p class="footer-phone-txt2">Atención al cliente</p>
                    <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
                    <p class="footer-phone-txt3">clientes@ayudatpymes.com</p>

                </div>
            </div>
            <div class="row legal justify-content-md-center">
                <img src="../img/logo-footer-legal.jpg" alt="AyudaT" />
                <p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="../terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="../politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="../acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>
                <p class="condiciones-legal">*Los precios que aparecen en la web no incluyen IVA, para saber qué incluye el servicio desde 9,99€, el presentador de impuestos, consulte condiciones con el comercial.</p>
            </div>
        </footer>
        <!--         --------------------------------------------------------------------------------------------------------------------------------------TEST-------------------------------------------------------------------------------------------------------------------------------------------->

        <span class="ir-arriba icon-arrow-up2"></span>

        <?php
     if(isset($_GET['gracias'])){   
?>
            <!-- Google Code for Formulario OK Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1038674096;
                var google_conversion_language = "es";
                var google_conversion_format = "1";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "74JwCNaUrQQQsNGj7wM";
                var google_conversion_value = 1.00;
                var google_conversion_currency = "EUR";
                var google_remarketing_only = false;
                /* ]]> */

            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">


            </script>
            <noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1038674096/?value=1.00&amp;currency_code=EUR&amp;label=74JwCNaUrQQQsNGj7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

    </div>

    <style>
        .tabs-section2 .col {
            opacity: 1;
        }

    </style>

    <?php } ?>
    <!-- Google Code para etiquetas de remarketing -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1038674096;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */

    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">


    </script>
    <noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
<script src="../js/script.js"></script>
<?php 
    if ($_SERVER['REQUEST_URI'] === '/' || $_SERVER['REQUEST_URI'] === '/?gracias') { 
    ?>

<?php } ?>

</html>
