<?php
include 'required/header.php';
include 'curso-online';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="cursos-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                   LA FORMACIÓN DE TU EMPRESA<br>Y TRABAJADORES DESDE 0€
                </h1>
                
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-1 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-purple.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDER ---------------------------------------> 
        
        <div class="row slider-learning justify-content-sm-center">
            <h2>CURSOS ONLINE DE FORMACIÓN BONIFICADA PARA EMPRESAS</h2>
            <img src="img/logo-learning.png" alt="AyudaT Learning" />
        
        </div>
        
        
        <!--        ---------------------------------- FIN SLIDER --------------------------------------->

        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesoria justify-content-sm-center">
                    <div class="col-lg-5 col-md-5 img-cursos">
                        <img src="img/curso-online.jpg" alt="Curso online" />
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <p>Si tienes un negocio y estás buscando cursos de formación para empresas, has llegado al lugar indicado. En Ayuda-T Learning encontrarás todo tipo de cursos de capacitación para profesionales.</p>
                        <p><span>+ de 4.000 cursos online bonificados para empresas y sus profesionales, con posibilidad de coste cero.</span></p>
                        <p>Nuestra oferta abarca cursos de formación bonificada de obligado cumplimiento, como la <span>prevención de riesgos laborales</span>, de especialización en el manejo de software de gestión, facturación y contabilidad para negocios (Selfconta) y muchos más dentro de una amplia selección.</p>
                        <p>Además, en <span>Ayuda T Learning</span> tienes a disposición de tu negocio y de los profesionales de tu empresa, un catálogo con <span>más de 4.000 cursos formativos específicos</span>, con conocimientos aplicados a sectores y procesos concretos. Cursos online que podrán realizar desde cualquier lugar, cómodamente, con el seguimiento de formadores especializados.</p>
                        <p class="boton-asesorias"><a class="ir-formulario">¡Quiero + info!</a></p>
                    </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img src="img/formacion-bonificada-empresas.jpg" class="gestor1" alt="Formación Bonificada Empresas" />
                </div>
                <div class="col-lg-4">
                    <h2 class="tit-gestores">FORMACIÓN BONIFICADA PARA EMPRESAS</h2>
                    <p class="text-gestores1">Los cursos de formación para empresas Ayuda T Learning están certificados por FUNDAE (Fundación Tripartita), la Fundación Estatal para la Formación en el Empleo, que garantiza la calidad de nuestra formación bonificada como vía eficiente para contribuir en la implementación de competencias profesionales de empresarios y trabajadores.</p>
                    <p class="text-gestores1">Proporciona a tus empleados formación especializada con su actividad laboral con la realización de cursos online con posibilidad de coste cero, aptos en la validación de créditos obligatorios en la celebración de cualquier contrato para la formación.</p>
                </div>
            </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img src="img/formacion-bonificada.jpg" class="gestor2" alt="Formación Bonificada" />
                    </div>
                    <div class="col-lg-4 float-right">
                        <h2 class="tit-gestores2">CERTIFICADOS DE PROFESIONALIDAD A TRAVÉS DE CURSOS DE FORMACIÓN BONIFICADA PRÓXIMAMENTE</h2>
                        <p class="text-gestores1">Ahora entre nuestros cursos online encontrarás cursos de formación en materias como la gestión contable, fiscal y laboral de negocios; gestión de aplicaciones informáticas documentales, gráficos, tratamiento de bases de datos, atención al cliente, gestión comercial; orientados a actividades agropecuarias; control fitosanitario; horticultura y floricultura; gestión logística; aptitudes fábricas y de albañilería; electricidad y electrónica; maquinaria industrial; cocina y gastronomía, hostelería y cáterin; entre un sinfín de opciones.</p>
                    </div>
                </div>
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp3">
                    <img src="img/formacion-bonificada-para-empresas.jpg" class="gestor3" alt="Formación Bonificada para Empresas" />
                </div>
                <div class="col-lg-4">
                    <h2 class="tit-gestores">FORMACIÓN BONIFICADA PARA EMPRESAS</h2>
                    <p class="text-gestores1">En Ayuda T Learning podrás acceder a cursos bonificados de formación para empresas en, prácticamente, cualquier ámbito que te interese. Ampliando tu formación o la capacitación de tus trabajadores con seguimiento continuo de los mejores profesionales docentes, sin moverte de casa o la oficina.</p>
                    <p class="text-gestores1">Ayuda T Learning apuesta por la mejora formativa de equipos como vía para desarrollar el talento humano y el alcance y proyección de cualquier negocio, sin ningún coste y a través de la realización de cursos online de formación bonificada para empresas. No dejes pasar tu oportunidad e infórmate sin ningún tipo de compromiso.</p>
                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

           

        </div>




        
      
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php

include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->

<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
            if ($(window).scrollTop() >= $('.item-emp1').offset().top + $('.item-emp1').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
    tl4.to(".gestor3", 1,{opacity:1,x:0},"-=.5");
        }
    });
    
});
</script>


    <?php
include 'required/footer.php';
?>