<!DOCTYPE html>
<html lang="en">

<head>
    <title>PRUEBA EVALUACIÓN FINAL</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font.css">
    <!--===============================================================================================-->
 <!--===============================================================================================-->
     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
    <!--===============================================================================================-->
 <!--===============================================================================================-->
     <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.js"></script>
    <!--===============================================================================================-->
<!--===============================================================================================-->
     
    <!--===============================================================================================-->

</head>

<body>
    <div class="container">
        
        <div class="row cabecera">
            <img src="images/cabecera.jpg" />
            
        </div>
        <div class="row formulario" id="formulario">
        <form action="mail/mail.php" class="form-inline" method="post" id="examen">
            <div class="row datos">
                <h1>NUEVO REGLAMENTO GENERAL DE PROTECCION DE DATOS A NIVEL EUROPEO</h1>
            <h2>DATOS DEL PARTICIPANTE</h2>    
            <div class="form-group">
                <label for="nombre">D./Dña. </label>
                <input type="text" class="form-control" id="nombre" name="nombre" required>
            </div>
            <div class="form-group">
                <label for="nif">con NIF</label>
                <input type="text" class="form-control" id="nif" name="nif" required>
            </div>
            <div class="form-group">
                <label for="empresa">Que presta los servicios en la empresa </label>
                <input type="text" class="form-control" id="empresa" name="empresa" required>
            </div>
            <div class="form-group">
                <label for="cif">con CIF</label>
                <input type="text" class="form-control" id="cif" name="cif" required>
            </div>
            <div class="form-group">
                <label for="telefono">Teléfono de contacto</label>
                <input type="text" class="form-control" id="telefono" pattern="\d+" name="telefono" required>
            </div><br>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            </div>
            <div class="row">
                <h2>PRUEBA DE EVALUACION GLOBAL</h2></div>
            <fieldset class="form-group row examen">
                
                <!-- ================================================ PREGUNTA ================================================================= -->
                
                <legend class="col-form-legend"><span>01.</span> El Reglamento General de Protección de Datos (RGPD) ha entrado en vigor el 25 de mayo de 2016 y será de obligado cumplimiento a partir del 25 de mayo de 2018.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz1" id="quiz1_1" value="A">
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz1" id="quiz1_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>02.</span> El Reglamento afectará a los responsables que traten datos de empresas u organismos dentro de la Unión Europea, siempre que el tratamiento tenga lugar en la Unión.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz2" id="quiz2_1" value="A">
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz2" id="quiz2_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>03.</span> El contrato definirá la posición del encargado del tratamiento siempre y cuando ese acto vincule jurídicamente al encargado.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz3" id="quiz3_1" value="A">
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz3" id="quiz3_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>04.</span> Una de las novedades que introduce el Reglamento es la figura del Delegado de Protección de Datos para sociedades que traten datos de caracter sensible y empresas mayores de 250 trabajadores.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz4" id="quiz4_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz4" id="quiz4_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>05.</span> El Reglamento considera que el límite de edad para el tratamiento de datos de menores ha de situarse en los 16 años.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz5" id="quiz5_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz5" id="quiz5_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>06.</span> Es necesario el consentimiento expreso con el nuevo Reglamento.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz6" id="quiz6_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz6" id="quiz6_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>07.</span> La adaptación de las medidas necesarias para cumplir con los nuevos requisitos de este reglamento serán gestionadas por los gerentes de cada empresa u organismo que sea afectado.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz7" id="quiz7_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz7" id="quiz7_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>08.</span> Con el principio de transparencia, los perfiles de privacidad de las redes sociales estarán pordefecto abiertos, debiendo ser el usuario quien los cierre.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz8" id="quiz8_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz8" id="quiz8_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>09.</span> El nuevo Reglamento obliga a que se elabore un contrato entre el encargado y el responsable del tratamiento de datos.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz9" id="quiz9_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz9" id="quiz9_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>10.</span> Con el nuevo Reglamento no será aplicable la privacidad desde el origen.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz10" id="quiz10_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz10" id="quiz10_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>11.</span> El Delegado de Protección de Datos debe tener unos conocimientos juridicos minimos.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz11" id="quiz11_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz11" id="quiz11_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>12.</span> El nuevo Reglamento no sólo es de aplicación para todas las empresas europeas, sino también para aquellas empresas internacionales que gestionen datos de usuarios residentes en la Unión Europea.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz12" id="quiz12_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz12" id="quiz12_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>13.</span> En el derecho de transparencia informa que el ejercicio de los derechos deberá ser gratuito para los interesados, pero se establecen los supuestos en los que el responsable puede cobrar una tasa o canon razonable o incluso negarse a responder.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz13" id="quiz13_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz13" id="quiz13_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>14.</span> Se entiende por “Derecho al olvido” como la manifestación del derecho de supresión de datos personales en organismos públicos como hospitales, centros de enseñanza o seguridad social.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz14" id="quiz14_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz14" id="quiz14_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>15.</span> Con el nuevo Reglamento las transferencias se pueden llevar a cabo autorización previa.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz15" id="quiz15_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz15" id="quiz15_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>16.</span> En empresas u organizaciones que no traten datos de caracter sensible no será necesario realizar análisis de riesgo.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz16" id="quiz16_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz16" id="quiz16_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>17.</span> Cuando se viole la seguridad, el responsable debe notificarlo a la autoridad de control en un plazo máximo de 72 horas.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz17" id="quiz17_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz17" id="quiz17_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>18.</span> En el artículo 83 RGPD se prevé la posibilidad de sancionar las infracciones cometidas con multas administrativas de 10 y 20 millones euros o el 2 y 4 porciento de la facturación anual.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz18" id="quiz18_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz18" id="quiz18_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>
                
                <!-- ================================================ PREGUNTA ================================================================= -->

                <legend class="col-form-legend"><span>19.</span> Se trata de una norma muy extensa que agrupa en once capítulos 173 considerandos previos y 99 artículos.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz19" id="quiz19_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz19" id="quiz19_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                <!-- ================================================ PREGUNTA ================================================================= -->
                <legend class="col-form-legend"><span>20.</span> Con el principio de transparencia lo que se pretende es que los avisos legales y políticas de privacidad sean fácilmente comprensibles.</legend>

                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz20" id="quiz20_1" value="A"  >
                        <span>A)</span>&nbsp; Verdadero.
          </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
            <input class="form-check-input" type="radio" name="quiz20" id="quiz20_2" value="B">
                        <span>B)</span>&nbsp; Falso.
          </label>
                </div>

                
            </fieldset>

            <div class="form-group btn-submit">
            <button id="submit" type="submit" class="btn btn-primary">Enviar para corrección</button>
            </div>
            
        </form>
            </div>
         <footer>
        <div class="row">
        <img src="images/footer.jpg" />     
        
        </div>
        
        </footer>       
    </div>
        <style>
            body{
                font-family: 'DIN Pro';
                color: #616161;
            }
            
            .col-form-legend{
                margin-top:20px;
            }
            
            .col-form-legend span{
                font-family: 'DIN Pro Black';
                color: #ff000e;
            }
            .form-check-label span{
                font-family: 'DIN Pro Bold';
            }
            .btn-submit{
                width: 100%;
            }
            
            .btn-submit .btn{
                    border-color: #ff000e;
    background-color: #ff000e;
    font-size: 20px;
        margin-top: 70px;
                cursor: pointer;
            }
            .btn-submit .btn:hover{
                color: #ff000e;
                background-color: #fff;
            }
            
            .cabecera img{
                width: 100%;
    height: 100%;
            }
            
            .row.formulario {
    width: 90%;
    margin: 60px auto 0 auto;
}
            .formulario h1{
                    font-family: 'DIN Pro Bold';
    color: #ff000e;
    font-size: 40px;
            }
            
            .formulario h2{
                    font-family: 'DIN Pro Bold';
    color: #ff000e;
    font-size: 20px;
                margin-top: 20px;
                width: 100%;
            }
            
            .row{margin: 0}
            fieldset {
                width: 100%;

            }

            .form-inline .form-check {
                justify-content: flex-start !important;
            }

            input#nombre {
                width: 60%;
            }

            .form-control {
                margin-right: 30px;
            }

            label {
                margin-right: 10px;
            }

            input#empresa {
                width: 480px;
            }

            body>div>form>div:nth-child(7) {
                width: 100%;
            }

            input#email {
                width: 730px;
            }
            
            .datos .form-group {
    margin: 5px 0;
}

            body > div > div.row.formulario > form > div.row.datos > div:nth-child(3){
                width: 100%;
            }
        </style>

    <?php 
    if(isset($_GET['gracias'])){  ?>
    
    <script>
    $(document).ready(function () {
        console.log('gracias');
$('#formulario').html('<h2>Gracias por enviar el examen</h2><p>En breve nos pondremos en contacto con usted</p>') 
    });
    </script>
    
    <?php } ?>
    
</body>

</html>
