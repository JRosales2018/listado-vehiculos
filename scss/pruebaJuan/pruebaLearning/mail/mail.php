<?php

if(!$_POST) exit;

// Email address verification, do not edit.

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

//DATOS
$nombre    	= $_POST['nombre'];
$nif   	    = $_POST['nif'];
$empresa 	= $_POST['empresa'];
$cif 	    = $_POST['cif'];
$telefono 	= $_POST['telefono'];
$email      = $_POST['email'];

//PREGUNTAS

$quiz1       = $_POST['quiz1'];
$quiz2       = $_POST['quiz2'];
$quiz3       = $_POST['quiz3'];
$quiz4       = $_POST['quiz4'];
$quiz5       = $_POST['quiz5'];
$quiz6       = $_POST['quiz6'];
$quiz7       = $_POST['quiz7'];
$quiz8       = $_POST['quiz8'];
$quiz9       = $_POST['quiz9'];
$quiz10       = $_POST['quiz10'];
$quiz11       = $_POST['quiz11'];
$quiz12       = $_POST['quiz12'];
$quiz13       = $_POST['quiz13'];
$quiz14       = $_POST['quiz14'];
$quiz15       = $_POST['quiz15'];
$quiz16       = $_POST['quiz16'];
$quiz17       = $_POST['quiz17'];
$quiz18       = $_POST['quiz18'];
$quiz19       = $_POST['quiz19'];
$quiz20       = $_POST['quiz20'];



// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";
$address = "carmenmoreno@ayudatlearning.com ";


// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

$e_subject = "PRUEBA EVALUACIÓN FINAL";


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "Nombre: $nombre" . PHP_EOL . "NIF: $nif" . PHP_EOL;
$e_content = "Nombre de la empresa: $empresa" . PHP_EOL . "CIF: $cif" . PHP_EOL;
$e_reply = "Email: $email / telefono: $telefono" . PHP_EOL . PHP_EOL;
$e_news = "PREGUNTAS:" . PHP_EOL . PHP_EOL . "Pregunta 1: $quiz1" . PHP_EOL . "Pregunta 2: $quiz2". PHP_EOL . "Pregunta 3: $quiz3" . PHP_EOL . "Pregunta 4: $quiz4" . PHP_EOL . "Pregunta 5: $quiz5" . PHP_EOL . PHP_EOL . "Pregunta 6: $quiz6" . PHP_EOL . "Pregunta 7: $quiz7" . PHP_EOL . "Pregunta 8: $quiz8" . PHP_EOL . "Pregunta 9: $quiz9" . PHP_EOL . "Pregunta 10: $quiz10" . PHP_EOL . PHP_EOL . "Pregunta 11: $quiz11" . PHP_EOL . "Pregunta 12: $quiz12" . PHP_EOL . "Pregunta 13: $quiz13" . PHP_EOL . "Pregunta 14: $quiz14" . PHP_EOL . "Pregunta 15: $quiz15" . PHP_EOL . PHP_EOL . "Pregunta 16: $quiz16" . PHP_EOL . "Pregunta 17: $quiz17" . PHP_EOL . "Pregunta 18: $quiz18" . PHP_EOL . "Pregunta 19: $quiz19" . PHP_EOL . "Pregunta 20: $quiz20";

$msg = wordwrap( $e_body . utf8_decode($e_content) . $e_reply . $e_news , 70 );

$headers  = "From: $email" . PHP_EOL;
$headers .= "Reply-To: $email" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;

if(isset($_GET['gracias'])){   
         header('Location:' . getenv('HTTP_REFERER'));
    } else if(mail($address, $e_subject, utf8_decode($msg), $headers)) {

	// Email has sent successfully, echo a success page.

    
//	echo "<fieldset>";
//	echo "<center><div id='success_page'>";
//	echo "<h1>¡Mensaje enviado con éxito!</h1>";
//	echo "<p>¡Gracias!, tu mensaje es importante para nosotros.</p>";
//	echo "</div></center>";
//	echo "</fieldset>";
    

    header('Location:' . getenv('HTTP_REFERER').'?gracias');
    
} 

?>