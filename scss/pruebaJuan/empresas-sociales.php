<?php
include 'required/header.php';
$what = 'empresas-sociales';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="empresas-sociales-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    GESTORÍA PARA FUNDACIONES <br>Y ORGANIZACIONES SIN LUCRO
                </h1>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">
                    <h2 class="radj-sb-23 tit-precio">Asesoría para empresas sociales incluido cooperativas</h2>
                    <div class="row">
                        <div class="precio">
                            <img src="img/precios/precio-empresa-social.png" alt="Precio crear empresa social" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-gestoria justify-content-sm-center">
            <h2><span>TU ASESORÍA GESTORÍA PARA EMPRESAS SOCIALES</span><br> MÁS DE <?php echo $clientes ?> CLIENTES DE ASESORÍA PYMES Y AUTÓNOMOS</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row gestoria justify-content-sm-center">

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-md-center">
                        <div class="col-sm-6 col-md-4 col-lg-5 img-section1">

                            <div class="row">
                                <h2>GESTORÍA <br>EMPRESAS</h2>
                                <div class="precio">
                                    <img src="img/precios/precio-empresa-social.png" alt="Precio crear empresa social" />
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-6">
                            <p>Toda la <span>gestión laboral, fiscal y contable;</span> un <span>asesor personal</span> que se encargará de cubrir todas tus obligaciones y <span>las mejores herramientas para que gestiones mejor tu negocio.</span></p>
                            <p class="boton-asesorias"><a class="ir-formulario">lo quiero!</a></p>
                        </div>

                    </div>
                </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img src="img/crear-ong.jpg" class="gestor1" alt="Crear ONG" />
                </div>
                <div class="col-lg-5">
                    <h2 class="tit-gestores">SERVICIO DE GESTORÍA PARA EMPRESAS SOCIALES Y ONG</h2>
                    <p class="text-gestores1"><span>¿A QUIEN VA DIRIGIDO?</span><br> Tanto si quieres <span>crear una asociación</span> u otra empresa social o ya la tienes, damos servicio especializados a <span>cualquier empresa social, ONG</span> y diferentes tipos de empresas sociales: Asociaciones, Fundaciones, Cooperativas, Sociedades Laborales, Empresas de inserción y Centros Especiales de Empleo. Ofrecemos un servicio de asesoría para cooperativas, ONG y empresas sociales personalizado y adaptado a tu situación. Pondremos a tu disposición personal de nuestro equipo formado expresamente para prestar este servicio único en España. </p>
                    <p class="text-gestores1"><span>¿POR QUÉ?</span><br> Si eres una empresa social, para ti las personas son lo primero. Te ofrecemos un servicio de gestoría especializada para que puedas ahorrar costes y dedicar más tiempo a las personas que realmente lo necesitan. </p>
                </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img src="img/asesoria-ong.jpg" class="gestor2" alt="Asesoría ONG" />
                    </div>
                    <div class="col-lg-5 float-right">
                        <h2 class="tit-gestores2">ACCESIBILIDAD A LA ASESORÍA PARA EMPRESAS</h2>
                        <p class="text-gestores1">Trabajamos muy duro día a día para poder darte <span>el más tecnológico y mejor servicio de asesoría para cooperativas, asesoría fundaciones y asociaciones</span> que existe en el mercado. Gracias a nuestras de herramientas online podrás disfrutar de todos nuestros servicios, estés donde estés y sin permanencia.
                        </p>
                    </div>
                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

            <!--        ---------------------------------- OPINIONES --------------------------------------->

        </div>
    </div>

    <div class="row slide-opiniones justify-content-sm-center">
        <h2>+ DE <?php echo $clientes ?> CLIENTES CONFIAN EN NUESTRA ASESORÍA ONLINE</h2>
    </div>


    <div class="container">
        <div class="row opiniones-autonomo justify-content-sm-center">
            <div class="col-sm-5 col-lg-4">
                <a href="https://www.ayudatpymes.com/opiniones/" target="_blank"><p class="title-opiniones1">Opiniones de Ayuda T clientes</p></a>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <p class="comentario">"Me gusta por la sencillez y rapidez de nuestro gestor y sobre todo la facilidad de tener todo disponible en cualquier momento”</p>
                            <p class="autor">Juan Villa Carrasco.</p>
                            <p class="empresa">Gerente de <span>Voip telecom S.L.</span></p>
                            <p class="url"><a >voiptelecom.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me gusta Ayuda-T-Pymes " por su rápida respuesta y acertada" la recomiendo por su precio."</p>
                            <p class="autor">Gerente de solupcdoctor</p>
                            <p class="url"><a >solupcdoctor.es</a></p>

                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su sencilla plataforma y su sencillez de uso, y por la excelente atención al cliente."</p>
                            <p class="autor">Daniel Tomas</p>
                            <p class="empresa">Gerente de <span>dtconsultor</span></p>
                            <p class="url"><a >dtconsultor.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Ayuda-T nos está ayudando a sacar adelante nuestro proyecto, con sus consejos y su trabajo efectivo."</p>
                            <p class="autor">Javier Calleja</p>
                            <p class="url"><a >flunorte.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Siempre están cuando los necesito. Además de mucha profesionalidad y buen servicio. Y todo esto a un precio increíble."</p>
                            <p class="autor">Inmaculada Ruiz</p>
                            <p class="empresa">Gerente R.D. de <span>Nutricion Jerez S.L.</span></p>
                            <p class="url"><a >Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Mi asesor es el mejor!"</p>
                            <p class="autor">Javier Infantes Martín </p>
                            <p class="url"><a >Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Muy buena. Siempre me ha atendido que prontitud y dado soluciones."</p>
                            <p class="autor">Luis Alberto Encina Rojas </p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"No tenemos hasta el momento sugerencias de mejora. Nuestra súper asesora Laura cumple perfectamente con nuestras expectativas."</p>
                            <p class="autor">Juri-Dileyc SL</p>
                            <p class="url"><a >juri-dileyc.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su profesionalidad, trato personalizado y empatía con el cliente."</p>
                            <p class="autor">Gerente PresumeBox Ecommerce Report S.L.</p>
                            <p class="url"><a >presumedebebe.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"me gusta por la atención y por el precio final, y se lo recomendaría por lo mismo."</p>
                            <p class="autor">Gerente Laredo Express S.L.</p>
                            <p class="url"><a >laredoexpress.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por la rapidez en la respuesta y la cercanía en el trato a pesar de la distancia.”</p>
                            <p class="autor">Javier</p>
                            <p class="empresa">Administrador de <span>inbade.es</span></p>
                            <p class="url"><a >inbade.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me simplifican mucho las cosas. Estoy empezando y se agradece."</p>
                            <p class="autor">Angel Canas</p>
                            <p class="url"><a >Field Engineer Autónomo</a></p>
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-caret-left flecha-opiniones-izquierda"></i>
  </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-caret-right flecha-opiniones-derecha"></i>
  </a>
                </div>
            </div>
        </div>
        <!--        ---------------------------------- FIN OPINIONES --------------------------------------->
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php
include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->


<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= $('.formulario').offset().top + $('.formulario').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
        }
        
             if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestoria .precio", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>

    <?php
include 'required/footer.php';
?>
