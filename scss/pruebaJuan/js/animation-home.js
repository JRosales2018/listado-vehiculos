$(document).ready(function () {

    var fecha = new Date();
    var day = fecha.getDate();
    var month = "" + (fecha.getMonth() + 1);
    var month = "" + (fecha.getMonth() + 1);
    var year = fecha.getFullYear();
    if (month.length <= 1) {
        month = "0" + month;
    }


    $('#fecha').html(day + " / " + month + " / " + year);
    $('#fecha2').html(day + " / " + month + " / " + year);


    var minNumber = 17;
    var maxNumber = 25;

    var randomNumber = randomNumberFromRange(minNumber, maxNumber);

    function randomNumberFromRange(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    console.log(randomNumber);


    var fechaRef = 25;
    var contador = 8547;



    if (fechaRef + 7 == day) {
        fechaRef = fechaRef + 7;
        if (fechaRef >= month) {
            var newFechaRef = fechaRef - month;
            fechaRef = 0 + newFechaRef;
        }


        count = count + randomNumber;
    }

    var cuadroRange = $('#cuadro-range'),
        cuadroRangeMobile = $('#cuadro-range-mobile'),
        numClientes = $('#numClientes'),
        numClientesMobile = $('#numClientes-mobile'),
        profesion = $('#profesion'),
        foto1 = $('#foto1'),
        foto2 = $('#foto2'),
        foto3 = $('#foto3'),
        foto4 = $('#foto4'),
        foto5 = $('#foto5'),
        foto6 = $('#foto6'),
        foto7 = $('#foto7'),
        foto8 = $('#foto8'),
        foto9 = $('#foto9'),
        foto10 = $('#foto10');
    var counter = {
        var: 0
    };
    var counter2 = {
        var: 0
    };
    var profesionesText = [{
        profesion: "Fotógrafo"
    }, {
        profesion: "Deportista"
    }, {
        profesion: "Psicólogo"
    }, {
        profesion: "Músico"
    }, {
        profesion: "Chef"
    }, {
        profesion: "Diseñador Gráfico"
    }, {
        profesion: "Escritor"
    }, {
        profesion: "Y muchos más..."
    }, {
        profesion: "Arquitecto"
    }];

    
    $(window).bind('scroll', function () {
    if ($(window).width() >= 637) {

        
            if ($(window).scrollTop() >= $('#clients').offset().top + $('#clients').outerHeight() - window.innerHeight) {


                var tl = new TimelineLite({});
                var tl2 = new TimelineLite({});
                var tl3 = new TimelineLite({});


                tl.to(cuadroRange, 7, {
                    x: -682,
                    delay: .5
                });


                tl2.from(foto2, .3, {
                    opacity: 0,
                    delay: .5
                });

                tl2.from(foto3, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto4, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto5, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto6, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto7, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto8, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto9, .3, {
                    opacity: 0,
                    delay: .3
                });

                tl2.from(foto10, .3, {
                    opacity: 0,
                    delay: .3
                });
                var foo = {};
                tl3.to(foo, 7, {
                    bezier: {
                        values: profesionesText,
                        curviness: 0,
                        timeResolution: 0
                    },
                    ease: Linear.easeNone,
                    onStart: update,
                    onUpdate: update,
                    delay: 0
                });

                function update() {
                    var textProf = foo.profesion;
                    textProf = textProf + "";
                    $('#profesion').html(textProf.replace('NaN', ''));
                }
                tl3.to(counter, 7, {
                    var: contador,
                    onUpdate: function () {
                        var number = Math.ceil(counter.var);
                        $('#numClientes').html(number);
                        $.each(profesionesText, function (i, val) {});
                    },
                    ease: Circ.easeOut
                }, "-=7");

                $(window).unbind("scroll");
            }
            // $(window).unbind("scroll");    

        

    } else if ($(window).width() <= 636) {

        
            if ($(window).scrollTop() >= $('#clientes-mobile').offset().top + $('#clientes-mobile').outerHeight() - window.innerHeight) {


                var tl = new TimelineLite({});

                var tl3 = new TimelineLite({});

                if ($(window).width() >= 500) {

                    tl.to(cuadroRangeMobile, 7, {
                        x: -411,
                        delay: .5
                    });

                } else if ($(window).width() <= 500 && $(window).width() >= 400) {
                    tl.to(cuadroRangeMobile, 7, {
                        x: -412,
                        delay: .5
                    });
                } else if ($(window).width() <= 400 && $(window).width() >= 330) {
                    tl.to(cuadroRangeMobile, 7, {
                        x: -237,
                        delay: .5
                    });
                } else if ($(window).width() <= 330) {
                    tl.to(cuadroRangeMobile, 7, {
                        x: -205,
                        delay: .5
                    });
                }


                tl3.to(counter2, 7, {
                    var: contador,
                    onUpdate: function () {
                        var number = Math.ceil(counter2.var);
                        $('#numClientes-mobile').html(number);
                    },
                    ease: Circ.easeOut
                });

                $(window).unbind("scroll");
            }
            // $(window).unbind("scroll");    

       

    }

        
});


    var tl4 = new TimelineLite({});
    
    tl4.to(".tabs-section2 .link1", 1,{opacity:1,y:10});
    tl4.to(".tabs-section2 .link2", 1,{opacity:1,y:10},'-=.4');
    tl4.to(".tabs-section2 .link3", 1,{opacity:1,y:10},'-=.4');
    tl4.to(".tabs-section2 .link4", 1,{opacity:1,y:10},'-=.4');
    tl4.to(".tabs-section2 .link5", 1,{opacity:1,y:10},'-=.4');
            
//    tl4.to(".tabs-section2 .link1", 1,{height:"90px"},'-=.4');
//    tl4.to(".tabs-section2 .link2", 1,{height:"90px"},'-=.4');
//    tl4.to(".tabs-section2 .link3", 1,{height:"90px"},'-=.4');
//    tl4.to(".tabs-section2 .link4", 1,{height:"90px"},'-=.4');
//    tl4.to(".tabs-section2 .link5", 1,{height:"90px"},'-=.4');
        
    
 
    
    
    
});


