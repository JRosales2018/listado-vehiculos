$(document).ready(function () {



//    $("#form-section1").submit(function (event) {
//        $('#popup').fadeIn('slow');
//        $('.popup-overlay').fadeIn('slow');
//        $('.popup-overlay').height($(window).height());
//        return false;
//        event.preventDefault();
//    });
//    $("#form-section2").submit(function (event) {
//        $('#popup').fadeIn('slow');
//        $('.popup-overlay').fadeIn('slow');
//        $('.popup-overlay').height($(window).height());
//        return false;
//        event.preventDefault();
//    });
//
//
    if($(window).width()>767){
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("navbar-collapse collapse show");
        console.log('click');
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $(".navbar-toggler").click();
        }
    });}
    
    $('#popup').click(function () {
        $('#popup').fadeOut('slow');
        $('.popup-overlay').fadeOut('slow');
        return false;
    });

    (function () {

        $(".text-collapse").on("show.bs.collapse hide.bs.collapse", function (e) {
            var padre = $(this).parent().children('a');
            if (e.type == 'show') {
                padre.addClass('active-link');
            } else {
                padre.removeClass('active-link');
            }
        });

    }).call(this);

    $('.customer-logos').slick({
        slidesToShow: 4,
        autoplay: false,
        autoplaySpeed: 3000,
        arrows: true,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
				}, {
            breakpoint: 634,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
				}]
    });    
    
    $('.customer-servicios').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 3000,
        arrows: true,
        dots: false,
        pauseOnHover: true,
        responsive: [
            
            {
            breakpoint: 1100,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
				},{
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
				}, {
            breakpoint: 634,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
				}]
    });

    (function () {

        $(".panel").on("show.bs.collapse hide.bs.collapse", function (e) {
            if (e.type == 'show') {
                $(this).addClass('active-publi');
            } else {
                $(this).removeClass('active-publi');
            }
        });

    }).call(this);

    (function () {

        $(".panel1").on("show.bs.collapse hide.bs.collapse", function (e) {
            if (e.type == 'show') {
                $(this).addClass('active-publi');
            } else {
                $(this).removeClass('active-publi');
            }
        });

    }).call(this);

    (function () {

        $(".panel-why").on("show.bs.collapse hide.bs.collapse", function (e) {
            if (e.type == 'show') {
                $(this).addClass('active-publi');
            } else {
                $(this).removeClass('active-publi');
            }
        });

    }).call(this);


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////CAMBIO CABECERA HOME ///////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    var tl5 = new TimelineLite({
      onComplete: function() {
                                this.restart();
                            }
 });
    
    
//    tl5.to(".arrow", .8,{x:5, ease: Power1.easeIn});
//    tl5.to(".arrow", .8,{x:0,ease: Power1.easeIn});
    
    
    
  
    
});

$(document).ready(function(){
 
	$('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});	
    $('.ir-formulario').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});	
    
    $('#linkarrow').click(function(){
		$('body, html').animate({
			scrollTop: $('#detalles').offset().top + $('#detalles').outerHeight() - window.innerHeight + 300
		}, 300);
	});
 
	$(window).scroll(function(){
		if( $(this).scrollTop() > 1 ){
            $('.menu').addClass('menu-scroll');
			$('.ir-arriba').slideDown(300);
		} else if( $(this).scrollTop() <= 1 ){
			$('.ir-arriba').slideUp(300);
            $('.menu').removeClass('menu-scroll');
		}
	});
 
    
    
    
    // ///////////////////////////////////////////////////// //
    // //////////////////// ANIMACIONES //////////////////// //
    // ///////////////////////////////////////////////////// //
    

    $('.navbar-toggler').click(function(){
        if($(window).width()<=767){
        if($('.navbar-collapse').hasClass('show')){
            $('body').css('overflow-y','visible');
             $('body').css('max-height','100%');
             $('.menu').css('position','fixed');
        
        }else{
             $('body').css('overflow-y','hidden');
             $('body').css('max-height','100vh');
             $('.menu').css('position','relative');
        
        }}
    });
    
    
    
    
    
    
    
    
    
    
    
    
});
