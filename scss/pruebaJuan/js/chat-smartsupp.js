var _smartsupp = _smartsupp || {};
  _smartsupp.key = 'fe7370d16c5da3e7462f800a2229247bfd145c36';
  
  window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);

//var chatHorarios = [false, '9,20', '9,20', '9,20', '9,20', '9,20', false];//0=>domingo
var chatHorarios = [false, '9,14', '9,14', '9,14', '9,14', '9,14', false];//0=>domingo
//var chatHorarios = [false, '9,15', '9,15', '9,15', '9,15', '9,15', false];//0=>domingo

var chatAgenteOnline = false;
var chatHorario = chatHorarios[chatDiaSemana];

smartsupp('on', 'agent.join', function(model, agent) {
	chatAgenteOnline = true;
  //console.log('agenteOnline: '+chatAgenteOnline);
});
  
smartsupp('on', 'agent.leave', function(model, agent) {
  chatAgenteOnline = false;
  //console.log('agenteOnline: '+chatAgenteOnline);
});

function at_chat()
{
  var muestra = false;
  
  //si hay agente online o no se atiende en el día, no quitar
  if (chatAgenteOnline) muestra = true;

  //si estamos en horario, no quitar
  if (chatHorario)
  {
    var horas = chatHorario.split(',');
    if (chatHora >= horas[0] && chatHora < horas[1]) muestra = true;
  }
  
  if (muestra)
    $('#chat-application-iframe').show();
  else
    $('#chat-application-iframe').hide();
}
$(document).ready(function() { at_chat(); });
setInterval("at_chat()", 2000);
