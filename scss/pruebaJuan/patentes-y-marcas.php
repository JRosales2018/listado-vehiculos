<?php
include 'required/header.php';
$what = 'patentes';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="patentes-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">      
REGISTRO DE MARCAS +<br> ANÁLISIS DE DISPONIBILIDAD GRATIS
                </h1>

            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <img src="img/precios/registrar-una-marca-precio.png" alt="Registrar una marca precio" />
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-deepblue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesoria justify-content-sm-center">
                <img class="R" src="img/registro-marcas.jpg" alt="Registro marcas" />
                <h2>REGISTRA TU MARCA EN CUALQUIER PAÍS AL MISMO PRECIO <br><span>DESCUBRE SI TUS MARCAS Y PATENTES ESTÁN DISPONIBLES</span></h2>

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-sm-center">

                        <div class="col-sm-6 col-md-5 col-lg-4">
                            <h2>REGISTRO DE MARCAS</h2>
                           <img src="img/precios/registrar-una-marca-precio.png" alt="Registrar una marca precio" />
                        </div>
                        <div class="col-sm-6 col-md-5 col-lg-6">
                            <p>Realizamos por ti el registro de marcas y su renovación. Nos encargamos de las actualizaciones legales y de la gestión de incidencias. Velamos para que nadie acceda en el registro a tu marca o a una similar. </p>
                            <p class="boton-asesorias"><a class="ir-formulario">lo quiero!</a></p>
                        </div>

                    </div>


                </div>
            </div>


        </div>





        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito <a href="tel:900100162">900 100 162</a>. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

        <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
        <div class="container">
            <div class="row modulo-empresas justify-content-sm-center">
                <div class="col-lg-6 pre-item-sucursal">
                    <h2>MARCAS Y PATENTES - REGISTRO DE MARCAS</h2>
                    <div class="row justify-content-sm-center">
                        <div class="col-lg-6 col-md-6 diagrama">
                            <img src="img/patentes-y-marcas.jpg" alt="Patentes y marcas"/>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <p><span>Registro de marcas</span> tanto descriptiva como gráficas a nivel nacional, europeo e internacional. No importa el lugar, la gestión de tu marca siempre te costará lo mismo.</p>
                            <p>No sólo nos encargamos de <span>registrar la marca,</span> también haremos un estudio previo para asegurarte que no existe ningún <span>registro de nombres comerciales</span> como el tuyo. El servicio de investigación es <span>100% GRATUITO.</span></p>
                            <p>Además del <span>registro de nombre</span> comercial te incluimos el <span>servicio de vigilancia</span> de la vigencia de <span>marca</span> a coste 0. La vigencia de la marca es de 10 años, pero no te preocupes, nosotros te avisaremos para su renovación.</p>
                            <h3>SERVICIOS QUE OFRECEMOS:</h3>
                            <li>REGISTRO DE MARCAS </li>
                            <li>REGISTRO DE PATENTES</li>
                            <li>REGISTRO DE DISEÑO INDUSTRIAL</li>
                            <li>REGISTRO DE MARCA NACIONAL</li>
                            <li>REGISTRO DE MARCA INTERNACIONAL</li>
                        </div>

                    </div>
                    <div class="row last-item-sucursal justify-content-sm-center">
                        <img class="etiqueta-derecha" src="img/etiqueta.png" alt="etiqueta" />
    <img class="etiqueta-izq" src="img/etiqueta.png" alt="etiqueta" />
                        <div class="col-lg-6 col-md-6">
                            <h2>¿PARA QUÉ SIRVE <br>REGISTRAR UNA MARCA?</h2>
                            <p>Registrar un nombre comercial te concede automáticamente la garantía de que nadie en el mundo podrá utilizar tu marca, sólo las personas que la hayan registrado o las que éstas mismas decidan que podrán explotarla.</p>
                            <p>El registro de marca te otorga la propiedad jurídica, quedando así amparado por los derechos de la Ley de Marcas y de Competencia Desleal, pudiendo llevar a cabo diferentes acciones civiles y penales contra quien sin consentimiento hayan utilizado o traten de registrar similares o idénticas denominaciones.</p>
                        </div>
                        
                        <div class="col-lg-6 col-md-6">
                            <h2>¿QUÉ PASA SI ALGUIEN USA <br>MI MARCA REGISTRADA?</h2>
                            <p>La persona o empresa será sancionada por la administración que contempla la Ley de Propiedad Industrial, ya que tu nombre comercial al estar registrado estará bajo la protección de la ley.</p>
                        </div>
                    </div>
                    </div>
                </div> 
        
        </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->
         <!--        ---------------------------------- SLIDER --------------------------------------->
        
        
        <div class="row slider-patentes justify-content-sm-center">
        <h2>REGISTRO DE PATENTES</h2>
        
        </div>
        
        
        
        
         <!--        ---------------------------------- FIN SLIDER --------------------------------------->

        <div class="container">

         <!--        ---------------------------------- REGISTRO --------------------------------------->
        <div class="row registro justify-content-sm-center">
            <div class="col-lg-8 item-registro">
            <div class="row justify-content-sm-center">
                 <div class="col-sm-6 col-md-3 col-lg-3 img-section1">

                    <div class="row">
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <span class="datos-tlf"><a href="tel:900100162">900 100 162</a></span>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-lg-9 col-md-7 text-registro">
                    <p>¿Tienes una idea de negocio innovadora? ¿Has inventado algún producto o servicio? No olvides registrar las marcas y patentes. 
                    </p>
                    <p>El registrar la patente te concederá el derecho exclusivo, por parte del Estado, a su plena explotación durante un periodo determinado. Este derecho puede ser para una o varias personas, nacionales o extranjeras, físicas o empresas. 
                    </p>
                    <p>¿Quieres saber más? Infórmate y pide presupuesto sin compromiso. 
                    </p>
                    <p>* Las tasas y el IVA no están incluidas en el precio.</p>
                    
                </div>
                
            </div>
            
            </div>
        </div>




 <!--        ---------------------------------- FIN REGISTRO --------------------------------------->

            <!--        ---------------------------------- BANNER --------------------------------------->

            <?php

include 'required/bannerSelfconta.php';
?>
                <!--        ---------------------------------- FIN BANNER --------------------------------------->
                <!--        ---------------------------------- CIUDADES --------------------------------------->
        </div>
        </div>


        <!--        ---------------------------------- FIN CIUDADES --------------------------------------->

<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {

    
        if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".contenido-gestoria img", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>


        <?php
include 'required/footer.php';
?>
