﻿<?php
include 'required/header.php';
$what = 'asesoria';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="autonomos-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    ASESORÍA AUTÓNOMOS Y GESTORÍA EMPRESAS
                </h1>
                <h2>
                    Servicio online y presencial
                </h2>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">
                    <h3 class="radj-sb-23 tit-precio">Todas tus obligaciones cubiertas</h3>
                    <div class="row">
                        <div class="precio">
                            <img src="img/precios/precio-gestoria-online.png" alt="Precio Gestoría Online" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Servicio de presentación de impuestos</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-autonomo justify-content-sm-center">
            <h2><span>GESTORÍA AUTÓNOMOS Y PYMES LÍDER</span><br> MILES DE CLIENTES YA TRABAJAN CON NOSOTROS, ¿TE UNES?</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesorias justify-content-sm-center">
                <h3>Ahorra tiempo encontrando todas las soluciones <br>para tu negocio en un mismo lugar</h3>

                <div class="col-lg-4 item-asesorias item-left-asesorias">
                    <h2>ASESORÍA<br><span>AUTÓNOMOS</span></h2>
                    <div class="precio">
                        <img src="img/precios/precio-asesoria-autonomo.png" alt="Precio asesoría autónomos">
                    </div>
                    <p>Desde presentar tus impuestos, hasta asesorarte y gestionar de forma integral <span>todas tus obligaciones como autónomo a nivel fiscal, laboral, contable y jurídico.</span> Depende de lo que estés buscando, nosotros nos adaptamos a ti y a tu negocio.
</p>

                    <p class="boton-asesorias"><a class="ir-formulario">¡Me interesa!</a></p>

                </div>
                <div class="col-lg-4 item-asesorias item-right-asesorias">
                    <h2>ASESORÍA<br><span>EMPRESAS</span></h2>
                    <div class="precio">
                        <img src="img/precios/precio-asesoria-empresas.png" alt="Precio asesoría empresas">
                    </div>
                    <p>Desde la presentación de impuestos trimestrales y anuales de tu negocio, hasta hacernos cargo de cada una de las gestiones de tu sociedad: <span>constitución, impuesto de sociedades, cuentas anuales,</span> laboral, fiscal, contable y jurídico.</p>

                    <p class="boton-asesorias"><a class="ir-formulario">¡Quiero + info!</a></p>
                </div>



            </div>



        
        <!--        ---------------------------------- FIN ASESORIAS --------------------------------------->
        
        <!--        ---------------------------------- INTERESAR --------------------------------------->
            <div class="row interesar justify-content-sm-center">
                <h3>
                    Pero, ¡ESPERA! También podemos ayudarte con:
                </h3>
                <div class="col-lg-3 col-md-6 col-sm-12"><p><a href="servicio-rgpd">CUMPLIR CON LA LEY<br>DE PROTECCIÓN DE DATOS</a></p></div>
                <div class="col-lg-3 col-md-6 col-sm-12"><p><a href="curso-online">FORMAR SIN COSTES<br>A TU EQUIPO</a></p></div>
                <div class="col-lg-3 col-md-6 col-sm-12"><p><a href="patentes-y-marcas">REGISTRAR<br>TU MARCA</a></p></div>
                <div class="col-lg-3 col-md-6 col-sm-12"><p><a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">USAR UN PROGRAMA<br>DE FACTURACIÓN GRATIS</a></p></div>
            </div>
        
        </div>
        <!--        ---------------------------------- FIN INTERESAR --------------------------------------->
        
        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <br class="visible-xs"><a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra empresa es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img class="gestor1" src="img/asesoria-empresas.jpg" alt="Asesoría empresas" />
                    <p class="gestor-name">Jorge | Director de Operaciones</p>
                    <p class="gestor-description">Le flipa el deporte y es súper competitivo, en el buen sentido ;)</p>
                </div>
                <div class="col-lg-5">
                    <h2 class="tit-gestores">ASESORÍA EMPRESAS ESPECIALIZADA<br>
LA GESTORÍA EMPRESAS MÁS INNOVADORA Y TECNOLÓGICA</h2>
                    <p class="text-gestores2">Ahorra molestias y complicaciones con el papeleo.</p>
                    <div class="panel1">
                        <a class="text-gestores1" data-toggle="collapse" data-parent="#accordion" href="#collapseAuto">Nosotros nos encargamos de todo mientras tú dedicas toda esa energía<br> a atender tu negocio. <i class="fa fa-caret-right"></i>

        </a>


                        <div id="collapseAuto" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Llevamos una década desarrollando herramientas tecnológicas y soluciones a medida a las necesidades administrativas de tu empresa. <span>Somos la asesoría online y gestoría presencial que revolucionó el sector introduciendo el salto digital.</span>
                            </div>
                            <div class="panel-body">
                                Desde enviar facturas a tu asesor para que las mecanice con una simple fotografía, a <span>realizar consultas 24/7</span> directamente desde un chat al que acceder desde tu propio dispositivo móvil.
                            </div>
                            <div class="panel-body">
                                <span>Todo está pensado para ahorrarte tiempo,</span> esfuerzo, desplazamientos y complicaciones. Somos gente práctica y el sistema es sencillo, ir un paso más allá: ponértelo fácil y adaptarnos a tus necesidades, para que puedas dedicarte a tu negocio mientras nosotros nos encargamos de todo el papeleo.

                            </div>
                        </div>

                        <p class="text-gestores3">Más de <?php echo $clientes ?> clientes ya confían en nosotros<br>¿Te unes al equipo y empiezas a optimizar tus procesos de gestión? </p>
                    </div>
                </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img class="gestor2" src="img/asesoria-autonomos.jpg" alt="Asesoría autónomos" />
                        <p class="gestor-name">Alex | Director Área Administrativa</p>
                        <p class="gestor-description">Le encanta viajar y plantarse en el quinto pino cada 2x3.</p>
                    </div>
                    <div class="col-lg-5 float-right">
                        <h2 class="tit-gestores">TRABAJA CON LA ASESORÍA AUTÓNOMOS <br class="visible-xs">LÍDER EN ESPAÑA<br> Más de <?php echo $clientes ?> clientes ya confían en nosotros,<br class="visible-xs"> ¿te unes?</h2>
                        <p class="text-gestores1">Si no necesitas el asesoramiento de un gestor, <span>no pagues de más.</span> ¿Sólo quieres que presentemos tus impuestos? Genial, tenemos justo lo que estás buscando desde 9,99€* mes. Tal vez prefieres <span>despreocuparte y que un experto se encargue de todo.</span> Sin problema, seas autónomo o empresa, puedes dejarlo todo en nuestras manos. Desde la presentación de impuestos (declaraciones de IVA, Retenciones IRPF, RENTA, liquidación del Impuesto de Sociedades), pasando por la gestión laboral y contable, hasta recibir asesoramiento legal especializado.
                        </p>
                    </div>
                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

            <!--        ---------------------------------- OPINIONES --------------------------------------->

        </div>
    </div>

    <div class="row slide-opiniones justify-content-sm-center">
        <h2>MILES DE CLIENTES CONFÍAN EN NOSOTROS</h2>
    </div>


    <div class="container">
        <div class="row opiniones-autonomo justify-content-sm-center">
            <div class="col-sm-5 col-lg-4">
                <a href="https://www.ayudatpymes.com/opiniones/" target="_blank"><p class="title-opiniones1">Opiniones de Ayuda T clientes</p></a>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <p class="comentario">"Me gusta por la sencillez y rapidez de nuestro gestor y sobre todo la facilidad de tener todo disponible en cualquier momento”</p>
                            <p class="autor">Juan Villa Carrasco.</p>
                            <p class="empresa">Gerente de <span>Voip telecom S.L.</span></p>
                            <p class="url"><a >voiptelecom.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me gusta Ayuda-T-Pymes " por su rápida respuesta y acertada" la recomiendo por su precio."</p>
                            <p class="autor">Gerente de solupcdoctor</p>
                            <p class="url"><a >solupcdoctor.es</a></p>

                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su sencilla plataforma y su sencillez de uso, y por la excelente atención al cliente."</p>
                            <p class="autor">Daniel Tomas</p>
                            <p class="empresa">Gerente de <span>dtconsultor</span></p>
                            <p class="url"><a >dtconsultor.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Ayuda-T nos está ayudando a sacar adelante nuestro proyecto, con sus consejos y su trabajo efectivo."</p>
                            <p class="autor">Javier Calleja</p>
                            <p class="url"><a >flunorte.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Siempre están cuando los necesito. Además de mucha profesionalidad y buen servicio. Y todo esto a un precio increíble."</p>
                            <p class="autor">Inmaculada Ruiz</p>
                            <p class="empresa">Gerente R.D. de <span>Nutricion Jerez S.L.</span></p>
                            <p class="url"><a >Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Mi asesor es el mejor!"</p>
                            <p class="autor">Javier Infantes Martín </p>
                            <p class="url"><a >Nutricion Jerez S.L.</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Muy buena. Siempre me ha atendido que prontitud y dado soluciones."</p>
                            <p class="autor">Luis Alberto Encina Rojas </p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"No tenemos hasta el momento sugerencias de mejora. Nuestra súper asesora Laura cumple perfectamente con nuestras expectativas."</p>
                            <p class="autor">Juri-Dileyc SL</p>
                            <p class="url"><a >juri-dileyc.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por su profesionalidad, trato personalizado y empatía con el cliente."</p>
                            <p class="autor">Gerente PresumeBox Ecommerce Report S.L.</p>
                            <p class="url"><a >presumedebebe.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"me gusta por la atención y por el precio final, y se lo recomendaría por lo mismo."</p>
                            <p class="autor">Gerente Laredo Express S.L.</p>
                            <p class="url"><a >laredoexpress.com</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Por la rapidez en la respuesta y la cercanía en el trato a pesar de la distancia.”</p>
                            <p class="autor">Javier</p>
                            <p class="empresa">Administrador de <span>inbade.es</span></p>
                            <p class="url"><a >inbade.es</a></p>
                        </div>
                        <div class="carousel-item">
                            <p class="comentario">"Me simplifican mucho las cosas. Estoy empezando y se agradece."</p>
                            <p class="autor">Angel Canas</p>
                            <p class="url"><a >Field Engineer Autónomo</a></p>
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fa fa-caret-left flecha-opiniones-izquierda"></i>
  </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fa fa-caret-right flecha-opiniones-derecha"></i>
  </a>
                </div>
            </div>
        </div>
        <!--        ---------------------------------- FIN OPINIONES --------------------------------------->
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php
include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>

    <div class="row slide-ciudades justify-content-sm-center">
        <h2>GESTORÍA AUTÓNOMOS Y PYMES, SERVICIO PRESENCIAL Y ONLINE EN TODA ESPAÑA</h2>
    </div>

    <div class="row ciudades justify-content-sm-center">
        <div class="col-xl-10 col-lg-10 col-sm-12 col-md-12">
            <p class="tit-ciudades"><span>Gestoría con todo incluido:</span> laboral, fiscal y contable en toda las localidades españolas, ciudades y pueblos. También tienes el <span>servicio jurídico especializado en negocios a tu disposición. ;)</span>
            </p>
            <div class="panel-why">
                <a class="desplegable-ciudades" data-toggle="collapse" data-parent="#accordion" href="#collapseciudades">Trabaja con nosotros, estés donde estés, sin moverte de casa:<br><i class="fa fa-caret-down"></i></a>


                <div id="collapseciudades" class="panel-collapse collapse in">
                    <div class="row justify-content-sm-center">

                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Acoruna">A Coruña</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Alava">Álava</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Albacete">Albacete</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Alicante">Alicante</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Almeria">Almería</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Asturias">Asturias</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Avila">Ávila</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Badajoz">Badajoz</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Barcelona">Barcelona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Burgos">Burgos</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Caceres">Cáceres</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cadiz">Cádiz</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cantabria">Cantabria</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Castellon">Castellón</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ceuta">Ceuta</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ciudadreal">Ciudad Real</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cordoba">Córdoba</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Cuenca">Cuenca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Girona">Girona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Granada">Granada</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Guadalajara">Guadalajara</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Guipuzcoa">Guipúzcoa</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Huelva">Huelva</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Huesca">Huesca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Mallorca">Islas Baleares</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Jaen">Jaén</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Larioja">La Rioja</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Laspalmas">Las Palmas</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Leon">León</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Lleida">Lleida</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Lugo">Lugo</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Madrid">Madrid</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Malaga">Málaga</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Melilla">Melilla</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Murcia">Murcia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Navarra">Navarra</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Ourense">Ourense</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Palencia">Palencia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Pontevedra">Pontevedra</a>
                        </div>
                        <div class="col-md-6 col-lg-3 subpanel-why">
                            <a href="https://www.ayudatpymes.com/asesorias/Salamanca">Salamanca</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Tenerife">Tenerife</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Sevilla">Sevilla</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Soria">Soria</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Tarragona">Tarragona</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Teruel">Teruel</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Toledo">Toledo</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Valencia">Valencia</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Valladolid">Valladolid</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Vizcaya">Vizcaya</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Zamora">Zamora</a>
                            <a href="https://www.ayudatpymes.com/asesorias/Zaragoza">Zaragoza</a>
                        </div>

                    </div>
                </div>
            <p class="tit-ciudades2">Si estás en una localidad pequeña, no te preocupes porque nuestro servicio gestoría autónomos y empresas llega también a todos los pueblos y pedanías.
            </p>
        </div>


    </div>
    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->


<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop()+1000 >= $('.modulo-empresas').offset().top + $('.modulo-empresas').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
        }
    
        if ($(window).scrollTop()+500 >= $('.item-asesorias').offset().top + $('.item-asesorias').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".asesorias .precio", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>

    <?php
include 'required/footer.php';
?>
