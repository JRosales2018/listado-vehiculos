<?php $clientes = "8547" ?>

<!DOCTYPE html>
<html lang="es">

<head>
    
    
    
    <link rel="icon" type="image/png" href="img/Pymes-ico.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--===================================================== CACHE NONE ====================================================-->
    <meta http-equiv="Expires" content="0">
 
<meta http-equiv="Last-Modified" content="0">
 
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
 
<meta http-equiv="Pragma" content="no-cache">
<!--===================================================== CACHE NONE ====================================================-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="/img/logo.png" />
    <title>POLÍTICA DE PRIVACIDAD</title>
    <meta property="og:title" content="POLÍTICA DE PRIVACIDAD" />
    <meta property="og:url" content="https://www.ayudatpymes.com/" />
    <meta property="og:description" content="Política de privacidad AyudaTPymes" />
    <meta name="description" content="Política de privacidad AyudaTPymes" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/build.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <script src="lib/jquery-latest.js"></script>
    <script src="lib/jquery.min.js"></script>
    <script src="lib/popper.min.js"></script>
    <script src="lib/bootstrap.min.js"></script>
    <script src="js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="lib/TweenMax.min.js"></script>
    <script src="lib/jquery.cookieBar.js"></script>
    <link rel="stylesheet" type="text/css" href="lib/cookieBar.css">
    <script type="text/javascript">
	$(document).ready(function() {
	  $('.cookie-message').cookieBar({ closeButton : '.my-close-button', hideOnClose: false });
	  $('.cookie-message').on('cookieBar-close', function() { $(this).slideUp(); });
	});
    </script>

    <?php
    require 'chat_smartsupp.php';
  ?>
    
    <!-- ======================================================================= -->
    <!-- ===============================PIXELES================================= -->
    <!-- ======================================================================= -->
    
    <!-- Facebook Pixel Code -->
    <?php
     if(isset($_GET['gracias'])){   
?>
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
    fbq('track', 'CompleteRegistration');
	</script>
    <noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
    
    <?php } else{?>
    
    <script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '660233234086506');
	fbq('track', 'PageView');
	</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
	/></noscript>
	<!-- End Facebook Pixel Code -->
<?php } ?>
	
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-8047470-2');
</script>



</head>

<body>
    <div class="cookie-message">
	<p>Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación y ofrecer contenidos y publicidad de interés. Al continuar con la navegación entendemos que aceptas nuestra <a href="/cookies">política de cookies.</a> <a class="my-close-button" href>Lo Entiendo</a></p>
</div>
    <div class="topbar"></div>
<span id="arriba"></span>

    <!-- -------------------------------------- MENU --------------------------------------->
    <div class="row menu">
        <nav class="navbar navbar-light bg-lignt">
            <a href="/" class="navbar-brand mar-top-10 logo-desktop"><img id=logo src="img/logo.png" alt="AyudaT Pymes" /></a>
            

            <a href="http://www.ayudat.es/" target="_blank" class="subicon"><img id="subicon" src="img/subicon.png" alt="AyudaT.es"/></a>

            <button class="navbar-toggler float-right mar-top-35 collapsed" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
          <span class="sr-only">Toggle navigation</span>
             
                
            </button>
            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            <span class="float-right radj-sb-22 grey mar-right-22 mar-top-17 phone"><a href="tel:900100162">T. 900 100 162</a></span>
            <div class="navbar-collapse collapse" id="collapsingNavbar2">
                <div class="row menu-desplegado">

                    <div class="col-sm-12 col-md-12 col-lg-9 justify-content-md-center menu-desplegado-center">
                        <div class="menu-escritorio">
                        <div class="col web-map">
                            <p>ASESORÍA</p>
                            <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                        </div>
                        <div class="col web-map">
                            <p>LEGAL</p>
                            <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                            
                            
                        </div>
                        <div class="col web-map">
                            <p>DESPACHOS</p>
                            <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                        </div>
                        <div class="col web-map">
                            <p>MÁS</p>
                            <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                        </div>
                        <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                        </div>
                        <div class="menu-mobile">
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu1" >ASESORÍA<i class="fa fa-caret-down"></i></a>
                            <div id="menu1" class="panel-collapse collapse in contenido-link show">
                        <a href="servicios">Servicios</a>
                            <a href="asesoria">Autónomo y empresas</a>
                            <a href="alta-autonomos">Alta autónomo</a>
                            <a href="emprendedores">Emprendedores</a>
                            <a href="empresas-sociales">Empresas sociales</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu2">LEGAL<i class="fa fa-caret-down"></i></a>
                            <div id="menu2" class="panel-collapse collapse in contenido-link">
                        <a href="juridica">Asesoría jurídica</a>
                            <a href="patentes-y-marcas">Registro de marcas</a>
                            <a href="servicio-rgpd">RGPD</a>
                            <a href="crear-empresa">Crear empresa</a>
                            <a href="abrir-sucursal">Abrir sucursal</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu3">DESPACHOS<i class="fa fa-caret-down"></i></a>
                            <div id="menu3" class="panel-collapse collapse in contenido-link">
                        <a href="https://www.ayudatpymes.com/para-despachos/"target="_blank">Para despachos</a>
                            <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/"target="_blank">Software asesoría</a>
                                </div>
                                </div>
                            <div class="col web-map">
                        <a class="link-menu-collapse" data-toggle="collapse" data-parent="#accordion" href="#menu4">MÁS<i class="fa fa-caret-down"></i></a>
                            <div id="menu4" class="panel-collapse collapse in contenido-link">
                        <a href="curso-online">Cursos de formación</a>
                            <a href="https://www.ayudatpymes.com/cambio-nombre-coche/"target="_blank">Transferencia de vehículos</a>
                            <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/"target="_blank">Programa facturación Gratis</a>
                            <a href="http://etlsport.es/"target="_blank">Asesoría para deportistas</a>
                            <a href="http://gestron.es/"target="_blank">Blog GesTron</a>
                                </div>
                                </div>
                            <div class="col contact">
                            <a href="/">Inicio</a>
                            <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                            <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                            <a href="http://www.ayudat.es/"target="_blank">Ayuda T</a>
                            <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
                        </div>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
<style>
#legal-page .container {
    margin-top: 150px;
}

    #legal-page{
        font-family: 'Rajdhani';
        font-weight: normal;
        font-size: 14px;
        color: #808183;
    }
    #legal-page h1{
        text-align: center;
        color:#00AEC8;
        margin-bottom: 30px;
    }
</style>
    <div id="legal-page">
        <div class="container">
            <h1>POLÍTICA DE PRIVACIDAD</h1>
            <div align="justify">
<p><strong>Declaración de Privacidad de Datos</strong></p>
<p>Nosotros, “<strong>Ayuda-T un Lugar todas las soluciones S.L.”</strong>, con CIF: B93011708, domicilio en Polígono industrial salinas de San José, Avenida Isaac Newton Edif. 287, CP. 11.500, El Puerto de Santa María (CÁDIZ) e inscrita en el Registro Mercantil de Málaga, tomo 4666, folio 17, inscripción 1, con hoja MA-102143, escrituras de constitución formalizada ante Notario, en cuya representación actúa <strong>D. Alfredo Pérez Guerrero</strong>, con D.N.I. 48901576-B, somos el operador del sitio web <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u> , así como el proveedor de servicios de la aplicación <strong>Selfconta</strong> para iOS y Android, incluidos los demás servicios que se ofrecen a través de la aplicación <b>Selfconta</b> y los sitios webs de <b>Ayuda-T Pymes</b>, en adelante <strong>Ayuda-T</strong>. Somos responsables de la recopilación, el procesamiento y el uso de los datos personales según la legislación de protección de datos, en concreto, el Reglamento General de Protección de Datos ("RGPD").</p>
<p>Tú, el cliente, eres el controlador de datos y <strong>Ayuda-T</strong>, el proveedor del servicio, es el procesador de datos en tu nombre. Solo usamos tus datos en cumplimiento de la legislación de protección de datos relevantes. <strong>Ayuda-T</strong> también tiene un Delegado de Protección de Datos designado (Data Protection Officer o "DPO", en sus siglas en inglés) que puede ser contactado por carta o por correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u>.</p>
<p>Con esta Declaración de Privacidad de Datos queremos informarte qué datos personales se recopilan y se guardan cuando visitas nuestro sitio web o utilizas nuestros servicios ofrecidos en el sitio web. Además, recibirás información sobre cómo usamos tus datos y qué derechos tienes con respecto al uso de ellos. Esta Declaración de Privacidad de Datos también se aplica para el acceso y el uso de la aplicación <strong>Selfconta</strong>, así como de los demás servicios disponibles.</p>
<p> </p>
<p><strong>1. Seguridad de datos</strong></p>
<p>Para protegerlos, todos los datos que nos proporcionas se cifran de acuerdo con el protocolo de seguridad TLS (Transport Layer Security). El TLS es un protocolo seguro y probado que se utiliza, por ejemplo, para la banca en línea. Puedes identificar la conexión TLS, por ejemplo, al observar la "s" después de la "http" en la URL que se muestra en tu navegador (https://..) o desde el icono de bloqueo que se muestra en la pestaña del navegador.</p>
<p>También tomamos medidas de seguridad técnica y organizativa adecuadas para proteger tus datos contra manipulaciones aleatorias o deliberadas, pérdidas parciales o totales, destrucción y/o accesos no autorizados. Para evitar la pérdida de datos, configuramos una base de datos duplicada, lo que significa que tus datos siempre se almacenan en dos ubicaciones separadas. Además, actualizamos y almacenamos los datos cada hora en una copia de seguridad fuera del sitio y, en línea con los análisis de alto riesgo, realizamos continuamente pruebas de seguridad en nuestra infraestructura. Tu contraseña se almacena a través de un proceso seguro encriptado. Nunca te pediremos la contraseña, ni por correo electrónico ni por teléfono. Si olvidas tu contraseña, podemos restablecerla por ti. Mejoramos nuestras medidas de seguridad continuamente en función del desarrollo tecnológico.</p>
<p>Los datos personales que recopilamos se almacenan en un entorno seguro dentro de la UE y se tratan de forma confidencial. El acceso a esta información está limitado a los empleados y proveedores seleccionados del grupo <strong>Ayuda-T</strong>. Cumplimos, en todo momento, con los requisitos legislativos de protección de datos.</p>
<p>Hacemos todo lo posible para proteger tus datos, pero no podemos garantizar la seguridad de tus datos cuando los transfieres a través de Internet. Cuando esto ocurre, existe un cierto riesgo de que otros puedan acceder a tus datos de forma ilícita. En otras palabras, al transferir datos por internet, asumes la responsabilidad de la seguridad de estos como controlador de datos.</p>
<p> </p>
<p><strong>2. Recolección y almacenamiento de datos personales y naturaleza y propósito de su uso</strong></p>
<p><strong>a) Si visitas nuestro sitio web</strong></p>
<p>Puedes visitar el sitio web <u><a href="https://www.ayudatpymes.com/">https://www.ayudatpymes.com/</a></u> sin revelar tu identidad. Tu navegador solo envía información - que se recopila automáticamente - a los servidores de nuestro sitio web. Esta información se almacena temporalmente en un archivo de registro (<em>logfile</em>). Esta es la información que se recopila y almacena automáticamente hasta su eliminación, también automática:</p>
<p>Dirección IP del ordenador solicitante.</p>
<p>Fecha y hora del acceso.</p>
<p>Nombre y URL de los datos introducidos.</p>
<p>Sitio web, de donde proviene el acceso (URL de referencia).</p>
<p>Navegador de uso y, si es necesario, el sistema operativo de tu ordenador, así como el nombre de tu proveedor de acceso.</p>
<p>Estos datos se recopilan y procesan para hacer posible el uso de nuestro sitio web (establecimiento de conexión), con el fin de garantizar la seguridad y la estabilidad de nuestro sistema, así como para la administración técnica de la infraestructura de la red. Dichos datos no nos ofrecen ninguna información que pueda identificarte personalmente.</p>
<p>Además, empleamos cookies, así como herramientas de análisis y de marketing web. Puedes encontrar más información sobre este tema en las secciones 3 a 5.</p>
<p><strong>b) Si te registras en nuestros servicios online</strong></p>
<p>En nuestro sitio web ofrecemos servicios online de asesoría contable, laboral y fiscal. Para usar estos servicios, primero debes registrarte. Cuando te registras, debes introducir una dirección de correo electrónico y una contraseña; de este modo, podrás crearte una cuenta con nosotros e iniciar sesión.</p>
<p>Para emplear nuestros servicios al completo, puede que debas introducir más datos personales.</p>
<p>También utilizamos tu nombre y tus datos de contacto con la siguiente finalidad:</p>
<p>Para saber quién es nuestra parte contratante.</p>
<p>Para la justificación, estructura, procesamiento y cambio de la relación contractual contigo sobre el uso de nuestros servicios.</p>
<p>Para verificar la plausibilidad de la información introducida.</p>
<p>Para contactar contigo, si es necesario.</p>
<p><strong>c) Si te registras en nuestra newsletter o boletín informativo</strong></p>
<p>Si has aceptado recibir nuestra newsletter o boletín informativo, podemos utilizar tu dirección de correo electrónico para enviarte periódicamente newsletters, así como información sobre nuestros servicios. Para recibir newsletters, primero debemos obtener tu consentimiento. Este consentimiento se puede hacer durante el registro. Puedes revocar tu consentimiento en cualquier momento, ya sea dentro de tu cuenta, rechazando los correos electrónicos o enviándonos un correo electrónico.</p>
<p>También puedes darte de baja de nuestra newsletter en cualquier momento, por ejemplo, al hacer clic en el enlace de cancelar suscripción que se encuentra en la parte inferior de la newsletter. Por otro lado, también puedes enviarnos un correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u></p>
<p>Si cancelas tu suscripción a la newsletter o boletín informativo, mantendremos tu dirección de correo electrónico registrada únicamente para garantizar que ya no recibirás estos correos electrónicos.</p>
<p><strong>d) Desarrollador, cliente, proveedor, asesor y equipo</strong></p>
<p>Con nuestros servicios, puedes introducir datos de terceros, permitir el acceso de terceros a tu cuenta, conectar tu cuenta con terceros y ofrecer a terceros tus propias aplicaciones o aplicaciones de terceros. Por supuesto, también respetamos la privacidad de los datos de terceros, a los que podemos acceder a través del uso del servicio que te ofrecemos. A veces, esto puede requerir un contrato por separado contigo. Si crees que este es el caso, contacta con nosotros.</p>
<p>Según nuestros Términos y Condiciones, no tienes derecho a compartir tus datos de acceso con terceros y estás obligado a tratar dichos datos con el debido cuidado. Además, eres responsable de los datos de terceros que introduzcas en <strong>Ayuda-T</strong>. Ten en cuenta que no tenemos ninguna capacidad sobre el cumplimiento de las normas de seguridad y protección de datos fuera de nuestro sitio web, la aplicación <strong>Selfconta</strong> o los servicios proporcionados por nosotros. En los casos descritos, tú o el tercero a quien le has otorgado acceso a tus datos sois los responsables.</p>
<p> </p>
<p><strong>3. Consentimiento para la transferencia de datos</strong></p>
<p>Transmitimos tus datos personales a terceros si nos lo solicitas, solo si has dado tu consentimiento explícito o si existen obligaciones legislativas para hacerlo.</p>
<p>No se realiza ninguna transferencia de datos personales a terceros con otros fines. Tus datos no se divulgan a ningún tercero sin tu permiso; a menos que un tribunal exija su entrega, y cuando lo haga solo en la medida necesaria.</p>
<p><strong>Ayuda-T</strong> se reserva el derecho de compartir datos dentro de su grupo de empresas, “Grupo Ayuda-T”, según se requiera, para proporcionarte servicios. <strong>Ayuda-T</strong> también puede solicitar, de vez en cuando, compartir datos con una compañía hermana, por ejemplo, para permitir la facturación de tu cuenta desde una entidad diferente a <strong>Ayuda-T.</strong> La seguridad de los datos está asegurada en todo momento.</p>
<p>Al registrarte en <strong>Ayuda-T,</strong> das tu consentimiento para el procesamiento de tus datos. También das tu consentimiento explícito para compartir tus datos con terceros cuando sea necesario para ofrecerte nuestro servicio.</p>
<p>Confirmamos que sólo compartimos tus datos con terceros cuyos estándares de mantenimiento de datos nos satisfacen y cumplen con toda la legislación de protección de datos.</p>
<p>Concretamente, cuando compartimos datos con territorios fuera de la UE o del EEE u otro no incluido en la lista aprobada por la Comisión Europea, nos aseguramos plenamente de sus normas de seguridad y confidencialidad de datos así como de que mantienen todos los datos compartidos de manera homologable a los estándares de la UE. Estamos obligados a poner a disposición, previa solicitud, prueba de - o referencia a -, las salvaguardas apropiadas, y podemos hacerlo después de recibir una solicitud dirigida a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u> por correo electrónico.</p>
<p>Tienes el derecho a retirar, en cualquier momento, tu consentimiento para el procesamiento y/o intercambio de tus datos, ya sea cerrando tu cuenta, que tiene efecto inmediato, o poniéndote en contacto con nosotros para solicitar el cierre, para que lo hagamos tan pronto como sea posible. Una vez finalizada tu relación con <strong>Ayuda-T</strong>, mantenemos solamente los datos mínimos que debemos mantener para cumplir con todos los requisitos legales, y solo por el período mínimo requerido.</p>
<p>Si tienes alguna pregunta sobre el procesamiento de tus datos personales, o si deseas solicitar el acceso a tus datos, puedes ponerte en contacto con el Delegado de Protección de Datos (DPO) escribiendo a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u> o escribiendo al DPO en la dirección indicada anteriormente.</p>
<p>Si no estás satisfecho, tienes derecho a presentar una queja ante la autoridad de protección de datos correspondiente. <strong>Ayuda-T</strong> cooperará plenamente con cualquier investigación de este tipo y se esforzará por satisfacer todas las consultas de la manera más completa posible. La autoridad pertinente para cada país se puede encontrar en el sitio web de la Comisión Europea: <a href="http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080">http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080</a></p>
<p> </p>
<p><strong>4. Cookies</strong></p>
<p>Nuestro sitio web utiliza cookies. Las cookies son pequeños archivos que se crean automáticamente en tu navegador y se almacenan en tu dispositivo (ordenador portátil, tableta, teléfono inteligente, etc.) cuando visitas una página. Las cookies no dañan tu dispositivo y no contienen virus, troyanos u otro software malicioso.</p>
<p>Las cookies almacenan información relacionada con tu dispositivo. Ahora bien, esto no significa que recibamos ningún conocimiento detallado de tu identidad.</p>
<p>El uso de cookies tiene el propósito de crear un uso más satisfactorio de nuestros servicios. Por lo tanto, usamos las llamadas cookies de sesión para reconocer si has visitado anteriormente páginas únicas de nuestro sitio web o si ya has creado una cuenta de cliente. Tu navegador las borrará automáticamente cuando caduquen.</p>
<p>Para fines de usabilidad, empleamos cookies temporales, que se almacenan en tu dispositivo por un período de tiempo específico. Si vuelves a visitar nuestro sitio web para usar nuestros servicios, se reconocerá que ya has visitado nuestro sitio web anteriormente y qué actividades has realizado, para que no tengas que repetirlas.</p>
<p>También usamos cookies para rastrear estadísticamente el uso de nuestro sitio web y para optimizar nuestra oferta (ver sección 4), así como para mostrarte información personalizada (ver sección 5.). Cuando vuelves a visitar nuestro sitio web, estas cookies nos permiten reconocer automáticamente que ya has visitado anteriormente nuestro sitio web. Después de un período de tiempo determinado, las cookies se eliminarán automáticamente.</p>
<p>La mayoría de los navegadores acepta cookies automáticamente. Puedes configurar tu navegador de manera que no se guarden cookies en este o para que siempre aparezca una advertencia antes de que se cree una nueva cookie.</p>
<p>Sin embargo, ten en cuenta que la desactivación completa de las cookies también puede ocasionar una funcionalidad limitada de nuestro sitio web.</p>
<p> </p>
<p><strong>5. Análisis web</strong></p>
<p>Para diseñar y optimizar continuamente nuestro sitio web, usamos diversos servicios de análisis web. Por lo tanto, creamos perfiles de usuarios anónimos y usamos cookies (ver sección 4).</p>
<p>A continuación, puedes encontrar más información sobre nuestros servicios de análisis web y sus opciones de desactivación:</p>
<p><strong>a) Google Analytics</strong></p>
<p>Usamos Google Analytics. Este es un servicio de análisis web de Google Inc. La información sobre el uso de nuestro sitio web (incluida tu dirección IP), que se recopila a través de una cookie, se transfiere a un servidor de Google en Estados Unidos y se almacena allí. Las direcciones IP son anónimas, por lo que no es posible asignártelas (enmascaramiento de IP). La información se emplea para analizar el uso de nuestro sitio web, para crear informes sobre las actividades del sitio web y para proporcionarnos otros servicios que están conectados con el uso de nuestro sitio web e internet. Los datos que has introducido al usar nuestro servicio no se incorporarán, de ninguna forma, a otros datos que se recopilan a través de Google.</p>
<p>La transferencia de información por parte de Google a terceros solo se llevará a cabo si así lo exige la ley o si terceros procesan los datos en su nombre.</p>
<p>También usamos Google Optimize. Este es un servicio de análisis web de Google Inc, que está integrado en Google Analytics. Google Optimize nos permite realizar tests A/B y multivariantes. De esta manera podemos averiguar qué versión de nuestro sitio web es la preferida por los usuarios. Aquí puedes encontrar más información sobre este servicio.</p>
<p>Puedes evitar la recopilación de datos, que se lleva a cabo a través de la cookie, así como el procesamiento de datos de Google descargando e instalando un complemento de navegador aquí.</p>
<p>Como alternativa al complemento de navegador, especialmente para navegadores en dispositivos móviles, puedes evitar la recopilación de datos de Google Analytics haciendo clic <u><a href="https://tools.google.com/dlpage/gaoptout">en este enlace</a></u>. Se colocará una cookie de bloqueo, que impidirá la recopilación de datos cuando se visite un sitio web. La cookie de bloqueo es válida solo en este navegador y para nuestro sitio web, y se archivará en tu dispositivo. Si eliminas la cookie en tu navegador, deberás volver a colocar la cookie de bloqueo.</p>
<p>Puedes encontrar más información sobre protección de datos con Google Analytics en <u><a href="https://support.google.com/analytics/?hl=es#topic=3544906">Ayuda de Analytics</a></u>.</p>
<p>Además, usamos la API Vision de Google Cloud. El sistema OCR (OCR) sirve para el reconocimiento óptico de caracteres, esto es, permite el reconocimiento automático y el análisis de letras, así como la categorización de documentos. Puedes encontrar más información sobre este servicio <u><a href="https://cloud.google.com/vision/?hl=es">aquí</a></u>. El reconocimiento de caracteres basado en la API Vision de Google Cloud es esencial para el uso de nuestros servicios. Si no deseas que se use la API Vision de Google Cloud, tienes la posibilidad de crear gastos sin cargar documentos. En tal caso, no podrás usar los servicios de <strong>Ayuda-T</strong> al completo.</p>
<p>En el siguiente enlace, puedes encontrar más información sobre protección de datos de Google: <u><a href="https://policies.google.com/privacy?hl=es">https://policies.google.com/privacy?hl=es</a></u></p>
<p> </p>
<p><strong>6. Segmentación de audiencia (</strong><em><strong>targeting</strong></em><strong>)</strong></p>
<p>Usamos tecnologías de segmentación de audiencia de Google Inc. (por ejemplo, Doubleclick, AdSense, AdWords) en nuestro sitio web. Estas tecnologías nos permiten ofrecerte publicidad basada en tus intereses. Para este propósito, recopilamos y evaluamos información sobre tu comportamiento de usuario en nuestro sitio web gracias al uso de cookies.</p>
<p>La recopilación y evaluación se lleva a cabo de forma anónima y no nos permite identificarte. En otras palabras, no vinculamos esta información con tus datos personales.</p>
<p>Si no deseas recibir publicidad personalizada, puedes evitarlo a través de la configuración de cookies relevantes en tu navegador.</p>
<p>Puedes cambiar la configuración para mostrar publicidad personalizada a través de la configuración de anuncios de Google.</p>
<p>Puedes encontrar más información, así como las normas de privacidad de datos sobre publicidad y Google en su <u><a href="https://policies.google.com/privacy?hl=es">política de privacidad</a></u>.</p>
<p> </p>
<p><strong>7. Seguimiento de Facebook</strong></p>
<p>No usamos plugins sociales de Facebook u otras redes sociales. En relación con nuestra publicidad en Facebook, empleamos un mecanismo de seguimiento basado en píxeles. Este es un servicio de análisis web provisto por Facebook Ireland Ltd. La información se usa para rastrear conversiones procedentes de la plataforma Facebook.</p>
<p>Este servicio lo provee Facebook Ireland Ltd., por el que se aplica la ley de privacidad de datos de la Unión Europea. No compartimos ningún dato que introduces mientras usas nuestro servicio con Facebook.</p>
<p>Consulta la <u><a href="https://www.facebook.com/business/gdpr">información de protección de datos de Facebook</a></u> para obtener más información sobre el propósito y el alcance de la recopilación de datos, y el procesamiento y uso de los datos por parte de Facebook, así como tus derechos y opciones de configuración para la protección de la privacidad.</p>
<p> </p>
<p><strong>8. Información, corrección, bloqueo, eliminación</strong></p>
<p>Tienes derecho a la información sobre datos personales que almacenamos y el derecho a corregir o a enmendar datos incorrectos, así como a bloquearlos y eliminarlos.</p>
<p>Como controlador de datos, eres responsable del contenido que publicas. Tienes derecho a rectificar, bloquear o borrar, en cualquier momento, cualquiera de tus datos. Podemos eliminar contenido que has publicado a petición tuya, pero mantenemos nuestro derecho a no eliminar el contenido que ya está publicado o que debemos mantener para cumplir con determinados requisitos legales.</p>
<p>Para obtener información sobre tus datos personales, para la corrección de datos erróneos o para el bloqueo o eliminación de datos, así como para otras preguntas sobre el uso de tus datos personales, envía un correo electrónico a <u><a href="mailto:soporte@ayudatepymes.com">soporte@ayudatepymes.com</a></u></p>
<p>Además, puedes ver y cambiar los datos que se almacenan en tu cuenta iniciando sesión en nuestro sitio web con tus datos de acceso. Puedes eliminar los datos de tu cuenta en todo momento. Esto se puede hacer mediante el uso de la opción correspondiente en tu cuenta. Advertimos que si eliminas tus datos, no podrás usar nuestro servicio al completo o en absoluto.</p>
<p> </p>
<p><strong>9. Cambios en la Declaración de Privacidad de Datos</strong></p>
<p>Esta Declaración de Privacidad de Datos está ahora vigente y se actualizó por última vez en mayo de 2018.</p>
<p>Debido al desarrollo posterior del sitio web, la aplicación <strong>Selfconta</strong> o cualquier otro servicio de <strong>Ayuda-T,</strong> o debido a cambios en los requisitos legales o reglamentarios, puede que esta Declaración de Privacidad de Datos deba modificarse en un futuro. Nuestra Declaración de Privacidad de Datos se puede acceder e imprimir en todo momento en nuestro sitio web.</p>


</div>

</div>


        </div>







    <?php
include 'required/footer.php';
?>
