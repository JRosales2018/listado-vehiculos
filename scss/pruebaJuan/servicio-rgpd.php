<?php
include 'required/header.php';
$what = 'servicio-rgpd';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="rgpd-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    PROTECCIÓN DATOS EMPRESAS Y PROFESIONALES<br>CUMPLE CON LA RGPD EN TU NEGOCIO
                </h1>

            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <img src="img/precios/precio-adaptacion-rgpd.png" alt="Precio adaptación RGPD" />
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-deepblue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                        
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row asesoria justify-content-sm-center">
                <h2>SERVICIO RGPD: TRABAJA SEGURO<br>
                    <span>CUMPLE CON LA NORMATIVA Y EVITA SANCIONES</span></h2>

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-sm-center">

                        <div class="col-sm-6 col-md-3 col-lg-4">
                            <h2>SERVICIO RGPD</h2>
                            <img src="img/precios/precio-adaptacion-rgpd.png" alt="Precio adaptación RGPD" />
                        </div>
                        <div class="col-sm-6 col-md-5 col-lg-6">
                            <p>Con la <span>auditoria RGPD</span> comprobamos <span>qué necesitas registrar</span> en cuanto a la <span>implantación RGPD.</span> Tu negocio <span>cumplirá con la normativa</span> y ante cualquier inspección elaboraremos y tramitaremos la documentación necesaria. Estarás <span>al día en cambios de normativa</span> con <span>asesoramiento continuo para tu negocio.</span></p>
                            <p class="boton-asesorias"><a class="ir-formulario">lo quiero!</a></p>
                        </div>

                    </div>


                </div>
            </div>


        </div>





        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito 900 100 162. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

        <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
        <div class="container">
            <div class="row modulo-empresas justify-content-sm-center">
                <div class="col-lg-7 pre-item-sucursal">
                    <h2>SERVICIOS RGPD</h2>

                    <div class="row last-item-sucursal justify-content-sm-center">
                        <div class="col-lg-6 col-md-6">

                            <p>Ayuda-T Pymes te ofrece <span>servicios RGPD para la protección de datos personales en los procesos de tu negocio.</span> Si tu empresa trabaja con datos sensibles está obligada a cumplir una serie de requisitos en cuanto a la Ley de Protección de Datos Personales. Con nosotros, no tienes de qué preocuparte: </p>
                            <li>1. Registro en la Agencia de Protección de Datos</li>
                            <li>2. Generación de documentos para los negocios
                            </li>
                            <li>3. Auditoria LOPD
                            </li>
                            <li>4. Consultoría LOPD
                            </li>
                        </div>

                        <div class="col-lg-6 col-md-6">

                            <p><span>¿Trabajas con pagos telemáticos a través de datafonos? ¿Recibe currículums tu empresa? ¿Tienes algún tipo de dispositivo de seguridad de vídeo? ¿Alguna vez has recogido formularios, envías marketing e-mail? </span></p>
                            <p>Prácticamente todas las empresas están obligadas a contemplar los requisitos de la normativa de la Ley de Protección de Datos Personales.</p>
                        </div>
                    </div>
                </div>

                <div class="row display-block-left">
                    <div class="col-lg-4 item-emp1">
                        <img src="img/rgpd.jpg" class="gestor1" alt="RGPD" />
                        
                    </div>
                    <div class="col-lg-4">
                        <h2 class="tit-gestores">TE OFRECEMOS SERVICIOS LOPD Y AUDITORÍA RGPD INTEGRALES</h2>
                        <h3 class="subtit-gestores">REGISTRO EN LA AGENCIA DE PROTECCIÓN DE DATOS</h3>
                        <p class="text-gestores1">Como tu consultor <span>RGPD</span>, Ayuda T Pymes se encargará de gestionar todos los trámites necesarios para registrar tu negocio en la Agencia de Protección de Datos, cumplimentando todos los requisitos legales necesarios.</p>
                        <h3 class="subtit-gestores">AUDITORÍA LOPD</h3>
                        <p class="text-gestores1">Las funciones de tu consultor <span>RGPD</span> no terminarán ahí. Ayuda T Pymes llevará a cabo una auditoría <span>RGPD</span> en tu negocio para asegurarnos de que todo cumple las condiciones que establece la Ley de Protección de Datos Personales y la normativa que lo regula. Todo bajo control.</p>
                    <h3 class="subtit-gestores">GENERACIÓN DE EXPEDIENTE Y<br>DOCUMENTACIÓN REGLADA PARA CUMPLIR LOPD</h3>
                        <p class="text-gestores1">Tras el registro <span>RGPD</span>, cada negocio necesita contar con su expediente de certificación. Para entendernos, se trata de una documentación que corrobora que cumples con la implantación de la LOPD. En este caso tu consultor <span>RGPD</span> también se hará cargo de todo, no tendrás que preocuparte de nada.</p>
                    </div>
                </div>
                <div class="row display-block-right">
                    <div class="col-lg-4 float-right item-emp2">
                        <img src="img/servicio-rgpd.jpg" class="gestor2" alt="Servicio RGPD" />
                        
                    </div>
                    <div class="col-lg-4 float-right">
                        <h3 class="subtit-gestores">CONSULTORÍA LOPD?</h3>
                        <p class="text-gestores1">Además de la auditoría <span>RGPD</span>, también contarás con un servicio de consultoría RGPD. ¿De qué se trata? Es sencillo. Supongo que tu negocio seguirá evolucionando. Hay muchos aspectos que son susceptibles de ser reflejados en el registro RGPD. No es necesario que tu empresa esté abriendo nuevas líneas de negocio, aunque dado el caso, <span>la consultoría RGPD será especialmente imprescindible.</span></p>
                        <p class="text-gestores1">Sin embargo, basta con que incluyas en tus procesos nuevas áreas o tareas que involucre la gestión de material sensible que esté sujeto a la <span>RGPD</span> para que tu empresa tenga que someterse al registro RGPD de estas actividades. De nuevo, no tendrás de qué preocuparte, <span>tu consultor RGPD se encargará de todo.</span></p>
                    </div>
                </div>
                <div class="row last-item-sucursal last-item-sucursal2 justify-content-sm-center">
                    <img class="etiqueta-derecha" src="img/etiqueta.png" alt="etiqueta"/>
    <img class="etiqueta-izq" src="img/etiqueta.png" alt="etiqueta"/>
                    <div class="col-lg-6 pre-item-sucursal">
                        <h2>CUMPLIR CON LA IMPLANTACIÓN LOPD ES FUNDAMENTAL EN TU NEGOCIO</h2>
                        <p><span>El servicio de consultoría RGPD de Ayuda T Pymes te pone fácil</span> llevar al día tus obligaciones legales en cuanto a este tema, la verdad, un tanto delicado. Para cualquier negocio, el hecho de no gestionar de forma adecuada la implantación <span>RGPD</span> puede tener una serie de consecuencias económicas bastante graves.</p>
                        <p><span>La imposición de sanciones por el incumplimiento de la Ley de Protección de Datos</span> y el adecuado registro <span>RGPD</span>, consiste en la imposición de sanciones económicas que, dependiendo de la importancia de la irregularidad, pueden oscilar entre los <span>600 euros como mínimo a los 600.000 euros.</span></p>
                        <p>Con el acceso a nuestro <span>servicios RGPD integrales</span>, el registro, el servicio de auditoría y consultoría <span>RGPD</span>, podrás olvidarte de todos estos aspectos y, como siempre trabajando con Ayuda T Pymes ahora también como consultor <span>RGPD</span>, centrarte en hacer crecer tu negocio.</p>

                    </div>
                </div>
            </div>


            <!--        ---------------------------------- BANNER --------------------------------------->

            <?php

include 'required/bannerSelfconta.php';
?>
                <!--        ---------------------------------- FIN BANNER --------------------------------------->
                <!--        ---------------------------------- CIUDADES --------------------------------------->
        </div>
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->
<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {

    
        if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".contenido-gestoria img", 1,{scale:1, ease: Back.easeOut});
        }
        
            if ($(window).scrollTop() >= $('.item-emp1').offset().top + $('.item-emp1').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
    tl4.to(".gestor2", 1,{opacity:1,x:0},"-=.5");
        }
    });
    
});
</script>



    <?php
include 'required/footer.php';
?>
