<?php
include 'required/header.php';
$what = 'abrir-sucursal';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="abrir-sucursal-page">
        <div class="container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <h1 class="radj-b-30 title-black">
                    ¿VAS A ABRIR UNA SUCURSAL EN ESPAÑA?
                </h1>
                <h2>Todos los trámites incluidos</h2>
            </div>
            <div class="row justify-content-sm-center cabecera">

                <div class="col-sm-6 col-lg-4 img-section1">

                    <div class="row">
                        <div class="precio">
                            <img src="img/precios/precio-abrir-sucursal.png" alt="Precio abrir sucursal" />
                        </div>
                        <div class="datosprecio">
                            <span class="llama">LLAMA AHORA</span>
                            <span class="gratis">GRATIS</span>
                            <a href="tel:900100162"><span class="datos-tlf">900 100 162</span></a>

                        </div>
                    </div>
                    <p class="legal-precio">​​Tú pones la idea, nosotros nos encargamos del resto</p>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-12 columncenter">
                    <div class="arrow">
                        <img src="img/arrow-blue.png" alt="flecha">
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4">
                    <div class="bloque2-section1">
                         
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->

        <div class="row slide-gestoria justify-content-sm-center">
            <h2><span>¿QUIERES ABRIR UNA SUCURSAL EN ESPAÑA Y NO SABES POR DÓNDE EMPEZAR?</span><br> NO HACE FALTA, NOS ENCARGAMOS NOSOTROS</h2>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
        <div class="container">
            <div class="row gestoria justify-content-sm-center">

                <div class="col-lg-7 contenido-gestoria">
                    <div class="row justify-content-sm-center">
                        <div class="col-sm-6 col-md-4 col-lg-6">

                            <div class="row">
                                <h2 class="subtit-gestoria">CREAR SUCURSAL</h2>
                                <h2>SERVICIO INTEGRAL</h2>
                                <img src="img/precios/precio-abrir-sucursal.png" alt="Precio abrir sucursal" />
                                 <p><span>Constitución de la sucursal</span> de acuerdo con la normativa española, gestión de trámites y registros. Asesoramiento especializado de la mano de un <span>equipo de expertos en la creación de sucursales.</span></p>

                            </div>
                            
                        </div>
                        

                    </div>
                </div>


            </div>



        </div>

        <!--        ---------------------------------- FORMULARIOS --------------------------------------->

        <div class="row pre-formulario  justify-content-sm-center">
            <p>
                LLAMA AHORA GRATIS AL <a href="tel:900100162">900 100 162</a>
            </p>
        </div>
        <div class="row formulario  justify-content-md-center">
            <div class="container">
                <div class="row justify-content-sm-center contenido-formulario">
                    <div class="col-lg-4">
                        <p class="texto-formulario">Póngase en contacto mediante nuestro teléfono gratuito <a href="tel:900100162">900 100 162</a>. El horario de atención telefónica de nuestra asesoría es 24 horas al día los 365 días del año. Si es usted cliente recuerde que el horario de los asesores es de Lunes a Jueves de 9 a 18h. y los Viernes de 9 a 14h. Si lo desea puede enviarnos un mensaje mediante nuestro formulario y le contestaremos con la máxima rapidez.</p>

                        <p class="texto-formulario2">INFÓRMATE ¡SIN COMPROMISO!</p>
                    </div>
                    <div class="col-lg-4 ">
                        <?php 
                        include 'required/form-body.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--        ---------------------------------- FIN FORMULARIOS --------------------------------------->

    <!--        ---------------------------------- MODULO EMPRESAS --------------------------------------->
    <div class="container">
        <div class="row modulo-empresas justify-content-sm-center">
            <div class="col-lg-6 pre-item-sucursal">
                <h2>ABRIR SUCURSAL EN ESPAÑA</h2>
                <h3>Servicio integral constitución sucursal</h3>
                <p><span>No conoces los requisitos para abrir una sucursal en España?</span><br> Si necesitas asesoramiento especializado en el establecimiento de una sucursal en España, has llegado al sitio indicado.</p>
                <p>En AYUDA T PYMES nos encargamos de todos los trámites necesarios, tú sólo tienes que arrancar tu negocio en cuanto terminemos con el proceso de crear sucursal.</p>
            </div>
            <div class="row display-block-left">
                <div class="col-lg-4 item-emp1">
                    <img src="img/crear-sucursal.jpg" class="gestor1" alt="Crear sucursal" />
                    <p class="gestor-name">Ana | Departamento de marketing</p>
                    <p class="gestor-description">Una rockera enamorada del deporte y de sus dos gatos: Sombra y Blue</p>
                </div>
                <div class="col-lg-5">
                    <h2 class="tit-gestores">¿HAS DECIDIDO ABRIR SUCURSAL EN ESPAÑA?</h2>
                    <p class="text-gestores1">Bien. Nada de lidiar con la normativa, olvídate del papeleo y las gestiones propias de la constitución de <span>empresas sucursales.</span></p>
                    <p class="text-gestores1">Si quieres emprender con tu negocio cuanto antes, nuestro departamento de expertos está a tu disposición cuando lo solicites.</p>
                    <p class="text-gestores1">Si quieres emprender con tu negocio cuanto antes, nuestro departamento de expertos está a tu disposición cuando lo solicites.</p>
                    <p class="text-gestores1"><span>Abrir una sucursal en España nunca ha sido tan sencillo.</span> No te dejes abrumar por todos los trámites y requisitos para crear sucursal, de eso se carga tu equipo de expertos en Ayuda-T Pymes.
</p>
                    <p class="text-gestores1"><span>¿Cuándo empezamos?</span></p>
                    
                </div>
            </div>

            <!--        ---------------------------------- FIN EMPRESAS --------------------------------------->

           

        </div>




        
      
        <!--        ---------------------------------- BANNER --------------------------------------->

        <?php
include 'required/services-module.php';
include 'required/bannerSelfconta.php';
?>
            <!--        ---------------------------------- FIN BANNER --------------------------------------->
            <!--        ---------------------------------- CIUDADES --------------------------------------->
    </div>


    <!--        ---------------------------------- FIN CIUDADES --------------------------------------->
<script>
$(document).ready(function(){
    
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() >= $('.pre-item-sucursal').offset().top + $('.pre-item-sucursal').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestor1", 1,{opacity:1,x:0});
        }
        
             if ($(window).scrollTop()+500 >= $('.contenido-gestoria').offset().top + $('.contenido-gestoria').outerHeight() - window.innerHeight) {
    var tl4 = new TimelineLite({});
    
    tl4.to(".gestoria img", 1,{scale:1, ease: Back.easeOut});
        }
    });
    
});
</script>



    <?php
include 'required/footer.php';
?>
