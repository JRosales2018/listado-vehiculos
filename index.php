<?php

    // Conexión DDBB
    $mysqli = new mysqli('localhost', 'root', '', 'coches');

    /*
    * Esta es la forma OO "oficial" de hacerlo,
    * AUNQUE $connect_error estaba averiado hasta PHP 5.2.9 y 5.3.0.
    */
    if ($mysqli->connect_error) {
        die('Error de Conexión (' . $mysqli->connect_errno . ') '
        . $mysqli->connect_error);
    }

    /*
    * Use esto en lugar de $connect_error si necesita asegurarse
    * de la compatibilidad con versiones de PHP anteriores a 5.2.9 y 5.3.0.
    */
    if (mysqli_connect_error()) {
        die('Error de Conexión (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
    }

    echo 'Éxito... ' . $mysqli->host_info . "\n";
    echo '</br>';


    //    $file = 'coches.xml';

    // Seleccionar el archivo correcto y en orden

    $file = 'xml4.php';

    //    // Cargamos el contenido del archivo en UTF-8
    //    $content = file_get_contents($file);
    //
    //    $dom = new DOMDocument;
    //    $dom->loadXML($content);
    //    $books = $dom->getElementsByTagName('metadatos');
    //    foreach ($books as $book) {
    //        echo htmlentities($book->nodeValue) . '<p>', PHP_EOL;
    //    }

    //    Extraemos el xml del archivo
    $xml = simplexml_load_file($file);
    //    Pillamos el contenido del elemento TABLE
    $txt = $xml->texto;

    //    Convertirmo el objeto simplexml en array
    $json = json_encode($txt);
    $array = json_decode($json,TRUE);

    //    Obtenemos el total de <TABLE> contienen el listado de vehículos, menos la primera tabla [0]
    $totalTablas = sizeof($array['table']);

    //      Los listados estan metidos en tabla, el primer valor [table][1] es la tabla que es una diferente para cada marca
    //      ['table'][1]['caption'] extrae el nombre de la marca
    //      El segundo valor [tr][0] es la línea que es una para cada modela
    //    print_r($array['table'][1]['tbody']['tr'][0]);

    //print_r($array['table'][1]['tbody']['tr'][0]['td'][0]);





    // for($i = 1; $i <= count($array['table']); $i++){
    //
    //     for($j = 0; $j <= count($array['table'][$i]['tbody']['tr']); $j++){
    //
    //
    //       var_dump($array['table'][$i]['tbody']['tr'][$j]);
    //       echo '</br>';
    //     }
    //
    // }
    //    Marca

    //if (!($sentencia = $mysqli->prepare("INSERT IGNORE INTO marcas(nombre) VALUES (?)"))) {
    ////     echo "Falló la preparación: (" . $mysqli->errno . ") " . $mysqli->error;
    ////}

    if (!($sentencia2 = $mysqli->prepare("INSERT IGNORE INTO modelos(marca, modelo, periodo, cc, cilindros, gd, kw, cvf, co2, cv, valor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"))) {
        echo "Falló la preparación: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    /* Sentencia preparada, etapa 2: vinculación y ejecución */

    if (!$sentencia2->bind_param("sssssssssss", $marca, $modelo, $periodo, $cc, $cilindros, $gd, $kw, $cvf, $co2, $cv, $valor)) {
        echo "Falló la vinculación de parámetros: (" . $sentencia->errno . ") " . $sentencia->error;
    }

    /* Sentencia preparada: ejecución repetida, sólo datos transferidos desde el cliente al servidor */
    for ($i = 0; $i < count($array['table']); $i++) {

        $marca = substr($array['table'][$i]['caption'], 7);
        $lineas = count($array['table'][$i]['tbody']['tr']);
        echo '<p>' . $i . '<br>';
        echo $marca . '<br>';
        echo $lineas . '<p>';

        for ($j = 0; $j < $lineas; $j++) {

            if($lineas == 1) {
                $modelo = utf8_decode($array['table'][$i]['tbody']['tr']['td'][0]);
                $periodo = utf8_decode($array['table'][$i]['tbody']['tr']['td'][1]);
                $cc = utf8_decode($array['table'][$i]['tbody']['tr']['td'][2]);
                $cilindros = utf8_decode($array['table'][$i]['tbody']['tr']['td'][3]);
                $gd = utf8_decode($array['table'][$i]['tbody']['tr']['td'][4]);
                $kw = utf8_decode($array['table'][$i]['tbody']['tr']['td'][5]);
                $cvf = utf8_decode($array['table'][$i]['tbody']['tr']['td'][6]);
                $co2 = utf8_decode($array['table'][$i]['tbody']['tr']['td'][7]);
                $cv = utf8_decode($array['table'][$i]['tbody']['tr']['td'][8]);
                $valor = utf8_decode($array['table'][$i]['tbody']['tr']['td'][9]);
            } else {
                $modelo = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][0]);
                $periodo = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][1]);
                $cc = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][2]);
                $cilindros = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][3]);
                $gd = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][4]);
                $kw = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][5]);
                $cvf = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][6]);
                $co2 = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][7]);
                $cv = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][8]);
                $valor = utf8_decode($array['table'][$i]['tbody']['tr'][$j]['td'][9]);
            }

            if (!$sentencia2->execute()) {
                echo "Falló la ejecución: (" . $sentencia->errno . ") " . $sentencia->error;
            }
        }
    }

    /* se recomienda el cierre explícito */
    $sentencia2->close();

    $mysqli->close();
